//
//  Details2VC.swift
//  Smart Repair
//
//  Created by MyMac on 16/11/21.
//  Copyright © 2021 Infrap. All rights reserved.
//

import UIKit
import iOSDropDown
// Add market detail page 2
class Details2VC: UIViewController,UITextFieldDelegate,UITextViewDelegate {
    @IBOutlet weak var MarketCountryCode: DropDown!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var currentLocation: UITextField!
    @IBOutlet weak var DescriptionField: UITextView!
    @IBOutlet weak var availbtn1: UIButton!
    @IBOutlet weak var availbtn2: UIButton!
    @IBOutlet weak var Nextbtn: UIButton!
    @IBOutlet weak var priceField: UITextField!{
        didSet {
          priceField?.addDoneCancelToolbar(onDone: (target: self, action: #selector(doneButtonTappedForMypriceTextField)))
        }
    }
    @IBOutlet weak var Whatsappnumber: UITextField!{
        didSet {
            Whatsappnumber?.addDoneCancelToolbar(onDone: (target: self, action: #selector(doneButtonTappedForMyWhatsappTextField)))
        }
    }
    @IBOutlet weak var Phonenumber: UITextField!{
        didSet {
           Phonenumber?.addDoneCancelToolbar(onDone: (target: self, action: #selector(doneButtonTappedForMyPhonenumberTextField)))
        }
    }
    
    var Isavailable = ""
    let radioController: RadioButtonController = RadioButtonController()
    override func viewDidLoad() {
        super.viewDidLoad()
        priceField.delegate = self
        DescriptionField.delegate = self
        DescriptionField.layer.borderWidth = 1
        DescriptionField.layer.borderColor = #colorLiteral(red: 0, green: 0.5976013541, blue: 1, alpha: 1)
        currentLocation.delegate = self
        Phonenumber.delegate = self
        Whatsappnumber.delegate = self
        //Isavailable.delegate = self
        Nextbtn.layer.cornerRadius = 15
        
        MarketCountryCode.optionArray = ["1","2","3"]
        MarketCountryCode.didSelect{(selectedText , index ,id) in
        self.MarketCountryCode.text = selectedText
        }
        MarketCountryCode.arrowSize = 10
        
        
        radioController.bluetoothArray = [availbtn1,availbtn2]
        
        let contentWidth = scrollview.bounds.width
               let contentHeight = scrollview.bounds.height * 3
        scrollview.contentSize = CGSize(width: contentWidth, height: contentHeight)
               
        
        
        // Do any additional setup after loading the view.
    }
    
    @objc func doneButtonTappedForMyWhatsappTextField() {
       print("Done");
       
            Whatsappnumber.resignFirstResponder()
   }
    @objc func doneButtonTappedForMyPhonenumberTextField() {
       print("Done");
       
            Phonenumber.resignFirstResponder()
   }
    
    @objc func doneButtonTappedForMypriceTextField() {
       print("Done");
       priceField.resignFirstResponder()
   }
    
    @IBAction func Availablebtn1Action(_ sender: UIButton) {
           radioController.buttonArrayUpdated1(buttonSelected: sender)
        
        Isavailable = "Yes"
       }

       @IBAction func  Availablebtn2Action(_ sender: UIButton) {
           radioController.buttonArrayUpdated1(buttonSelected: sender)
           Isavailable = "No"
       }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    /* Updated for Swift 4 */
        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            if(text == "\n") {
                textView.resignFirstResponder()
                return false
            }
            return true
        }

    @IBAction func nextbtnclicked(_ sender: Any) {
//        
//       if(priceField.text == "")
//        {
//            let alert = UIAlertController(title: " Price is Mandatory!", message: "Please complete the required field", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//            present(alert, animated: true, completion:  nil)
//        }
//        
//       else if(DescriptionField.text == "")
//         {
//             let alert = UIAlertController(title: " Description is Mandatory!", message: "Please complete the required field", preferredStyle: .alert)
//             alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//             present(alert, animated: true, completion:  nil)
//         }
//        
//        else if(currentLocation.text == "")
//          {
//              let alert = UIAlertController(title: " Location is Mandatory!", message: "Please complete the required field", preferredStyle: .alert)
//              alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//              present(alert, animated: true, completion:  nil)
//          }
//        else if(MarketCountryCode.text == "")
//          {
//              let alert = UIAlertController(title: " Country is Mandatory!", message: "Please complete the required field", preferredStyle: .alert)
//              alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//              present(alert, animated: true, completion:  nil)
//          }
//        else if(Phonenumber.text == "")
//          {
//              let alert = UIAlertController(title: " Phonenumber is Mandatory!", message: "Please complete the required field", preferredStyle: .alert)
//              alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//              present(alert, animated: true, completion:  nil)
//          }
//        else if(Whatsappnumber.text == "")
//          {
//              let alert = UIAlertController(title: " Whatsapp number is Mandatory!", message: "Please complete the required field", preferredStyle: .alert)
//              alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//              present(alert, animated: true, completion:  nil)
//          }
//        else if isValidPhone(phone: Phonenumber.text!) == false{
//            
//            let alert = UIAlertController(title: " Phonenumber is Mandatory!", message: "Please enter valid  Phone number", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//            present(alert, animated: true, completion:  nil)
//            //your code here
//            }
//        else if isValidPhone(phone: Whatsappnumber.text!) == false{
//            
//            let alert = UIAlertController(title: " Whatsapp number is Mandatory!", message: "Please enter valid  Whatsapp number", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//            present(alert, animated: true, completion:  nil)
//            //your code here
//            }
//
        let userd = UserDefaults.standard
        userd.set(priceField.text, forKey: "Price")
        userd.set(DescriptionField.text, forKey: "Description")
        userd.set(currentLocation.text, forKey: "CurrentLocation")
        userd.set(MarketCountryCode.text, forKey: "CountryCode")
        userd.set(Phonenumber.text, forKey: "Phonenumber")
        userd.set(Whatsappnumber.text, forKey: "Whatsappnumber")
        userd.set(Isavailable, forKey: "IsAvailable")
        
        performSegue(withIdentifier: "ThirdPage", sender: self)
    }
    
    
    
    func isValidPhone(phone: String) -> Bool {
            let phoneRegex = "^[0-9+]{0,1}+[0-9]{5,16}$"
            let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
            return phoneTest.evaluate(with: phone)
        }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
