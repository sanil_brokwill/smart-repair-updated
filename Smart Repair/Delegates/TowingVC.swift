//
//  TowingVC.swift
//  Smart Repair
//
//  Created by Infrap on 16/9/21.
//  Copyright © 2021 Infrap. All rights reserved.
//

import UIKit
import MessageUI
 import CoreLocation
import MapKit
// emergency messaging`
class TowingVC: UIViewController,MFMessageComposeViewControllerDelegate,CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    
    var place : Double?
    var place2 : Double?
    let latitude = UserDefaults.standard.value(forKey: "latitude")
    let longtitude = UserDefaults.standard.value(forKey: "longitude")
    let ipaddress =  UserDefaults.standard.value(forKey: "ipAddress")
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func displayMessageInterface() {
        let composeVC = MFMessageComposeViewController()
        composeVC.messageComposeDelegate = self
        
        // Configure the fields of the interface.
       
        
        composeVC.recipients = ["7560955245"]
       
        
        if (UIApplication.shared.canOpenURL(NSURL(string:"http://maps.apple.com/maps")! as URL)) {
                    // apple map
                   // let url = "http://maps.apple.com/maps?saddr=\(latitude),\(longtitude)&daddr=\(latitude),\(longtitude)"
                   // UIApplication.shared.openURL(URL(string:url)!)
            
            //composeVC.body = url
                }

//        let URLString = "https://maps.apple.com?ll=\(latitude),\(longtitude)"
//        composeVC.body = "Stuck at \(URLString)"
        
        // Present the view controller modally.
        if MFMessageComposeViewController.canSendText() {
            let urlToShare = "https://maps.apple.com?ll=\(latitude),\(longtitude)"

                   composeVC.body = "Hey,I am in danger.I need help.Please urgently reach me out.Here is my location \(urlToShare)"

                   composeVC.messageComposeDelegate = self
            
            
            self.present(composeVC, animated: true, completion: nil)
        } else {
            print("Can't send messages.")
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.requestAlwaysAuthorization()
                locationManager.requestWhenInUseAuthorization()
                if CLLocationManager.locationServicesEnabled() {
                    locationManager.delegate = self
                    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                    locationManager.startUpdatingLocation()
                }
        
        self.displayMessageInterface()
       
        
       // getAddressFromLatLon(pdblLatitude: latitude as! String, withLongitude: longtitude as! String)

        // Do any additional setup after loading the view.
    }
    
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
            var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
            let lat: Double = Double("\(pdblLatitude)")!
            //21.228124
            let lon: Double = Double("\(pdblLongitude)")!
            //72.833770
            let ceo: CLGeocoder = CLGeocoder()
            center.latitude = lat
            center.longitude = lon

            let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        place = center.latitude
        place2 = center.longitude


            ceo.reverseGeocodeLocation(loc, completionHandler:
                {(placemarks, error) in
                    if (error != nil)
                    {
                        print("reverse geodcode fail: \(error!.localizedDescription)")
                    }
                    let pm = placemarks! as [CLPlacemark]

                    if pm.count > 0 {
                        let pm = placemarks![0]
                        print(pm.country)
                        print(pm.locality)
                        print(pm.subLocality)
                        print(pm.thoroughfare)
                        print(pm.postalCode)
                        print(pm.subThoroughfare)
                        var addressString : String = ""
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                           // self.place = pm.subLocality
                        }
                        if pm.thoroughfare != nil {
                            addressString = addressString + pm.thoroughfare! + ", "
                            
                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                            //self.place2 = pm.locality
                        }
                        if pm.country != nil {
                            addressString = addressString + pm.country! + ", "
                        }
                        if pm.postalCode != nil {
                            addressString = addressString + pm.postalCode! + " "
                        }


                        print(addressString)
                        
                        
                  }
            })

        }
    
    
  

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
