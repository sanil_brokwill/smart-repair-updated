//
//  UIExtensions.swift
//  Smart Repair
//
//  Created by Admin on 26/7/2023.
//  Copyright © 2023 Infrap. All rights reserved.
//

import Foundation
import UIKit


extension UIButton{
    @IBInspectable var cornerR : CGFloat{
        get {
            return self.cornerR
        }
        set{
            self.layer.cornerRadius = newValue
        }
    }
}

extension UITextView{
    @IBInspectable var cornerR : CGFloat{
        get {
            return self.cornerR
        }
        set{
            self.layer.cornerRadius = newValue
        }
    }
    
}

extension Data {
    mutating func append(_ string: String) {
        if let data = string.data(using: .utf8) {
            append(data)
        }
    }
}
struct Media {
    let key: String
    let fileName: String
    let data: Data
    let mimeType: String

    init?(withImage image: UIImage, forKey key: String) {
        self.key = key
        self.mimeType = "image/jpg"
        self.fileName = "\(arc4random()).jpeg"
        guard let data = image.jpegData(compressionQuality: 0.5) else { return nil }
        self.data = data
    }
}

