//
//  Offer.swift
//  Smart Repair
//
//  Created by MyMac on 18/10/21.
//  Copyright © 2021 Infrap. All rights reserved.
//

import Foundation

// MARK: - Welcome8
struct OfferResponse: Decodable {
    var status: Int
    var meta: Meta
    var message: String?
    var data: [OfferItem]
    var errors: String
    var total_items: Int
    
}

// MARK: - Datum
struct OfferItem:  Decodable{
    var offer: String
    var plan_id: String
    var offer_id: Int
    var offer_start_date : String
    var offer_end_date: String
    let offer_applicable:Int
    let offer_used: Int
    let offer_included:Int
}

//struct Metaa: Decodable {
//    var code: Int
//   var message: String
//}

//{
//    "status": 0,
//    "meta": {
//        "code": 200,
//        "message": "success"
//    },
//    "message": "success",
//    "data": [
//        {
//            "plan_id": "price_1JBz3CEOrEwYl9P1p9PuKOq2",
//            "offer_id": 1,
//            "offer": "Headlight polishes & brush touch",
//            "offer_start_date": "2022-01-12 5:07:18",
//            "offer_end_date": "2021-11-14 05:07:18",
//            "offer_applicable": 0,
//            "offer_used": 0
//        },
//        {
//            "plan_id": "price_1JBz3CEOrEwYl9P1p9PuKOq2",
//            "offer_id": 2,
//            "offer": "Buff & polish",
//            "offer_start_date": "2022-01-12 5:07:18",
//            "offer_end_date": "2021-11-14 05:07:18",
//            "offer_applicable": 0,
//            "offer_used": 0
//        },
//        {
//            "plan_id": "price_1JBz3CEOrEwYl9P1p9PuKOq2",
//            "offer_id": 3,
//            "offer": "Aircon regas ",
//            "offer_start_date": "2022-04-12 5:07:18",
//            "offer_end_date": "2021-11-14 05:07:18",
//            "offer_applicable": 0,
//            "offer_used": 0
//        }
//    ],
//    "errors": "error",
//    "total_items": 3
//}
//
