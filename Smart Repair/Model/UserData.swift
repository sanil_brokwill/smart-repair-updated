//
//  UserData.swift
//  Coupons
//
//  Created by Infrap on 31/7/20.
//  Copyright © 2020 Infrap. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

final class UserData:NSObject,CLLocationManagerDelegate {
    
    let userDefults = UserDefaults.standard
    var userDic:[String:Any]{
        
        get{
            if let localuserDic = userDefults.dictionary(forKey: "userDict") as? Dictionary<String, Any>{
            
            return localuserDic
            }
            return ["":""]
        }
        set{
            
            let dic = newValue
            
            userId = dic["user_id"] as? String
            userEmail = dic["email"] as? String
            userSession = dic["session"] as? String
            userPassword = dic["password"] as? String
            
        }
        
    }
    
    var userEmail:String?
    var userId:String?
    var userSession:String?
    var userPassword:String?
    var deviceId:String?
    var latitude:String?
    var longitude:String?
    var ipAddress:String?
    let locationManager = CLLocationManager()
    static let sharedInstance = UserData()
    func setLocationIPAndDeviceId(){
        let userDicInit = userDic
      print(userDicInit)
        userId = userDicInit["user_id"] as? String
        userEmail = userDicInit["email"] as? String
        userSession = userDicInit["session"] as? String
        userPassword = userDicInit["password"] as? String
        locationSetup()
        sampleIPTest()
        getDeviceId()
    }
    func locationSetup(){
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])  {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        latitude = String(locValue.latitude)
        longitude = String(locValue.longitude)
        locationManager.stopUpdatingLocation()
    }
    
    func sampleIPTest(){
        let Ipaddress = getIFAddresses()
        print(Ipaddress)
        ipAddress = Ipaddress[0]
    }
    
    func getIFAddresses() -> [String] {
        var addresses = [String]()
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return [] }
        guard let firstAddr = ifaddr else { return [] }
        
        // For each interface ...
        for ptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let flags = Int32(ptr.pointee.ifa_flags)
            let addr = ptr.pointee.ifa_addr.pointee
            
            // Check for running IPv4, IPv6 interfaces. Skip the loopback interface.
            if (flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING) {
                if addr.sa_family == UInt8(AF_INET) || addr.sa_family == UInt8(AF_INET6) {
                    
                    // Convert interface address to a human readable string:
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    if (getnameinfo(ptr.pointee.ifa_addr, socklen_t(addr.sa_len), &hostname, socklen_t(hostname.count),
                                    nil, socklen_t(0), NI_NUMERICHOST) == 0) {
                        let address = String(cString: hostname)
                        addresses.append(address)
                    }
                }
            }
        }
        
        freeifaddrs(ifaddr)
        return addresses
    }
    
    func getDeviceId(){
        let identifier = UIDevice.current.identifierForVendor?.uuidString
        let userDefaults = UserDefaults.standard
        let token = String(identifier!)
        print("Device Token \(token)")
        deviceId = token
        
    }
//    private override init(){}
}
