//
//  AddQuote.swift
//  Smart Repair
//
//  Created by MyMac on 13/10/21.
//  Copyright © 2021 Infrap. All rights reserved.
//

import Foundation


struct AddquoterResponse:Decodable {
    
    var status:Int
    var meta:Meta1
    var message:String
    var data:[AddQuoteData]
    var errors:String
    
}

struct AddQuoteData:Decodable {
    var id : Int
    var member_id : String
    var front_view : String
    var  rear_view : String
    var left_view : String
    var right_view : String
    var status : String
}

struct Meta1:Decodable {
    let code : Int
    let message: String
}



//{
//    "status": 0,
//    "meta": {
//        "code": 200,
//        "message": "success"
//    },
//    "message": "success",
//    "data": [
//        {
//            "id": 18,
//            "member_id": "76",
//            "front_view": "quote_front_76_1634130181.gif",
//            "rear_view": "quote_rear_76_1634130181.gif",
//            "right_view": "quote_right_76_1634130181.png",
//            "left_view": "quote_left_76_1634130181.png",
//            "status": "1"
//        }
//    ],
//    "errors": "error",
//    "total_items": 1
//}
//
