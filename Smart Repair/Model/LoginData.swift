//
//  LoginData.swift
//  Coupons
//
//  Created by Infrap on 1/6/20.
//  Copyright © 2020 Infrap. All rights reserved.
//

import Foundation


struct loginResponse:Decodable {
    let meta: Meta
    let data:[LoginData]
    var dataValue:LoginData? {return data[0]}
    var message:String
    let status:Int
    let errors:String
    let total_items: Int
}
struct LoginData:Decodable {
    let id:Int
    let member_name:String
    let member_email:String
    let member_phone:String
    let member_image:String
    let member_added_by:Int
     let member_added_on:String
//     let member_activated_on:String
     let member_firebaseudid:String
     let member_status:Int
     let member_id:String
    let session_id:String
     let device_id:String

}

/*
{
    "status": 1,
    "meta": {
        "code": 200,
        "message": "Success"
    },
    "message": "Success",
    "data": [
        {
            "id": 1,
            "member_name": "Prasad Raju",
            "member_email": "prasad@infrap.com",
            "member_phone": "8086766660",
            "member_image": "",
            "member_added_by": 1,
            "member_added_on": "2020-07-22 12:44:30",
            "member_activated_on": "2020-07-22 00:00:00",
            "member_firebaseudid": "",
            "member_status": 1,
            "member_id": "1",
            "session_id": "uhfzwrYePT",
            "device_id": ""
        }
    ],
    "errors": "",
    "total-items": 1
}*/
