//
//  TowmateApp.swift
//  Towmate
//
//  Created by Admin on 27/7/2023.
//

import SwiftUI

@main
struct TowmateApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
