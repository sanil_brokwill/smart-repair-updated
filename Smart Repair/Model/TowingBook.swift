//
//  TowingBook.swift
//  Smart Repair
//
//  Created by Admin on 26/7/2023.
//  Copyright © 2023 Infrap. All rights reserved.
//

import Foundation
struct TowingBook: Decodable {
    let status: Int
    let meta: Meta
    let message: String
}



//struct TowingBook: Decodable {
//    let status: Int
//    let meta: Meta
//    let message: String
//    let data: DataClass?
//    let errors: String
//    let totalItems: Int
//
//    enum CodingKeys: String, CodingKey {
//        case status, meta, message, data, errors
//        case totalItems = "total_items"
//    }
//}
//
//// MARK: - DataClass
//struct DataClass: Decodable {
//    let bookingID, towingcompanyID: Int
//    let remarks, sessionID, deviceID: String
//
//    enum CodingKeys: String, CodingKey {
//        case bookingID = "booking_id"
//        case towingcompanyID = "towingcompany_id"
//        case remarks
//        case sessionID = "session_id"
//        case deviceID = "device_id"
//    }
//}
