
import Foundation
struct CouponResponse:Decodable {
    var status:Int
    var meta:Meta
    var message:String
    var data:[CouponData]
    var errors:String
    var total_items: Int
}
struct CouponData:Decodable {
    var coupon_id: Int
    var coupon_item: String
    var coupon_serial:String
    var coupon_discount:String
    var coupon_description:String
    var coupon_image:String
    var coupon_place:String
    var coupon_added_on:String
    var coupon_expires_on:String
}
struct Meta:Decodable {
    let code : Int
    let message: String
}






/*

 {
     "status": 0,
     "meta": {
         "code": 200,
         "message": "success"
     },
     "message": "success",
     "data": [
         {
             "coupon_id": 1,
             "coupon_item": "iPhone",
             "coupon_serial": "123456AAA",
             "coupon_discount": 50,
             "coupon_description": "First Coupon",
             "coupon_image": "",
             "coupon_place": "Cochin",
             "coupon_added_on": "2020-07-23 09:00:00",
             "coupon_expires_on": "2020-08-23 00:00:00"
         },
         {
             "coupon_id": 2,
             "coupon_item": "Android",
             "coupon_serial": "123456BBB",
             "coupon_discount": 30,
             "coupon_description": "Second Coupon",
             "coupon_image": "",
             "coupon_place": "Trivandrum",
             "coupon_added_on": "2020-07-23 03:00:11",
             "coupon_expires_on": "2020-08-23 00:00:00"
         }
     ],
     "errors": "error",
     "total-items": 3
 }
*/
