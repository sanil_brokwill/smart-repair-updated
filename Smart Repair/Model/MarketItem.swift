

import Foundation

struct MarketItemResponse:Decodable{
    var status:Int
       var meta:Meta
       var message:String
       var data:[MarketItem]
       var errors:String
       var total_items: Int
}
struct MarketItem:Decodable {
    var market_id:Int?
    var market_added_by:Int?
    var market_added_name:String?
    var market_networkid:Int?
    var market_added_date:String?
    var market_brand:String?
    var market_model:String?
    var market_variant:String?
    var market_year:String?
    var market_type:String?
    var market_fuel:String?
    var market_total_km:Int?
    var market_colour:String?
    var market_price:Int?
    var market_reg_place:String?
    var market_comments:String?
    var market_address:String?
    var market_contact_no1:String?
    var market_contact_no2:String?
    var market_whatsup_no:String?
    var market_status:Int?
    var market_images:[ImageObject]?
    var md_id:Int?
    var md_market_id:Int?
    var md_transmission:String?
    var md_bluetooth:String?
    var md_radio:String?
    var md_usb_port:String?
    var md_odometer:String?
    var md_cylinders:String?
    var md_seates:String?
    var md_door:String?
    var md_registration_expiry:String?
    var md_engine:String?
    var market_country_code:String?
    var market_sold_status:Int?
}
struct ImageObject:Decodable {
   var id : Int
    var image : String
}
/*
"status": 0,
"meta": {
    "code": 200,
    "message": "success"
},
"message": "success",
"data": [
    {
        "market_id": 1,
        "market_added_by": 13,
        "market_added_name": "abc",
        "market_networkid": 1234567890,
        "market_added_date": "2020-10-13 15:06:57",
        "market_brand": "Maruthi",
        "market_model": "Swift",
        "market_variant": "v6",
        "market_year": "2019",
        "market_type": "sedan",
        "market_fuel": "petrol10",
        "market_total_km": 30000,
        "market_colour": "gray",
        "market_price": 400000,
        "market_reg_place": "KL",
        "market_comments": "\r\nsuch an amazing front view am ever feel",
        "market_address": "shamil shajahan\r\nabc junction\r\nxyz\r\nThrissur",
        "market_contact_no1": "9895010203",
        "market_contact_no2": "995010203",
        "market_whatsup_no": "860010203",
        "market_status": 1,
        "market_images": [
            {
                "id": 3,
                "image": "http://apismartrepair.rosterwand.com/web/upload/asd1.jpg"
            },
            {
                "id": 2,
                "image": "http://apismartrepair.rosterwand.com/web/upload/asd1.jpg"
            },
            {
                "id": 1,
                "image": "http://apismartrepair.rosterwand.com/web/upload/asd1.jpg"
            }
        ],
        "md_id": 1,
        "md_market_id": 1,
        "md_transmission": "Manual",
        "md_bluetooth": "yes",
        "md_radio": "yes",
        "md_usb_port": "yes",
        "md_odometer": "yes",
        "md_cylinders": "yes",
        "md_seates": "asd",
        "md_door": "asd",
        "md_registration_expiry": "asd",
        "md_engine": "K Series VVT Engine"
    }
],
"errors": "error",
"total_items": 1


{
  "market_id": 4,
  "market_added_by": 1,
  "market_added_name": "-",
  "market_networkid": 100,
  "market_added_date": "2021-03-31 13:20:16",
  "market_brand": "Kia",
  "market_model": "Sportage",
  "market_variant": "2018",
  "market_year": "01-10-2018",
  "market_type": "",
  "market_fuel": "Petrol",
  "market_total_km": 186600,
  "market_colour": "Sparkling Silver",
  "market_price": 21000,
  "market_reg_place": "",
  "market_comments": "Finance Available 💰\r\n\r\nCHECK OUT the comparable prices of current Kia Sportage in the images!\r\n\r\nVehicle is being sold through dealership licence LMVD 1008",
  "market_address": "8 Presley St, Stuart Park (6,664.34 km)\r\n0820 Darwin, NT, Australia",
  "market_country_code": "61",
  "market_contact_no1": "8 8981 8402",
  "market_contact_no2": "",
  "market_whatsup_no": "8 8981 8402",
  "market_status": 1,
  "market_sold_status": 1,
  "market_images": [
    {
      "id": 16,
      "image": "http://api.exless.com/web/upload/market/1617196816-pic4.jpg"
    },
    {
      "id": 15,
      "image": "http://api.exless.com/web/upload/market/1617196816-pic3.jpg"
    },
    {
      "id": 14,
      "image": "http://api.exless.com/web/upload/market/1617196816-pic2.jpg"
    },
    {
      "id": 13,
      "image": "http://api.exless.com/web/upload/market/1617196816-pic1.jpg"
    }
  ],
  "md_id": 4,
  "md_market_id": 4,
  "md_transmission": "manual",
  "md_bluetooth": "yes",
  "md_radio": "yes",
  "md_usb_port": "yes",
  "md_odometer": "yes",
  "md_cylinders": "4 cylinder ",
  "md_seates": "5",
  "md_door": "5",
  "md_registration_expiry": "01-10-2025",
  "md_engine": "4cyl 2.0L Petrol"
}
*/
