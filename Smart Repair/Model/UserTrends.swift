
import Foundation
import CoreLocation

class UserTrends:NSObject,CLLocationManagerDelegate{

    var userId: String?
    var deviceId : String?
    var latitude: String?
    var longitude:String?
    var ipAddress:String?
    var startTime:String?
    var endTime:String?
    
    let manager = CouponManager()
     let userDefaults = UserDefaults.standard
    let locationManager = CLLocationManager()
    
    static let sharedInstance = UserTrends()
    
    private override init() {
        
    }
    
    func startTimer(){
       
        self.startTime = getTimeAsString()
    }
    
    func stopTimer(){
        self.endTime = getTimeAsString()
    }
    
    func getTimeAsString()->String{
         let date = Date()
               let dateformatter = DateFormatter()
               dateformatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
               let dateString = dateformatter.string(from: date)
        return dateString
    }
    
    func locationSetup(){
  
                   locationManager.delegate = self
                   locationManager.desiredAccuracy = kCLLocationAccuracyBest
                   locationManager.requestWhenInUseAuthorization()
                   locationManager.startUpdatingLocation()
            
               }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])  {
            
               let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        
               print("locations = \(locValue.latitude) \(locValue.longitude)")
            latitude = String(locValue.latitude)
            longitude = String(locValue.longitude)
        userDefaults.set(latitude, forKey: "latitude")
        userDefaults.set(longitude, forKey: "longitude")
        
        locationManager.stopUpdatingLocation()
           }
    
           func sampleIPTest(){
               let Ipaddress = getIFAddresses()
               print(Ipaddress)
            self.ipAddress = Ipaddress[0]
    
           }
    
           func getIFAddresses() -> [String] {
               var addresses = [String]()
    
               // Get list of all interfaces on the local machine:
               var ifaddr : UnsafeMutablePointer<ifaddrs>?
               guard getifaddrs(&ifaddr) == 0 else { return [] }
               guard let firstAddr = ifaddr else { return [] }
    
               // For each interface ...
               for ptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
                   let flags = Int32(ptr.pointee.ifa_flags)
                   let addr = ptr.pointee.ifa_addr.pointee
    
                   // Check for running IPv4, IPv6 interfaces. Skip the loopback interface.
                   if (flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING) {
                       if addr.sa_family == UInt8(AF_INET) || addr.sa_family == UInt8(AF_INET6) {
    
                           // Convert interface address to a human readable string:
                           var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                           if (getnameinfo(ptr.pointee.ifa_addr, socklen_t(addr.sa_len), &hostname, socklen_t(hostname.count),
                                           nil, socklen_t(0), NI_NUMERICHOST) == 0) {
                               let address = String(cString: hostname)
                               addresses.append(address)
                           }
                       }
                   }
               }
    
               freeifaddrs(ifaddr)
               return addresses
           }
    
    func submitUserTrends(){
        stopTimer()
        locationSetup()
        sampleIPTest()
        userDefaults.set(ipAddress, forKey: "ipAddress")
        userDefaults.set(startTime, forKey: "startTime")
        userDefaults.set(endTime, forKey: "endTime")

    }
    func callTrendsApi(){
        
       if let latitude = userDefaults.string(forKey: "latitude"),
        let longitude = userDefaults.string(forKey: "longitude"),
        let ipAddress = userDefaults.string(forKey: "ipAddress"),
        let startTime = userDefaults.string(forKey: "startTime"),
        let endTime = userDefaults.string(forKey: "endTime"){

        manager.submitUserTrends(latitude: latitude, longitude: longitude, starttime: startTime, endTime: endTime, ipAddress: ipAddress, success: { (success) in
            
            print("Success")
            
        }, failiure: {failed in})

        print("Failed")
        }
        
    }
    
}
