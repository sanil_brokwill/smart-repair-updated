//
//  LogOutData.swift
//  Coupons
//
//  Created by Infrap on 1/6/20.
//  Copyright © 2020 Infrap. All rights reserved.
//

import Foundation
struct LogoutResponse: Decodable{
    let meta: Meta
//    let data:[LogOutData]
//    let dataValue:LogOutData?
}
struct LogOutData:Decodable {
    let message : String
    let id : String
    let session : String
}
//{
//    "message": "Successfully logout.",
//    "id": "4",
//    "session": "4"
//}

struct ForgotResponse: Decodable {
    let status: Int
    let meta: MetaData
    let message: String
    let errors: String
}
struct MetaData: Decodable {
    let code: Int
    let message: String
}

