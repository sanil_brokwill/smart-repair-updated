

import Foundation
import UIKit
import Alamofire

// API

class CouponManager {
//    BASEURL for live= "https://exless.com/api/"
//    BASEURL for local="http://exless-laravel.rosterwand.com/api/"
    static let devUrl = "http://exless-laravel.rosterwand.com/api/"  // dev
    static let liveUrl = "https://exless.com/api/"  // live
    static let mainUrl = liveUrl
    let logInURL = mainUrl + "member_login"
    let logOutURL = mainUrl + "member_logout"
    let couponURL = mainUrl + "coupons"
    let changePasswordURL = mainUrl + "reset_password"
    let forgotURL = mainUrl + "forgot_password"
    let deviceTokenURL = mainUrl + "RegisterDevice.php"
    let addGcmUser = mainUrl + "addgcmuser"
    let userTrends = mainUrl + "session_track"
    let marketURL = mainUrl + "market"
    let OfferURL = mainUrl + "offers"
    let QuoteURL = mainUrl + "add_quote"
    let OfferUpdateURL = mainUrl + "redeem_offer"
    let NewsEventsURL = mainUrl + "news_events"
    let ProfileURL = mainUrl + "member_profile"
    let UpdateProfileURL = mainUrl + "update_profile"
    let UpdateProfileImg = mainUrl + "update_profile_image"
    let bookTowing = mainUrl + "towing_booking"
    let userDefults = UserDefaults.standard
    var userData  = UserData.sharedInstance
    var  productdata : OfferResponse?
    var userEmail = String()
    var sessionId = String()
    var userId = String()
    var password =  String()
    var deviceId = String()
     var fcmToken = String()
    //var labelarray2 = [OfferItem]()

      init() {

        userEmail = userData.userEmail ?? ""
        sessionId = userData.userSession ?? ""
        password = userData.userPassword ?? ""
        deviceId = userData.deviceId ?? ""
        userId = userData.userId ?? ""
          
          
        
        var userd = UserDefaults.standard
        userd.set(userId, forKey: "memberId")
        userd.set(deviceId, forKey: "deviceId")
        userd.set(sessionId, forKey: "sessionId")
          
         // fcmToken = userDefults.stanad
        
       }
    
    func login(email:String,password:String,success:@escaping(_ message:String)->Void,failiure:@escaping(_ message:String)->Void){
        
        let parameters = [
            "email" :email,//email ,
            "password" :password,// password
            "device_id": deviceId
        ]
        
        self.userEmail = email
        print("URL:\(logInURL)")
        print("Login Parameter:\(parameters)")
        
      performRequest(urlString: logInURL, parameters: parameters, success: {data in

            let decoder = JSONDecoder()
        let userDefults = UserDefaults.standard

            do{
            
                let decodedData = try decoder.decode(loginResponse.self, from: data)
                
                if decodedData.meta.code == 200 {
                    
                let loginData = decodedData.data[0]
                    
                    let userDict = ["email":loginData.member_email,"password":parameters["password"]!,"user_id":loginData.member_id,"session":loginData.session_id,"full_name":loginData.member_name] as [String : Any]
                userDefults.set(userDict, forKey: "userDict")
                userDefults.set(true, forKey: "login_status")
                userDefults.set(loginData.member_name, forKey: "full_name")
                UserData.sharedInstance.setLocationIPAndDeviceId()
                success(decodedData.meta.message)
                    
                }else{
                    failiure(decodedData.meta.message)
                }
            }
            catch{
                print("Error:\(error)")
                failiure(error.localizedDescription)
            }
            
        }, failiure: {error in
            print("Error:\(error)")
        })
        
    }
    
    func SavedImage(success:@escaping(_ message:String)->Void,failiure:@escaping(_ message:String)->Void){
        
        let parameters = ["member_id":userId,
                          "session_id":sessionId,
                          "device_id":deviceId]
        
        print("saved\(parameters)")
        performRequest(urlString: QuoteURL, parameters: parameters as! Dictionary<String,String>, success: { data in
            
            let decoder = JSONDecoder()
            let convertString = String(data:data, encoding: String.Encoding.utf8)
            print(convertString)
            
            do{
                
                let decodedData = try decoder.decode(SavedImageResponse.self, from:data)
                
                
                success(decodedData.meta.message)
                if decodedData.meta.code == 200{
                    if decodedData.data.count != 0{

                        print(data)
                        print(decodedData.data)
                           //success(decodedData)
                    }
                }
                
                
            }
                       catch{
                           print("Error:\(error)")
                           //failiure(error.localizedDescription)
                           debugPrint(error)
                       }
            
        }, failiure: {error in
            print("Error:\(error)")
        })
        
    }
    
    func Offer(limit: Int,start: Int,success:@escaping(_ labelarray2:OfferResponse)->Void,failiure:@escaping(_ message:String)->Void){
        
        let userDefults = UserDefaults.standard
        let parameters = [
            "member_id" : userId,
            "session_id" : sessionId,
            "device_id": deviceId,
            "limit":String(limit) ,
            "start":String(start)
            ] as [String : Any]
        
        print("URL:\(OfferURL)")
        print("Coupon fetch Parameter:\(parameters)")
        
        performRequest(urlString:OfferURL , parameters: parameters as! Dictionary<String, Any> as! Dictionary<String, String>, success: {data in
            
            let decoder = JSONDecoder()
            let convertedString = String(data: data, encoding: String.Encoding.utf8)
            
            print(convertedString)
            
            do{
                
                let decodedData = try decoder.decode(OfferResponse.self, from: data)
                if decodedData.meta.code == 200{
                    if decodedData.data.count != 0{
                        success(decodedData)
                    }
                }
                
            }
            catch{
                print("Error:\(error)")
                //failiure(error.localizedDescription)
                
            }
            
        }, failiure: {error in
            print("Error:\(error)")
        })

        
    }
    
    func logOut(success:@escaping(_ message:String)->Void,failiure:@escaping(_ message:String)->Void){
        
        let userDefults = UserDefaults.standard
            let parameters = [ "device_token":deviceId,"member_id":userId,"session_id":sessionId
                ] as [String : Any]
            print("Logout Parameter:\(parameters)")
            performRequest(urlString: logOutURL, parameters: parameters as! Dictionary<String, String>, success: {data in
                
                let decoder = JSONDecoder()

                do{
    
                    let decodedData = try decoder.decode(LogoutResponse.self, from: data)
                    userDefults.set(false, forKey: "login_status")
                    success(decodedData.meta.message)
                }
                catch{
                    print("Error:\(error.localizedDescription)")
                    failiure(error.localizedDescription)
                }
                
            }, failiure: {error in
                print("Error:\(error)")
            })
    }
//     Heading: Get coupons from backend
//      - Parameters: pagination only
//      - Returns : <#Returning Type#>
//      - Note: <#None#>
    
    
    
    func couponFetch(limit: Int,start: Int,success:@escaping(_ couponArray:CouponResponse)->Void,failiure:@escaping(_ message:String)->Void){
        
        let userDefults = UserDefaults.standard
        //            if let userDict = userDefults.dictionary(forKey: "userDict") as? Dictionary<String,String>{
        let parameters = [
            "member_id" : userId,
            "session_id" : sessionId,
            "device_id": deviceId,
            "limit":String(limit) ,
            "start":String(start)
            ] as [String : Any]
        
        print("URL:\(couponURL)")
        print("Coupon fetch Parameter:\(parameters)")
        
        performRequest(urlString:couponURL , parameters: parameters as! Dictionary<String, Any> as! Dictionary<String, String>, success: {data in
            
            let decoder = JSONDecoder()
            let convertedString = String(data: data, encoding: String.Encoding.utf8)
            
            print(convertedString)
            
            do{
                
                let decodedData = try decoder.decode(CouponResponse.self, from: data)
                if decodedData.meta.code == 200{
                    if decodedData.data.count != 0{
                        success(decodedData)
                        
                    }
                }
                
            }
            catch{
                print("Error:\(error)")
                failiure(error.localizedDescription)
                
            }
            
        }, failiure: {error in
            print("Error:\(error)")
        })

    }
    
    func changePassword(newPassword:String,success:@escaping(_ message:String)->Void,failiure:@escaping(_ message:String)->Void){
            
               
                let parameters = [
                               "member_id" : userId,
                                "session_id" : sessionId,
                                "device_id": deviceId,
                                "password": newPassword
 
                    ] as [String : Any]
                print("change password Parameter:\(parameters)")
                performRequest(urlString: changePasswordURL, parameters: parameters as! Dictionary<String, String>, success: {data in
                    
                    let decoder = JSONDecoder()

                    do{
                        //login response contains only meta so don't bother creating new class
                        let decodedData = try decoder.decode(LogoutResponse.self, from: data)
                        success(decodedData.meta.message)
                    }
                    catch{
                        print("Error:\(error.localizedDescription)")
                        failiure(error.localizedDescription)
                    }
                    
                }, failiure: {error in
                    print("Error:\(error)")
                })
                
        }
   
    
    func forgotPassword(email:String,success:@escaping(_ responseAr:ForgotResponse)->Void ,failiure:@escaping(_ message:String)->Void){


                   let parameters = [

                                  "member_email": email

                       ] as [String : Any]
                   print("change password Parameter:\(parameters)")
                   performRequest(urlString: forgotURL, parameters: parameters as! Dictionary<String, String>, success: {data in

                       let decoder = JSONDecoder()

                       do{
                           //login response contains only meta so don't bother creating new class
                           let decodedData = try decoder.decode(ForgotResponse.self, from: data)
                           success(decodedData)
                       }
                       catch{
                           print("Error:\(error.localizedDescription)")
                           failiure(error.localizedDescription)
                       }

                   }, failiure: {error in
                       print("Error:\(error)")
                   })

           }
    
    
    
    func addGcmUser(success:@escaping(_ message:String)->Void,failiure:@escaping(_ message:String)->Void){
        
            let userDefults = UserDefaults.standard
        
        deviceId = userData.deviceId!
        let userIdString = String(userData.userId!)
        
        userEmail = userData.userEmail!
        password = userData.userPassword!
        fcmToken = userDefults.value(forKey: "fcmToken") as! String
    
                let parameters = [
                                    "device_token":fcmToken,
                                   "device_type":"ios",
                                   "member_id": userIdString,
                                   "phone_id": deviceId,
                                   "longitude" : userData.longitude ?? "",
                                   "latitude" : userData.latitude ?? "",
                                   "ipaddress" :userData.ipAddress ?? ""
                    ]
        
                print("Device Token Parameter:\(parameters)")
                
                performRequest(urlString: addGcmUser, parameters: parameters as! Dictionary<String, String>, success: {data in
                    
                    let decoder = JSONDecoder()

                    do{
                        //login response contains only meta so don't bother creating new class
                        let decodedData = try decoder.decode(LogoutResponse.self, from: data)
                        success(decodedData.meta.message)
                        
                    }
                    catch{
                        print("Error:\(error.localizedDescription)")
                        failiure(error.localizedDescription)
                    }
                    
                }, failiure: {error in
                    print("Error:\(error)")
                })

        }
  
    
    func OfferUpdate(offer_id: String,plan_id: String,success:@escaping(_ message:String)->Void,failiure:@escaping(_ message:String)->Void){
        
        let params :[String:Any]? = ["member_id":userId,"session_id": sessionId,"device_id":deviceId,"offer_id":offer_id,"plan_id":plan_id]
        print("url = \(OfferUpdateURL)")
        print("offerUpdate Params:\(String(describing: params))")
        performRequest(urlString: OfferUpdateURL, parameters: params as! Dictionary<String, String>, success: {data in
                        
                        let decoder = JSONDecoder()

                        do{
                            //login response contains only meta so don't bother creating new class
                            let decodedData = try decoder.decode(OfferUpdaateResponse.self, from: data)
                           
                            success(decodedData.meta!.message)
                        }
                        catch{
                            print("Error:\(error.localizedDescription)")
                            //failiure(error.localizedDescription)
                        }
                        
                    }, failiure: {error in
                        print("Error:\(error)")
                    })
    
    }
    
    
    func ProfileUpdate(member_name: String,success:@escaping(_ message:String)->Void,failiure:@escaping(_ message:String)->Void){
        
        let params :[String:Any]? = ["member_id":userId,"session_id": sessionId,"device_id":deviceId,"member_name":member_name]
        print("url = \(UpdateProfileURL)")
        print("ProfileUpdate Params:\(String(describing: params))")
        performRequest(urlString: UpdateProfileURL, parameters: params as! Dictionary<String, String>, success: {data in
                        
                        let decoder = JSONDecoder()

                        do{
                            //login response contains only meta so don't bother creating new class
                            let decodedData = try decoder.decode(ProfileUpdateResponse.self, from: data)
                           
                            success(decodedData.meta.message)
                        }
                        catch{
                            print("Error:\(error.localizedDescription)")
                            //failiure(error.localizedDescription)
                        }
                        
                    }, failiure: {error in
                        print("Error:\(error)")
                    })
    
    }
    
    func submitUserTrends(latitude:String,longitude:String,starttime:String,endTime:String,ipAddress:String,success:@escaping(_ message:String)->Void,failiure:@escaping(_ message:String)->Void){
        
        // func getMovieIdFromAPI(imdbID: String, completionHandler: @escaping (User) -> () ) {
        //  let requestURL = "https://www.omdbapi.com/?i=\(imdbID)"
        guard let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String else {return}
      
         let userDefaults = UserDefaults.standard
        let params: [String:Any]? = [

             "member_id" : userId,
             "session_id" : sessionId,
             "device_id":  deviceId ,
             "session_starts":starttime,
             "session_ends":endTime,
             "latitude":latitude,
             "longitude":longitude,
             "ipaddress":ipAddress,
             "app_version": appVersion,
             "device_type": "iOS"

        ]
         
        print("url = \(userTrends)")
        print("UserTrend Params:\(String(describing: params))")
        
     performRequest(urlString: userTrends, parameters: params as! Dictionary<String, String>, success: {data in
                     
                     let decoder = JSONDecoder()

                     do{
                         //login response contains only meta so don't bother creating new class
                         let decodedData = try decoder.decode(LogoutResponse.self, from: data)
                        
                         success(decodedData.meta.message)
                     }
                     catch{
                         print("Error:\(error.localizedDescription)")
                         failiure(error.localizedDescription)
                     }
                     
                 }, failiure: {error in
                     print("Error:\(error)")
                 })
    }

    func getMarketList(limit:Int,start:Int, success:@escaping( [MarketItem])->Void,failiure:@escaping(_ message:String)->Void){
               
                  
                   let parameters = [
                                "limit":String(limit),
                                "session_id":sessionId,
                                "start":String(start),
                                "member_id":userId,
                                "device_id": deviceId
                                     
                       ] as [String : Any]
                print(marketURL)
                   print("marketparameters:\(parameters)")
        
        performRequest(urlString: marketURL, parameters: parameters as! Dictionary<String, String>, success: {data in
                       
                       let decoder = JSONDecoder()

                       do{
                          
                           let decodedData = try decoder.decode(MarketItemResponse.self, from: data)
                        success(decodedData.data)
                        
                        
                       }
                       catch{
                           print("Error:\(error)")
                           failiure(error.localizedDescription)
                       }
                       
                   }, failiure: {error in
                       print("Error:\(error)")
                   })
                   
           }

    

    
    func getNewsEvents(limit: Int,start: Int,success:@escaping(_ labelarray2:NewsEventsResponse)->Void,failiure:@escaping(_ message:String)->Void){
        
        let userDefults = UserDefaults.standard
        let parameters = [
            "member_id" : userId,
            "session_id" : sessionId,
            "device_id": deviceId,
            "limit":String(limit) ,
            "start":String(start)
            ] as [String : Any]
        
        print("URL:\(NewsEventsURL)")
        print("Coupon fetch Parameter:\(parameters)")
        
        performRequest(urlString:NewsEventsURL , parameters: parameters as! Dictionary<String, Any> as! Dictionary<String, String>, success: {data in
            
            let decoder = JSONDecoder()
            let convertedString = String(data: data, encoding: String.Encoding.utf8)
            
            print(convertedString)
            
            do{
                
                let decodedData = try decoder.decode(NewsEventsResponse.self, from: data)
                if decodedData.meta.code == 200{
                    if decodedData.data.count != 0{
                        success(decodedData)
                    }
                }
                
            }
            catch{
                print("Error:\(error)")
                //failiure(error.localizedDescription)
                
            }
            
        }, failiure: {error in
            print("Error:\(error)")
        })

        
    }
    
    
    
    func getProfiledata(success:@escaping(_ profilearray:ProfileResponse)->Void,failiure:@escaping(_ message:String)->Void){
        
        let userde = UserDefaults.standard
        let parameters = ["member_id" : userId,
                          "session_id" : sessionId,
                          "device_id":deviceId]
        
        performRequest(urlString:ProfileURL , parameters: parameters as! Dictionary<String, Any> as! Dictionary<String, String>, success: {data in
            let decoder = JSONDecoder()
            let convertedString = String(data: data, encoding: String.Encoding.utf8)
            
            print(convertedString)
            
            do{
                let decodeData = try decoder.decode(ProfileResponse.self, from: data)
                
                if decodeData.meta.code == 200 {
                  if decodeData.data.count != 0{
                       success(decodeData)
                   }
                        
                }
                
                
            }
            
            catch{
                print("Error:\(error)")
                
            }
            
            
        }, failiure: {error in
            print("Error:\(error)")
        })

        
    }
    
    
func performRequest(urlString:String,parameters:Dictionary<String,String>,success:@escaping(_ data:Data)->Void,failiure:@escaping(_ error:Error)->Void){
        
        if let url = URL(string: urlString){
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: [])else{return}
            request.httpBody = httpBody
            let session = URLSession(configuration: .default)
            
            let task =  session.dataTask(with: request) { (data, response, error) in
                
                    
                    if error != nil{
                        failiure(error!)
                        return
                    }
                    if let safeData = data{
                        success(safeData)
                        
                    }
                
            }
            task.resume()
        }
        
    }
    
    func createDataBody(withParameters params: [String: String]?, media: [Media]?, boundary: String) -> Data {

        let lineBreak = "\r\n"
        var body = Data()

        if let parameters = params {
            for (key, value) in parameters {
                body.append("--\(boundary + lineBreak)")
                body.append("Content-Disposition: form-data; name=\"\(key)\"\(lineBreak + lineBreak)")
                body.append("\(value + lineBreak)")
            }
        }

        if let media = media {
            for photo in media {
                body.append("--\(boundary + lineBreak)")
                body.append("Content-Disposition: form-data; name=\"\(photo.key)\"; filename=\"\(photo.fileName)\"\(lineBreak)")
                body.append("Content-Type: \(photo.mimeType + lineBreak + lineBreak)")
                body.append(photo.data)
                body.append(lineBreak)
            }
        }

        body.append("--\(boundary)--\(lineBreak)")

        return body
    }
    func bookTowing(latitude : String, longitude : String,success:@escaping(_ message:String)->Void,failiure:@escaping(_ message:String)->Void){

        let parameters = [
            "member_id": "1",
            "towing_id": "0",
            "cust_lati": "37.7749",
            "cust_longi": "-122.4194",
            "remarks": "remarks",
            "session_id": "AC2PNfNqIW",
            "device_id": "A7C3551F-B5C9-4490-BF0B-3D602801CC4C"
        ] 
        print("saved\(parameters)")
        performRequest(urlString:bookTowing , parameters: parameters as! Dictionary<String, Any> as! Dictionary<String, String>, success: {data in
            let decoder = JSONDecoder()
            let convertedString = String(data: data, encoding: String.Encoding.utf8)
            
            print(convertedString)
            
            do{
                let decodeData = try decoder.decode(ProfileResponse.self, from: data)
                
//                if decodeData.meta.code == 200 {
//                  if decodeData.data.count != 0{
//                       success(decodeData)
//                   }
//
//                }
                
                
            }
            
            catch{
                print("Error:\(error)")
                
            }
            
            
        }, failiure: {error in
            print("Error:\(error)")
        })

        
        
        
        
        
//        performRequest(urlString:"http://exless-laravel.rosterwand.com/api/towing_booking",parameters: parameters as! Dictionary<String,String> , success: {data in
//
//                        let decoder = JSONDecoder()
//
//                        do{
//                            let convertedString = String(data: data, encoding: String.Encoding.utf8)
//
//                                        print(convertedString)
//                            //login response contains only meta so don't bother creating new class
//                            let decodedData = try decoder.decode(TowingBook.self, from: data)
//
//                            success(decodedData.meta.message)
//                        }
//                        catch{
//                            print("Error:\(error.localizedDescription)")
//                            //failiure(error.localizedDescription)
//                        }
//
//                    }, failiure: {error in
//                        print("Error:\(error)")
//                    })

    }
    
    func bookTowing1(latitude : String, longitude : String, completion: @escaping(Result<ProfileResponse,Error>)-> ()) {
        guard let url =  URL(string: bookTowing)
        else {
            return     }
        let body: [String: Any] =
        [
            "member_id": "1",
                        "towing_id": "0",
                        "cust_lati": "37.7749",
                        "cust_longi": "-122.4194",
                        "remarks": "remarks",
                        "session_id": "sKowKxB41H",
                        "device_id": "9EE60713-DDBC-44F9-B6B5-099DD110812D"
        ]
        let finalBody = try? JSONSerialization.data(withJSONObject: body)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = finalBody
        request.allHTTPHeaderFields = ["Token":"gopaddi@v1"]
        URLSession.shared.dataTask(with: request){
            (data , response, error)  in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                completion(.failure(error?.localizedDescription as! Error))
                return
            }
            guard let response = response else {
                return
            }
            print(response)
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if let responseJSON = responseJSON as? [String: Any] {
                do {
                    let postBody = try JSONDecoder().decode(ProfileResponse.self, from: data)
                    completion(.success(postBody))
                }catch let error{
                    print(error.localizedDescription)
                }
                print(responseJSON)
            }
        }.resume()
    }
//
//    func bookTowing(latitude : String, longitude : String,success:@escaping(_ message:String)->Void,failiure:@escaping(_ message:String)->Void){
//        
//        let userde = UserDefaults.standard
//        let parameters = [
//            "member_id":userId,
//            "towing_id": "0",
//            "cust_lati": latitude,
//            "cust_longi": longitude,
//            "remarks": "remarks",
//            "session_id":sessionId,
//            "device_id":deviceId
//        ]
//        
//        performRequest(urlString:bookTowing , parameters: parameters as Dictionary<String, String> as! Dictionary<String, String>, success: {data in
//            let decoder = JSONDecoder()
//            let convertedString = String(data: data, encoding: String.Encoding.utf8)
//            
//            print(convertedString)
//            
//            do{
//                let decodeData = try decoder.decode(TowingBook.self, from: data)
//                print(decodeData)
////                if decodeData.meta.code == 200 {
////                  if decodeData.data.count != 0{
////                       success(decodeData)
////                   }
////
////                }
//                
//                
//            }
//            
//            catch{
//                print("Error:\(error)")
//                
//            }
//            
//            
//        }, failiure: {error in
//            print("Error:\(error)")
//        })
//
//        
//    }

}




