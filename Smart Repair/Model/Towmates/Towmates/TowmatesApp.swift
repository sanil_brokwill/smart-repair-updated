//
//  TowmatesApp.swift
//  Towmates
//
//  Created by Admin on 27/7/2023.
//

import SwiftUI

@main
struct TowmatesApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
