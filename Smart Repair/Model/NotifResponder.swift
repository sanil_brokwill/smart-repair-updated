//
//  NotifResponder.swift
//  Coupons
//
//  Created by Infrap on 4/6/20.
//  Copyright © 2020 Infrap. All rights reserved.
//

import Foundation
final class notifResponder{
    static let sharedInstance = notifResponder()
    var isFromNotificationGlobal = Bool()
    var notificationSenderIdGlobal = String()
    var notificationSenderTypeId = String()
    var isFromTerminatedState = Bool()
    var remoteNotif = [String:Any]()
    private init(){}
}
