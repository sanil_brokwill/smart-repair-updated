////
///
//
//  AppDelegate.swift
//  Coupons
//
//  Created by Infrap on 18/5/20.
//  Copyright © 2020 Infrap. All rights reserved.
//

import UIKit
import UserNotifications
import Firebase
import Messages
import FirebaseCrashlytics


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,MessagingDelegate {

var window: UIWindow?
var userData = UserData.sharedInstance
    
let gcmMessageIDKey = "gcm.message_id"
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        userData.setLocationIPAndDeviceId()
        print(userData.setLocationIPAndDeviceId())
        Messaging.messaging().delegate = self
        if #available(iOS 10.0, *) {
              // For iOS 10 display notification (sent via APNS)
              UNUserNotificationCenter.current().delegate = self
              
              let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
              UNUserNotificationCenter.current().requestAuthorization(
                  options: authOptions,
                  completionHandler: {_, _ in })
              
          } else {
              let settings: UIUserNotificationSettings =
                  UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
              application.registerUserNotificationSettings(settings)
          }
         application.registerForRemoteNotifications()
        
        let userLoginStatus = UserDefaults.standard.bool(forKey: "login_status")
        self.window = UIWindow(frame: UIScreen.main.bounds)

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if userLoginStatus{
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "MainNavView")
            self.window?.rootViewController = initialViewController

            self.window?.makeKeyAndVisible()
        }else{
                    let initialViewController = storyboard.instantiateViewController(withIdentifier: "loginView")
            self.window?.rootViewController = initialViewController
 

        }

         self.window?.makeKeyAndVisible()
        getDeviceId()
        return true
    }
    
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
           print("Firebase registration token: \(fcmToken)")
        
             
           
           let dataDict:[String: String] = ["token": fcmToken]
           NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
           let userDefaults = UserDefaults.standard
           userDefaults.set(fcmToken, forKey: "fcmToken")
           
       }
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print(fcmToken)
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
           let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
                   let token = tokenParts.joined()
                   print("Device Token: \(token)")
        
             Messaging.messaging().apnsToken = deviceToken as Data
        
       
        
       }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                        fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
         
           
           // Print message ID.
           if let messageID = userInfo[gcmMessageIDKey] {
               print("Message ID: \(messageID)")
           }
           print(userInfo)

           // Print full message.
           
           print(userInfo)
        
        
           let state = application.applicationState
           
           if (state == .active) {
            
                   
               
           } else {
                  let userInfoDic = userInfo as! [String:Any]
               
               
           }

           completionHandler(UIBackgroundFetchResult.newData)
       }
    
    
    
       func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
           print("Unable to register for remote notifications: \(error.localizedDescription)")
        
       }
    func applicationWillResignActive(_ application: UIApplication) {
       UserTrends.sharedInstance.submitUserTrends()
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
        UserTrends.sharedInstance.callTrendsApi()
        UserTrends.sharedInstance.startTimer()
    }
    func getDeviceId(){
        let identifier = UIDevice.current.identifierForVendor?.uuidString
        let userDefaults = UserDefaults.standard
        let token = String(identifier!)
        userDefaults.set(token, forKey: "DeviceToken")
                print("Device Token: \(token)")
        print("DeviceID:\(String(describing: identifier))")
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
      
       // NotificationCenter.default.post(name: NSNotification.Name(identifier), object: nil)
      let userInfo = response.notification.request.content.userInfo
      // Print message ID.
      if let messageID = userInfo[gcmMessageIDKey] {
        print("Message ID: \(messageID)")
          
//          let Title = userInfo[] as? String ?? ""
//             print(Title)
          
      }
        
        //notification
       
        var testnotify = userInfo["gcm.notification.type"] as? String
        
        switch testnotify
             {
              case "News":
               NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SecondTypeNotification"), object: userInfo["gcm.notification.title"] as? String, userInfo: userInfo)
               break
              case "Coupon":
               NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ThirdTypeNotification"), object: userInfo["gcm.notification.title"] as? String, userInfo: userInfo)
               break
            
           case "Market":
           NotificationCenter.default.post(name: NSNotification.Name(rawValue: "FourthTypeNotification"), object: userInfo["gcm.notification.title"] as? String, userInfo: userInfo)
           break
            
        case "Price":
         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "FifthTypeNotification"), object: userInfo["gcm.notification.title"] as? String, userInfo: userInfo)
         break
               default:
                break
             }

        
//        if userInfo["gcm.notification.type"] as? String == "News"
//        {
//
////            guard let window = UIApplication.shared.keyWindow else { return }
////
////            let storyboard = UIStoryboard(name: "Main", bundle: nil)
////            let vc: NewsandEventsTVC = storyboard.instantiateViewController(withIdentifier: "newsandevents") as! NewsandEventsTVC
////
////            let navController = UINavigationController(rootViewController: vc)
////            navController.modalPresentationStyle = .fullScreen
////
////            window.rootViewController = navController
////
////
////
////                window.makeKeyAndVisible()
////
//
//            let sb = UIStoryboard(name: "Main", bundle: nil)
//
//            let otherVC = sb.instantiateViewController(withIdentifier: "newsandevents") as! NewsandEventsTVC
//            // (actually, you don't need to cast to MessageViewController here;
//            // the returned reference already is guaranteed to at least be a
//            // UIViewController instance; you don't need more specificity in this
//            // case --i.e, embed in navigation)
//
//            let navigation = UINavigationController(rootViewController: otherVC)
//            window?.rootViewController = navigation
//
//
//
////            if  let conversationVC = storyboard1.instantiateViewController(withIdentifier: "newsandevents") as? NewsandEventsTVC,
////                   let tabBarController = self.window?.rootViewController as? UITabBarController,
////                   let navController = tabBarController.selectedViewController as? UINavigationController {
////
////                       // we can modify variable of the new view controller using notification data
////                       // (eg: title of notification)
////
////                       conversationVC.senderDisplayName = response.notification.request.content.title
////                       // you can access custom data of the push notification by using userInfo property
////                       // response.notification.request.content.userInfo
////                       navController.pushViewController(conversationVC, animated: true)
////               }
//
//        }
//
//       else if userInfo["gcm.notification.type"] as? String == "Price"
//        {
//
//            guard let window = UIApplication.shared.keyWindow else { return }
//
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let vc: MarketListVC = storyboard.instantiateViewController(withIdentifier: "MarketList") as! MarketListVC
//            let navController = UINavigationController(rootViewController: vc)
//            navController.modalPresentationStyle = .fullScreen
//
//            window.rootViewController = navController
//                window.makeKeyAndVisible()
//
//        }
//
//
//        else if userInfo["gcm.notification.type"] as? String == "Market"
//        {
//
//            guard let window = UIApplication.shared.keyWindow else { return }
//
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let vc: MarketListVC = storyboard.instantiateViewController(withIdentifier: "MarketList") as! MarketListVC
//            let navController = UINavigationController(rootViewController: vc)
//            navController.modalPresentationStyle = .fullScreen
//
//            window.rootViewController = navController
//                window.makeKeyAndVisible()
//
//        }
//
//       else if userInfo["gcm.notification.type"] as? String == "Coupon"
//        {
//
//            guard let window = UIApplication.shared.keyWindow else { return }
//
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let vc: HomeViewController = storyboard.instantiateViewController(withIdentifier: "homeView") as! HomeViewController
//            let navController = UINavigationController(rootViewController: vc)
//           navController.modalPresentationStyle = .fullScreen
//
//            window.rootViewController = navController
//                window.makeKeyAndVisible()
//
//        }
//
//
//
//
 

      completionHandler()
    }
  }
    

////  AppDelegate.swift
////  Coupons
////
////  Created by Infrap on 18/5/20.
////  Copyright © 2020 Infrap. All rights reserved.
////
//
//import UIKit
//import UserNotifications
//import Firebase
//import Messages
//import FirebaseCrashlytics
//
//
//@UIApplicationMain
//class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,MessagingDelegate {
//
//var window: UIWindow?
//var userData = UserData.sharedInstance
//
//let gcmMessageIDKey = "gcm.message_id"
//    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
//        FirebaseApp.configure()
//        userData.setLocationIPAndDeviceId()
//        print(userData.setLocationIPAndDeviceId())
//        Messaging.messaging().delegate = self
//        if #available(iOS 10.0, *) {
//              // For iOS 10 display notification (sent via APNS)
//              UNUserNotificationCenter.current().delegate = self
//
//              let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
//              UNUserNotificationCenter.current().requestAuthorization(
//                  options: authOptions,
//                  completionHandler: {_, _ in })
//
//          } else {
//              let settings: UIUserNotificationSettings =
//                  UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//              application.registerUserNotificationSettings(settings)
//          }
//         application.registerForRemoteNotifications()
//
//        let userLoginStatus = UserDefaults.standard.bool(forKey: "login_status")
//        self.window = UIWindow(frame: UIScreen.main.bounds)
//
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        if userLoginStatus{
//        let initialViewController = storyboard.instantiateViewController(withIdentifier: "MainNavView")
//            self.window?.rootViewController = initialViewController
//
//            self.window?.makeKeyAndVisible()
//        }else{
//                    let initialViewController = storyboard.instantiateViewController(withIdentifier: "loginView")
//            self.window?.rootViewController = initialViewController
//
//
//        }
//
//         self.window?.makeKeyAndVisible()
//        getDeviceId()
//        return true
//    }
//
//
//    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
//           print("Firebase registration token: \(fcmToken)")
//           let dataDict:[String: String] = ["token": fcmToken]
//           NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
//           let userDefaults = UserDefaults.standard
//           userDefaults.set(fcmToken, forKey: "fcmToken")
//
//       }
//    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
//        print(fcmToken)
//    }
//    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//           let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
//                   let token = tokenParts.joined()
//                   print("Device Token: \(token)")
//
//             Messaging.messaging().apnsToken = deviceToken as Data
//       }
//    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
//                        fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
//
//
//           // Print message ID.
//           if let messageID = userInfo[gcmMessageIDKey] {
//               print("Message ID: \(messageID)")
//           }
//           print(userInfo)
//
//           let state = application.applicationState
//
//           if (state == .active) {
//           } else {
//                  let userInfoDic = userInfo as! [String:Any]
//
//           }
//
//           completionHandler(UIBackgroundFetchResult.newData)
//       }
//
//       func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
//           print("Unable to register for remote notifications: \(error.localizedDescription)")
//
//       }
//    func applicationWillResignActive(_ application: UIApplication) {
//       UserTrends.sharedInstance.submitUserTrends()
//    }
//    func applicationDidBecomeActive(_ application: UIApplication) {
//        UserTrends.sharedInstance.callTrendsApi()
//        UserTrends.sharedInstance.startTimer()
//    }
//    func getDeviceId(){
//        let identifier = UIDevice.current.identifierForVendor?.uuidString
//        let userDefaults = UserDefaults.standard
//        let token = String(identifier!)
//        userDefaults.set(token, forKey: "DeviceToken")
//                print("Device Token: \(token)")
//        print("DeviceID:\(String(describing: identifier))")
//
//    }
//    func userNotificationCenter(_ center: UNUserNotificationCenter,
//                                didReceive response: UNNotificationResponse,
//                                withCompletionHandler completionHandler: @escaping () -> Void) {
//
//       // NotificationCenter.default.post(name: NSNotification.Name(identifier), object: nil)
//      let userInfo = response.notification.request.content.userInfo
//      // Print message ID.
//      if let messageID = userInfo[gcmMessageIDKey] {
//        print("Message ID: \(messageID)")
//
////          let Title = userInfo[] as? String ?? ""
////             print(Title)
//
//      }
//
//        //notification
//
//        var testnotify = userInfo["gcm.notification.type"] as? String
//
//        switch testnotify
//             {
//              case "News":
//               NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SecondTypeNotification"), object: userInfo["gcm.notification.title"] as? String, userInfo: userInfo)
//               break
//              case "Coupon":
//               NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ThirdTypeNotification"), object: userInfo["gcm.notification.title"] as? String, userInfo: userInfo)
//               break
//
//           case "Market":
//           NotificationCenter.default.post(name: NSNotification.Name(rawValue: "FourthTypeNotification"), object: userInfo["gcm.notification.title"] as? String, userInfo: userInfo)
//           break
//
//        case "Price":
//         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "FifthTypeNotification"), object: userInfo["gcm.notification.title"] as? String, userInfo: userInfo)
//         break
//               default:
//                break
//             }
//
//
////        if userInfo["gcm.notification.type"] as? String == "News"
////        {
////
//////            guard let window = UIApplication.shared.keyWindow else { return }
//////
//////            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//////            let vc: NewsandEventsTVC = storyboard.instantiateViewController(withIdentifier: "newsandevents") as! NewsandEventsTVC
//////
//////            let navController = UINavigationController(rootViewController: vc)
//////            navController.modalPresentationStyle = .fullScreen
//////
//////            window.rootViewController = navController
//////
//////
//////
//////                window.makeKeyAndVisible()
//////
////
////            let sb = UIStoryboard(name: "Main", bundle: nil)
////
////            let otherVC = sb.instantiateViewController(withIdentifier: "newsandevents") as! NewsandEventsTVC
////            // (actually, you don't need to cast to MessageViewController here;
////            // the returned reference already is guaranteed to at least be a
////            // UIViewController instance; you don't need more specificity in this
////            // case --i.e, embed in navigation)
////
////            let navigation = UINavigationController(rootViewController: otherVC)
////            window?.rootViewController = navigation
////
////
////
//////            if  let conversationVC = storyboard1.instantiateViewController(withIdentifier: "newsandevents") as? NewsandEventsTVC,
//////                   let tabBarController = self.window?.rootViewController as? UITabBarController,
//////                   let navController = tabBarController.selectedViewController as? UINavigationController {
//////
//////                       // we can modify variable of the new view controller using notification data
//////                       // (eg: title of notification)
//////
//////                       conversationVC.senderDisplayName = response.notification.request.content.title
//////                       // you can access custom data of the push notification by using userInfo property
//////                       // response.notification.request.content.userInfo
//////                       navController.pushViewController(conversationVC, animated: true)
//////               }
////
////        }
////
////       else if userInfo["gcm.notification.type"] as? String == "Price"
////        {
////
////            guard let window = UIApplication.shared.keyWindow else { return }
////
////            let storyboard = UIStoryboard(name: "Main", bundle: nil)
////            let vc: MarketListVC = storyboard.instantiateViewController(withIdentifier: "MarketList") as! MarketListVC
////            let navController = UINavigationController(rootViewController: vc)
////            navController.modalPresentationStyle = .fullScreen
////
////            window.rootViewController = navController
////                window.makeKeyAndVisible()
////
////        }
////
////
////        else if userInfo["gcm.notification.type"] as? String == "Market"
////        {
////
////            guard let window = UIApplication.shared.keyWindow else { return }
////
////            let storyboard = UIStoryboard(name: "Main", bundle: nil)
////            let vc: MarketListVC = storyboard.instantiateViewController(withIdentifier: "MarketList") as! MarketListVC
////            let navController = UINavigationController(rootViewController: vc)
////            navController.modalPresentationStyle = .fullScreen
////
////            window.rootViewController = navController
////                window.makeKeyAndVisible()
////
////        }
////
////       else if userInfo["gcm.notification.type"] as? String == "Coupon"
////        {
////
////            guard let window = UIApplication.shared.keyWindow else { return }
////
////            let storyboard = UIStoryboard(name: "Main", bundle: nil)
////            let vc: HomeViewController = storyboard.instantiateViewController(withIdentifier: "homeView") as! HomeViewController
////            let navController = UINavigationController(rootViewController: vc)
////           navController.modalPresentationStyle = .fullScreen
////
////            window.rootViewController = navController
////                window.makeKeyAndVisible()
////
////        }
////
////
////
////
//
//
//      completionHandler()
//    }
//  }
//
