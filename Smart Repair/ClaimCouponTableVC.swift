//
//  ClaimCouponTableVC.swift
//  Smart Repair
//
//  Created by Infrap on 16/9/21.
//  Copyright © 2021 Infrap. All rights reserved.
//

import UIKit
import Alamofire
import Reachability

//Claim Coupon
var  imagearray = [UIImage(named: "frontmirrorcar"), UIImage(named: "frontsidecar2"), UIImage(named: "rightcar1"), UIImage(named: "backsidecar1"),UIImage(named: "frontcar1"), UIImage(named: "backcar1"), UIImage(named: "leftcar1"), UIImage(named: "frontsidecar1")]



//var  imagearray = [UIImage(named: "frontcar1"), UIImage(named: "backcar1"), UIImage(named: "rightcar1"), UIImage(named: "leftcar1"),UIImage(named: "frontmirrorcar"), UIImage(named: "frontsidecar1"), UIImage(named: "frontsidecar2"), UIImage(named: "backsidecar1")]

var iconarray = [UIImage(named: "aircon regassIcons_black-2"), UIImage(named: "buff and polish Icons_black-3"), UIImage(named: "headlight"), UIImage(named: "aircon regassIcons_black-2"),UIImage(named: "aircon regassIcons_black-2")]
var imagearray1 = [UIImage?]()
class ClaimCouponTableVC: UITableViewController {
    @IBOutlet var ClaimTableView: UITableView!
    var flag = 0
    let mem = UserDefaults.standard.value(forKey: "memberId")
    let sess = UserDefaults.standard.value(forKey: "sessionId")
    let device = UserDefaults.standard.value(forKey: "deviceId")
     var startValue = 0
    var limit  = 20
    private var manager = CouponManager()

    var  productdata : OfferResponse?

    var labelarray = [OfferItem]()
    
    var Textarray = ["Buffing or polishing a vehicle correctly allows you to take away or remove a number of defects", "Towing is coupling two or more objects together so that they may be pulled by a designated power source or sources." , "Buffing or polishing a vehicle correctly allows you to take away or remove a number of defects.","Buffing or polishing a vehicle correctly allows you to take away or remove a number of defects.","Buffing or polishing a vehicle correctly allows you to take away or remove a number of defects."]
    
    private var pullControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if(labelarray.isEmpty)
//        {
//            let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
//                label.center = CGPoint(x: 160, y: 285)
//                label.textAlignment = .center
//                label.text = "I'm a test label"
//
//                self.view.addSubview(label)
//        }
        
        loadClaim()
        
        pullControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
          pullControl.addTarget(self, action: #selector(refreshListData(_:)), for: .valueChanged)
          if #available(iOS 10.0, *) {
              ClaimTableView.refreshControl = pullControl
          } else {
              ClaimTableView.addSubview(pullControl)
          }
        
    }
    
    @objc func refreshListData(_ sender: Any) {
        //  your code to reload tableView
       // labelarray.removeAll()
        //loadClaim()
        
        self.pullControl.endRefreshing()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.separatorColor = .clear
        self.tableView.allowsSelection = true
        //loadClaim()
        print(labelarray)
        
//        if(labelarray.isEmpty)
//        {
//            let alert = UIAlertController(title: "Oops!", message: "There is no offers to show", preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
//        }
//
        
        
        //Quote()
        tryInternet()
    }
    func tryInternet(){
        let reachability = try! Reachability()
        reachability.whenReachable = { reachability in
            if reachability.connection == .wifi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        }
        reachability.whenUnreachable = { _ in
            print("Not reachable")
            
            let alert = UIAlertController(title: "Oops!", message: "Check your internet Connection", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func loadClaim(){
        
        manager.Offer(limit: limit, start: startValue, success: { [self]
            (OfferResponse) in
            
            self.labelarray.removeAll()
            
            print("Success????")
            let offerData = OfferResponse.data
            
            self.labelarray.append(contentsOf: offerData)
            
            DispatchQueue.main.async {
                
                self.ClaimTableView.reloadData()
            }
        
            
        }, failiure: {message in
            
            print(message)
            
        })
        
        
    }
    func Quote(){
        manager.SavedImage(success: {
            response in
            
            print(response)
        }, failiure: {message in
            
            print(message)
        })
    }
   
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return labelarray.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(withIdentifier: "cellclaim", for: indexPath)as! ClaimTableViCell

       // cell.RedeemButton.titleLabel?.font = UIFont(name: "GillSans-Italic", size: 17)
        cell.RedeemButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        
        let couponData = labelarray[indexPath.row]
        cell.labeldata.text = couponData.offer
        
        if(couponData.offer_id == 1)
        {
            cell.iconimage?.image = UIImage(named: "headlight")
        }
        else if(couponData.offer_id == 2)
        {
            cell.iconimage?.image = UIImage(named: "buff and polish Icons_black-3")
        }
        else if(couponData.offer_id == 3)
        {
            cell.iconimage?.image = UIImage(named: "aircon regassIcons_black-2")
        }
//        else
//
//        {
//            cell.iconimage?.image = UIImage(named: "aircon regassIcons_black-2")
//        }
        
       // cell.iconimage?.image = iconarray[indexPath.row]
        
        cell.RedeemButton.tag = indexPath.row
    
        cell.RedeemButton.addTarget(self, action: #selector(ClaimCouponTableVC.btnAction(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func loadData() {
        // code to load data from network, and refresh the interface
        ClaimTableView.reloadData()
    }

    @objc func btnAction(_ sender: AnyObject) {
        
        let buttonTag = sender.tag

        let offerdata = labelarray[buttonTag!]
        
        if(offerdata.offer_included == 0)
        {
            let alert = UIAlertController(title: "Sorry!", message: "Please Upgrade your Plan to redeem the offer", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
        
        else
        {

        if(offerdata.offer_applicable == 0)
        {

            let alert = UIAlertController(title: "Sorry!", message: "\(offerdata.offer) will be available from\(offerdata.offer_start_date) ", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
        else if (offerdata.offer_applicable == 1)
        {
            
            if(offerdata.offer_used == 0){
                
                for i in 0..<labelarray.count {
                    
                    if(i == buttonTag)
                    {
                       flag = 0
                    }
                }
                
                if(flag == 0){
                OfferUpdate(offerId: String(offerdata.offer_id), planId: offerdata.plan_id)
                loadClaim()
                }
                
                else
                {
                    let alert = UIAlertController(title: "Oops", message: "Already Coupon is used", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                }
                
            }
            
            if(offerdata.offer_used == 1)
            {
                let alert = UIAlertController(title: "Oops", message: "Already Coupon is used", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            }
            
        }
            
     
        }
           var position: CGPoint = sender.convert(.zero, to: self.tableView)
           let indexPath = self.tableView.indexPathForRow(at: position)
          
   }
    func OfferUpdate(offerId:String,planId:String)
    {
    
        let alert = UIAlertController(title: "Confirm!", message: "Would you like to continue to Claim the Coupon?", preferredStyle: UIAlertController.Style.alert)
        self.labelarray.removeAll()
               // add the actions (buttons)
               alert.addAction(UIAlertAction(title: "Continue", style: UIAlertAction.Style.default, handler: { action in
                   
                
                   self.manager.OfferUpdate(offer_id: offerId, plan_id: planId, success: {response in
                       
                       print(response)
                       
                           DispatchQueue.main.async { [self] in
               
                               let alert = UIAlertController(title: "congratulations!", message: "You can Redeem the Coupon", preferredStyle: UIAlertController.Style.alert)
                               alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                  // labelarray.removeAll()
                                  self.showSpinner(onView: self.view)
                                   
                                  
//                                   let delayInSeconds = 4.0
//                                   DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
//
//                                       // here code perfomed with delay
//
//                                   }
//                                   
                                  // DispatchQueue.main.async(execute: {
                                       
                                       //Update UI
                                    //loadClaim()
//                                    self.viewDidLoad()
//                                    self.viewWillAppear(true)
                                       
                                       //Activitymain.isHidden = false
                                       //self.activeIndicator.isHidden = false
                                       
                                       let delay = 2 // seconds
                                       //self.activeIndicator.startAnimating()
                                       DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(delay)) {
                                          
                                           
                                           
                                           self.viewDidLoad()
                                           
                                           loadClaim()
                                           ClaimTableView.reloadData()
                                           
                                           self.removeSpinner()
                                           
                                           
                                           
                                         
                                       }
                                    
                                  // })

                                   
                               }))
                               
                               
                               
                               self.present(alert, animated: true)
                               
                               //self.activeIndicator.stopAnimating()
                               //self.activeIndicator.isHidden = true
                               //self.Activitymain.removeFromSuperview()
                               //self.removeSpinner()
                               
                           }
                       
                       
                      // self.removeSpinner()
                           
                       }, failiure: {message in
                           DispatchQueue.main.async {
                               
                               self.flag = 0
                           
                               print("failed to update")
                               
                               let alert = UIAlertController(title: "Oops", message: "Unable to Claim the Coupon.Try again later", preferredStyle: UIAlertController.Style.alert)
                               alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                               self.present(alert, animated: true, completion: nil)

                           }
                       })
                   
            }))
               alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))

               // show the alert
               self.present(alert, animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

       return 175
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      print("Row #: \(indexPath)")
  
        print(indexPath.row)
        
        
        if (indexPath.row == 0)
        {
        //self.performSegue(withIdentifier: "toTowing", sender: self)
        
        }
        
        if (indexPath.row == 1)
        {
        //self.performSegue(withIdentifier: "toTowing", sender: self)
        
        }
        if (indexPath.row == 2)
        {
        //self.performSegue(withIdentifier: "toTowing", sender: self)
        
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIViewController {
    class func displaySpinner(onView : UIView) -> UIView {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center

        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }

        return spinnerView
    }

    class func removeSpinner(spinner :UIView) {
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }
}

