
import UIKit
// market list details
class MarketItemDetailsTVC: UITableViewController {

    @IBOutlet weak var callAndWhatsappOuterLayer: UIView!
    @IBOutlet weak var imageOuterView: UIView!
    @IBOutlet weak var transmissionLabel: UILabel!
    @IBOutlet weak var vehicleLabel: UILabel!
    @IBOutlet weak var cylinders: UILabel!
    @IBOutlet weak var doorsLabel: UILabel!
    @IBOutlet weak var odometerLabel: UILabel!
    @IBOutlet weak var registrationExpiryLabel: UILabel!
    @IBOutlet weak var seatsLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var imageCollectionView: UICollectionView!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var networkIdLabel: UILabel!
    @IBOutlet weak var costToInsuranceLabel: UILabel!
    @IBOutlet weak var kilometerHistoryLabel: UILabel!
    @IBOutlet weak var colorLabel: UILabel!
    @IBOutlet weak var airbagsLabel: UILabel!
    @IBOutlet weak var fogLightsLabel: UILabel!
    @IBOutlet weak var fmRadioLabel: UILabel!
    @IBOutlet weak var labelbluetooth: UILabel!
    @IBOutlet weak var usbLabel: UILabel!
    @IBOutlet weak var sunroofLabel: UILabel!
    @IBOutlet weak var registeredAtLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var fuelTypeLabel: UILabel!
    @IBOutlet weak var noOfOwners: UILabel!
    @IBOutlet weak var engineLabel: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var showMoreButton: UIButton!
    var marketItem = MarketItem()
    var textFieldText = ""
    var isTextExpanded = false
    override func viewDidLoad() {
        super.viewDidLoad()
        imageCollectionView.delegate = self
        imageCollectionView.dataSource = self
        setValues()
        let label = UILabel()
        label.numberOfLines = 0
        label.text = textFieldText
    
        callAndWhatsappOuterLayer.layer.cornerRadius = 25
             callAndWhatsappOuterLayer.layer.shadowRadius = 2
             callAndWhatsappOuterLayer.layer.shadowOffset = .zero
             callAndWhatsappOuterLayer.layer.shadowOpacity = 0.5
    }
    func setValues(){
        textFieldText = marketItem.market_comments!
        commentsLabel.text = textFieldText

           if textFieldText.count > 120{
               //show read more button
               showMoreButton.isHidden = false
              
           }else{
               //hide readmore button
               showMoreButton.isHidden = true
           }
        
        transmissionLabel.text = ("Transmission: \(marketItem.md_transmission!)")
        vehicleLabel.text = ("\(marketItem.market_brand!) \(marketItem.market_model!) \(marketItem.market_variant!)")
        priceLabel.text = ("Price: $\(marketItem.market_price!)")
        networkIdLabel.text = String(marketItem.market_networkid!)
        //costToInsuranceLabel.text = marketItem.
        kilometerHistoryLabel.text = String(marketItem.market_total_km!)
        colorLabel.text = marketItem.market_colour
        yearLabel.text = marketItem.market_year
        engineLabel.text = marketItem.md_engine
        fuelTypeLabel.text = marketItem.market_fuel
        noOfOwners.text = "2"
        registeredAtLabel.text = marketItem.market_reg_place
        airbagsLabel.text = "2"
        sunroofLabel.text = "Yes"
        fogLightsLabel.text = "No"
        usbLabel.text = marketItem.md_usb_port
        labelbluetooth.text = marketItem.md_bluetooth
        fmRadioLabel.text = marketItem.md_radio
        cylinders.text = marketItem.md_cylinders
        seatsLabel.text = marketItem.md_seates
        doorsLabel.text = marketItem.md_door
        registrationExpiryLabel.text = marketItem.md_registration_expiry
    }
    @IBAction func showMorePessed(_ sender: Any) {
        
        if isTextExpanded{
            
            isTextExpanded = false
            let first150 = String(textFieldText.prefix(150))
            commentsLabel.text = first150
//            showMoreButton.titleLabel?.text = "Show More"
            showMoreButton.setTitle("Show More", for: .normal)
//             let text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s"
//            commentsLabel.text = text
//            let height = getLabelHeight(text:text , width: commentsLabel.frame.width, font: .systemFont(ofSize: 16))
//            commentsLabel.frame = CGRect(x:commentsLabel.frame.origin.x , y:commentsLabel.frame.origin.y , width: commentsLabel.frame.width, height: height)
            tableView.reloadData()
            
        }else{
            isTextExpanded = true
//            showMoreButton.titleLabel?.text = "Show Less"
            showMoreButton.setTitle("Show Less", for: .normal)
//           let height = getLabelHeight(text:textFieldText , width: commentsLabel.frame.width, font: .systemFont(ofSize: 16))
            commentsLabel.text = textFieldText

            tableView.reloadData()

            
        }
    }
    
    func getLabelHeight(text: String, width: CGFloat, font: UIFont) -> CGFloat {
         let lbl = UILabel(frame: .zero)
         lbl.frame.size.width = width
         lbl.font = font
         lbl.text = text
         lbl.sizeToFit()
        lbl.numberOfLines = 0
         return lbl.frame.size.height
     }
    
    func imageTapped(index:Int) {
        
        let url = URL(string: marketItem.market_images![index].image)
     // let imageView = sender.view as! UIImageView
      let newImageView = UIImageView()
        newImageView.kf.setImage(with: url)
      newImageView.frame = UIScreen.main.bounds
      newImageView.backgroundColor = .black
      newImageView.contentMode = .scaleAspectFit
      newImageView.isUserInteractionEnabled = true
      let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
      newImageView.addGestureRecognizer(tap)
      self.view.addSubview(newImageView)
      self.navigationController?.isNavigationBarHidden = true
      self.tabBarController?.tabBar.isHidden = true
  }

  @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
      self.navigationController?.isNavigationBarHidden = false
      self.tabBarController?.tabBar.isHidden = false
      sender.view?.removeFromSuperview()
  }
    @IBAction func callAction(_ sender: UIControl) {
        var phoneNo = marketItem.market_country_code! + marketItem.market_contact_no1!//"+"
        phoneNo = phoneNo.replacingOccurrences(of: " ", with: "")
        phoneNo = "+" + phoneNo
        let alert = UIAlertController(title: "Call", message: "Do you want to call \(phoneNo)?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            if let url = URL(string: "tel://\(phoneNo)"),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            alert.addAction(okAction)
            alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    //whatsapp
    @IBAction func whatsappAction(_ sender: UIControl) {
        var phoneNo = marketItem.market_country_code! + marketItem.market_whatsup_no!//"+"
        phoneNo = phoneNo.replacingOccurrences(of: " ", with: "")
        phoneNo = "+" + phoneNo
        let alert = UIAlertController(title: "Whatsapp", message: "Do you want to Whatsapp \(phoneNo)?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            
            let text = "Is it still available to buy?"
            if let encodedText = text.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed){
            let urlString = "https://wa.me/\(phoneNo)?text=\(encodedText)"
            
            guard let url = URL(string: urlString) else {
              return //be safe
            }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            alert.addAction(okAction)
            alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 44
        case 1:
            return 267
        case 2:
             return 167
            case 3:
                if isTextExpanded{
                    //custom height
                     return 400
                }else{
                     return 202
                }
                     
            case 4:
                     return 454
            case 5:
                    return 0
            case 6:
                               return 120
             
            
        default:
             return 0
            
        }
    }
    
    
}

//marketlist details
extension MarketItemDetailsTVC:UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        pageControl.numberOfPages = marketItem.market_images!.count
        return marketItem.market_images!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "itemCellImage", for: indexPath) as! imageCollectionCell
        let url = URL(string: marketItem.market_images![indexPath.item].image)
        cell.itemImage.kf.setImage(with: url)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.layer.frame.size
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        imageTapped(index: indexPath.item)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == imageCollectionView{
        let offSet = scrollView.contentOffset.x
        let width = scrollView.frame.width
        let horizontalCenter = width / 2
        pageControl.currentPage = Int(offSet + horizontalCenter) / Int(width)
        }
    }
    
}

