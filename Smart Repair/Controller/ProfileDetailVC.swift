//
//  ProfileDetailVC.swift
//  Smart Repair
//
//  Created by MyMac on 16/12/21.
//  Copyright © 2021 Infrap. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire
//Profile page
extension UIView {
    func roundCorners(_ corners: CACornerMask, radius: CGFloat) {
        layer.cornerRadius = radius
        layer.maskedCorners = corners
        clipsToBounds = true
    }
}
class ProfileDetailVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    var picker = UIImagePickerController()
    var profileArray = [ProfileData]()
    private var manager = CouponManager()
    var profilevehicle = [MemberVehicle]()
    var profilename = UserDefaults.standard.value(forKey: "profile_name")
    var profileimage = UserDefaults.standard.value(forKey: "profile_image")
    var profilephone = UserDefaults.standard.value(forKey: "profile_number")
    var profileid = UserDefaults.standard.value(forKey: "profile_id")
    var profilemail = UserDefaults.standard.value(forKey: "profile_email")
   // var profilevehicle = UserDefaults.standard.value(forKey: "profile_vehicle")
    
    let mem = UserDefaults.standard.value(forKey: "memberId")
    let sess = UserDefaults.standard.value(forKey: "sessionId")
    let device = UserDefaults.standard.value(forKey: "deviceId")
    @IBOutlet weak var cambtn: UIButton!
    @IBOutlet weak var HeaderView: UIView!
    @IBOutlet weak var DetailView: UIView!
    @IBOutlet weak var profileicon: UIImageView!
    @IBOutlet weak var profileTableview: UITableView!
    @IBOutlet weak var ProfileName: UILabel!
    @IBOutlet weak var vehNumberLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phLabel: UILabel!
    var textField: UITextField?
    var selectedimage : UIImage?
    let responsearray :[Datum] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.picker.allowsEditing = true
         makeImageCircle()
//        DetailView.layer.cornerRadius = 15
        DetailView.clipsToBounds = true
        DetailView.layer.masksToBounds = false
//        DetailView.layer.shadowRadius = 5
//        DetailView.layer.shadowOpacity = 0.3
//        DetailView.layer.shadowOffset = CGSize(width: 3, height: 3)
        DetailView.roundCorners([.layerMinXMinYCorner], radius: 80)
        if(profilename as? String  == nil)
        {
            ProfileName.text = ""
            
        }
        else
        {
        ProfileName.text = profilename as? String
            
            
            if profileimage is Data
                
            {
                
                print("Success")
                
                let image = UIImage(data: profileimage as! Data)
                
                profileicon.image  = image
                
                
            }
         
            else
          {
       profileicon.downloadImageFrom(link: profileimage as! String, contentMode: UIView.ContentMode.scaleAspectFill)
        print(profilevehicle)
       }
        }
            // loadValues()
        cambtn.layer.cornerRadius = 10
        
        
        // Do any additional setup after loading the view.
       // let profile_data = self.profileArray[0]
       // ProfileName.text = profile_data.memberName
        
        if let  profilephone = profilephone {
            phLabel.text = profilephone as? String
        }
        if let  profilemail = profilemail {
            emailLabel.text = profilemail as? String
        }
        
        if let  vehNo = profilevehicle[0].vehicleNumber as? String {
            vehNumberLabel.text = vehNo
        }
    }
        
    @IBAction func camerabtntapped(_ sender: Any) {
        
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
                {
                    UIAlertAction in
                    self.openCamera()
            }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
                {
                    UIAlertAction in
                    self.openGallary()
            }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
                {
                    UIAlertAction in
            }

            // Add the actions
            picker.delegate = self
            alert.addAction(cameraAction)
            alert.addAction(gallaryAction)
            alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            picker.sourceType = UIImagePickerController.SourceType.camera
            self .present(picker, animated: true, completion: nil)
        }else{
            let alert = UIAlertView()
            alert.title = "Warning"
            alert.message = "You don't have camera"
            alert.addButton(withTitle: "OK")
            alert.show()
        }
    }
    func openGallary(){
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(picker, animated: true, completion: nil)
    }
   
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

//        guard let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
//            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
//        }
//
//        profileicon.image = selectedImage
//        print(selectedImage)
//        dismiss(animated: true, completion: nil)
        
        
        
        // cropping
        var image : UIImage!

        if let img = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
                {
                    image = img
            
            profileicon.image = img
            
            selectedimage = img.resizedTo1MB()
            
            let yourDataImageJPG = selectedimage!.jpegData(compressionQuality:1)
            
            let used = UserDefaults.standard
            
            used.set(yourDataImageJPG, forKey: "profile_image")
           
            
           // selectedimage = img
            
            Uploadmultiformpartdata()

                }
        else if let img = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
                {
                    image = img
            
            profileicon.image = img
            
            selectedimage = img.resizedTo1MB()
            
            let yourDataImagePNG = selectedimage!.jpegData(compressionQuality: 1)
            
            let used = UserDefaults.standard
            
            used.set(yourDataImagePNG, forKey: "profile_image")
            
            
            
            
            Uploadmultiformpartdata()
            
            
            
                }


                picker.dismiss(animated: true,completion: nil)
        
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        
        dismiss(animated: true, completion:nil)
        print("picker cancel.")
    }
    
    func makeImageCircle(){
        //profileicon.layer.borderWidth = 0
        profileicon.layer.masksToBounds = false
        //profileicon.layer.borderColor = UIColor.black.cgColor
        profileicon.layer.cornerRadius = profileicon.frame.height/2
        profileicon.clipsToBounds = true
     }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profilevehicle.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        let profile_data = profileArray[indexPath.row]
//
//        ProfileName.text = profile_data.memberName
//
        if(indexPath.row == 0)
            
        {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath)as! Profile1TVCell
            
            cell.phoneLabel.text = profilephone as? String
            cell.emailLabel.text = profilemail as? String
            return cell
            
        }
        
        else
            
        {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell3", for: indexPath)as! Profile3TVCell
            
            cell.Registerlabel.text = profilevehicle[indexPath.row-1].vehicleNumber
            cell.BrandLabel.text = profilevehicle[indexPath.row-1].vehicleMake
            cell.ModelLabel.text = profilevehicle[indexPath.row-1].vehicleModel
            cell.ColourLabel.text = profilevehicle[indexPath.row-1].vehicleColour
            cell.PlanLabel.text = profilevehicle[indexPath.row-1].vehiclePolicy
            
            return cell
        }
        
//        if(indexPath.row == 2)
//        {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath)as! Profile2TVCell
//
//            return cell
//
//        }
        
        return UITableViewCell()
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        print("You selected cell number: \(indexPath.row)!")
        
        if(indexPath.row == 1)
        {
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "VehicleDetail") as! VehicleDetailTVC
            self.present(nextViewController, animated:true, completion:nil)
            
           
        }
    }
    
    func configurationTextField(textField: UITextField!) {
        if (textField) != nil {
            self.textField = textField!        //Save reference to the UITextField
            self.textField?.placeholder = "Some text";
        }
    }

    
    @IBAction func editnamebuttonclick(_ sender: Any) {
        
      openAlertView()
        
    }
    
    
    func openAlertView() {
        let alert = UIAlertController(title: "Edit", message: "Please Enter Profile Name", preferredStyle: UIAlertController.Style.alert)
        alert.addTextField(configurationHandler: configurationTextField)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:nil))
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler:{ (UIAlertAction) in
            print("User click Ok button")
            
            
            if(self.textField?.text=="")
                
            {
                self.alert()
                
           
            }
            
            else{
            self.ProfileName.text = self.textField?.text
            
            self.UpdateProfile(member_name: (self.textField?.text)!)
            
            let used = UserDefaults.standard
            
            used.set(self.textField?.text, forKey: "profile_name")
            
            
            }
            
        }))
        
        
      
        self.present(alert, animated: true, completion: nil)
            
            
    }
    
    
    func alert(){
        // create the alert
               let alert = UIAlertController(title: "Oops!", message: "Please Enter name to continue.", preferredStyle: UIAlertController.Style.alert)

               // add an action (button)
               alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

               // show the alert
               self.present(alert, animated: true, completion: nil)
        
    }
    
    
    func UpdateProfile(member_name:String){
        
        manager.ProfileUpdate(member_name: member_name, success: {response in
            
            print(response)
            
                DispatchQueue.main.async { [self] in
    
                    let alert = UIAlertController(title: "Updated", message: "Profile name is successfully updated", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        

                        
                    }))
                    
    
                    self.present(alert, animated: true)
                    
                }
            
                
            }, failiure: {message in
                DispatchQueue.main.async {
                    
                    //self.flag = 0
                
                    print("failed to update")
                    
                    let alert = UIAlertController(title: "Oops", message: "Unable to Update.Try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)

                }
            })
        
        
    }
    
    func Uploadmultiformpartdata(){
        
        let photoImageData = selectedimage?.pngData()
        
        var parameters: [String : Any] = [:]
        
        let urlString = "https://api.exless.com/web/index.php?r=api_3_1_/update_profile_image"
    
        self.showSpinner(onView: self.view)
    
        parameters = ["member_id": mem, "session_id" : sess ,"device_id" :device ]
        
        let headers: HTTPHeaders =
            ["Content-type": "multipart/form-data",
            "Accept": "application/json"]
        
        AF.upload(multipartFormData: { (multipartFormData) in
            
            if let data = photoImageData
            {
            multipartFormData.append(data, withName: "member_image", fileName: "swift_file1.jpeg", mimeType: "image/png")
            }
               
            
            for (key, value) in parameters {
               multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        
        }, to: CouponManager().UpdateProfileImg, //URL Here
                  method: .post,
                  headers: headers)
                  .responseJSON { (resp) in
                      //defer{SVProgressHUD.dismiss()}
                      self.removeSpinner()
                  print(resp)
                      let responsedata = resp.value
                      
                      
  //                   if let clubs = resp. {
//
//
//                              for club in clubs {
//                                  //print(club.member_image)
//                                  //print(club.location.city)
//                              }
//                          }
                      
                      //responsearray.append(contentsOf: responsedata)
                      
                     //self.responsearray.append(contentsOf: responsedata)
                      
                      if let err = resp.error{
                                  print(err)
                          print("Something went Wrong!Try again Later")
                          let alert = UIAlertController(title: "Oops!", message: "Something went Wrong!Try again Later", preferredStyle: UIAlertController.Style.alert)
                          alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                          self.present(alert, animated: true, completion: nil)
                                  //onError?(err)
                                  return
                              }
                      
                              print("Successfully uploaded")
                      let alert = UIAlertController(title: "Success", message: "Successfully uploaded", preferredStyle: UIAlertController.Style.alert)
                      alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                      self.present(alert, animated: true, completion: nil)
                      
                      //self.faButton.isEnabled = true
                      }

                  }

       
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
