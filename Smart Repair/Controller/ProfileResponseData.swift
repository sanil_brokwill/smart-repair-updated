//
//  ProfileResponseData.swift
//  Smart Repair
//
//  Created by MyMac on 03/01/22.
//  Copyright © 2022 Infrap. All rights reserved.
//

import Foundation

// MARK: - ProfileResponse2
struct ProfileResponse2: Decodable {
    let status: Int
    let meta: Meta6
    let message: String
    let data: [Datum]
    let errors: String
    let totalItems: Int

    enum CodingKeys: String, CodingKey {
        case status, meta, message, data, errors
        case totalItems = "total_items"
    }
}

// MARK: - Datum
struct Datum: Decodable{
    let memberImage: String

    enum CodingKeys: String, CodingKey {
        case memberImage = "member_image"
    }
}

// MARK: - Meta
struct Meta6: Decodable {
    let code: Int
    let message: String
}
