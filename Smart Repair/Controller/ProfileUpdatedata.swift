//
//  ProfileUpdatedata.swift
//  Smart Repair
//
//  Created by MyMac on 04/01/22.
//  Copyright © 2022 Infrap. All rights reserved.
//

import Foundation

// MARK: - Welcome
struct ProfileUpdateResponse : Decodable {
    let status: Int
    let meta: Meta7
    let message: String
   // let data: [Any?]
    let errors: String
}

// MARK: - Meta
struct Meta7: Decodable {
    let code: Int
    let message: String
}

//struct OfferResponse: Decodable {
//    var status: Int
//    var meta: Meta
//    var message: String?
//    var data: [OfferItem]
//    var errors: String
//    var total_items: Int
//
//}

