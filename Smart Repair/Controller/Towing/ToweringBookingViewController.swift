//
//  ToweringBookingViewController.swift
//  Smart Repair
//
//  Created by Admin on 26/7/2023.
//  Copyright © 2023 Infrap. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ToweringBookingViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var locationText: UITextView!
    var customAlert = CustomAlert()
    var manager = CouponManager()
    let locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpMap()
    }
    func setUpMap(){
        let locationManager = CLLocationManager()
        locationManager.requestWhenInUseAuthorization()
        mapView.showsUserLocation = true
        let location = CLLocationCoordinate2D(latitude: 37.7749, longitude: -122.4194) // Coordinates for San Francisco
        let region = MKCoordinateRegion(center: location, latitudinalMeters: 10000, longitudinalMeters: 10000)
        mapView.setRegion(region, animated: true)
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = "San Francisco"
        mapView.addAnnotation(annotation)
    }
 
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
                    let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
                    let region = MKCoordinateRegion(center: location.coordinate, span: span)
                    mapView.setRegion(region, animated: true)

                    // Add annotation
                    let annotation = MKPointAnnotation()
                    annotation.coordinate = location.coordinate
                    annotation.title = "My Location"
                    mapView.addAnnotation(annotation)
                }
                // Stop updating the location
                locationManager.stopUpdatingLocation()
           
        }
    @IBAction func bookNow(_ sender: Any){
     let latitude = String(37.7749)
         let longitude = String(-122.4194)
            if latitude == nil && longitude == nil{
                let alert = self.customAlert.showAlert(title: " Sorry!", message: "Please Enter Email")
                self.present(alert, animated: true, completion: nil)
            } else{
               
                manager.bookTowing1(latitude: latitude, longitude: longitude) { result in
                        switch result {
                        case .success(let model) :
                            DispatchQueue.main.async {
                    
                            }
                        case .failure(let error):
                            print(error.localizedDescription)
                        }
                    }
                }
//                manager.bookTowing(latitude: latitude, longitude: longitude, success: {response in
//                    print(response)
//                    DispatchQueue.main.async {
////                        if response.meta.code == 200 {
//                            let alert = UIAlertController(title: "Successful", message: response, preferredStyle:.alert)
//                            let alertBtn = UIAlertAction(title: "Ok", style: .cancel){_ in
////                                self.dismiss(animated: true)
//                            }
//                            alert.addAction(alertBtn)
//                            self.present(alert, animated: true, completion: nil)
//
////                            self.emailTextField.text = nil
////                        }
////                        else {
////                            let alert = self.customAlert.showAlert(title: "Sorry!", message: response.message)
////                            self.present(alert, animated: true, completion: nil)
////                        }
////                        self.sendButton.isEnabled = true
//                    }
//                    //self.emailTextField.text?.removeAll()
//                }, failiure: {error in
//                })
                }
        
        
//    }
    
    @IBAction func searchTapped(_ sender: Any) {
    }
    
}
