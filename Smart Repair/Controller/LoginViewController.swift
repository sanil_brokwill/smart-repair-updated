//
//  ViewController.swift
//  Coupons
//
//  Created by Infrap on 18/5/20.
//  Copyright © 2020 Infrap. All rights reserved.
//
//Login 
import UIKit
import TextFieldEffects
import NVActivityIndicatorView
import Reachability
import Toast
import Firebase
//login 
class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    var activityData = ActivityData()
    var uifunctions = UIFunctions()
    let baseColor = UIColor.init(named: "BaseColor")!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.delegate = self
        passwordTextField.delegate = self
        tryInternet()
        uiBeauty()
        
        //flag set for swiping in camera pic
        let userd = UserDefaults.standard
        userd.set("0", forKey: "swipeflag")
        userd.synchronize()
    }
    
    func tryInternet(){
        
        let reachability = try! Reachability()
        
        reachability.whenReachable = { reachability in
            
            if reachability.connection == .wifi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        }
        reachability.whenUnreachable = { _ in
            print("Not reachable")
            self.view.makeToast("Not reachable")
        }
    }
    func uiBeauty(){
        signInButton.layer.cornerRadius = signInButton.frame.height/2
        //uifunctions.beautify(button: signInButton)
//        uifunctions.beautify(textField: emailTextField)
//        uifunctions.beautify(textField: passwordTextField)
    }
//Text field is validated and call api
    @IBAction func signInAction(_ sender: UIButton) {
        let manager = CouponManager()
        
        //perform segue to home page and save user preferences
        
        if emailTextField.text == "" || passwordTextField.text == ""{
            let alert = UIAlertController(title: "Please enter credentials", message: "Please enter username and password to continue", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:"OK", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        }else{
            let email = emailTextField.text
            if (email?.contains("@"))! && (email?.contains("."))!{
                NVActivityIndicatorPresenter.sharedInstance.startAnimating(self.activityData)
                manager.login(email: emailTextField.text!, password: passwordTextField.text!, success: {message in
                    print(message)
                    
                    DispatchQueue.main.async {
                        manager.addGcmUser(success: {message in
                            print(message)
                            
                        }, failiure: {message in
                            print(message)
                            
                        })
                        UserDefaults.standard.set(self.passwordTextField.text, forKey: "password")
                        self.performSegue(withIdentifier: "loginToHome", sender: nil)

                    }
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                }, failiure: {message in
                     print(message)
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    let alert = UIAlertController(title: "Invalid Credentials", message: "Please check your username and password", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title:"OK", style: .cancel, handler: nil))
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    DispatchQueue.main.async {
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                })
                
            }else{
                let alert = UIAlertController(title: "Invalid Email", message: "please enter a valid email address", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title:"OK", style: .cancel, handler: nil))
                present(alert, animated: true, completion: nil)
            }
        }
    }
    
}

extension LoginViewController: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
