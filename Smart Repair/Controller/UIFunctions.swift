//
//  UIFunctions.swift
//  Coupons
//
//  Created by Infrap on 20/5/20.
//  Copyright © 2020 Infrap. All rights reserved.
    //

import Foundation
import TextFieldEffects
class UIFunctions{
    var color2 = UIColor(red: 55.0/255.0, green: 173.0/255.0, blue: 237.0/255.0, alpha: 1)
    var color1 = UIColor(red: 5.0/255.0, green: 60.0/255.0, blue: 167.0/255.0, alpha: 1)
      let baseColor = UIColor.init(named: "BaseColor")!
    func beautify(textField:HoshiTextField){
           textField.placeholderColor = baseColor
           textField.placeholderFontScale = 1
           textField.borderActiveColor = baseColor
           textField.borderInactiveColor = .darkGray
           textField.font = UIFont.systemFont(ofSize: 21)
          
       }
    func beautify(button:UIButton){
         button.layer.cornerRadius = 10
        button.clipsToBounds = true
        applyGradient(view: button, colours: [color1,color2], locations: nil,direction: .horizontal)
    }
    func beutify(insetView:UIView){
        insetView.layer.cornerRadius = 10
    }
    func setGradientBackground(view:UIView) {
        //89f7fe
        //66a6ff
        let colorTop =  UIColor(red: 137.0/255.0, green: 247.0/255.0, blue: 254.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 196.0/255.0, green: 233.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor

        let gradientLayer = CAGradientLayer()
        
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = view.bounds
        

        view.layer.insertSublayer(gradientLayer, at:0)
    }
    func applyGradient(view:UIView, colours: [UIColor], locations: [NSNumber]?,direction:GradientColorDirection = .vertical) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = view.bounds
        gradient.colors = colours.map { $0.cgColor }
        if case .horizontal = direction {
                   gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
                   gradient.endPoint = CGPoint(x: 1.0, y: 0.0)
               }
        gradient.locations = locations
        view.layer.insertSublayer(gradient, at: 0)
        
    }
    func applySecondaryStyle(button:UIButton){
        button.layer.cornerRadius = 10
        button.layer.borderWidth = 2
        button.layer.borderColor = color1.cgColor
        button.clipsToBounds = true
    }
    enum GradientColorDirection {
        case vertical
        case horizontal
    }
}
