//
//  RadioButtonController.swift
//  
//
//  Created by MyMac on 24/11/21.
//

import Foundation
import UIKit

class RadioButtonController: NSObject {
    var bluetoothArray: [UIButton]! {
        didSet {
            for b in bluetoothArray {
                b.setImage(UIImage(named: "circle_radio_unselected"), for: .normal)
                b.setImage(UIImage(named: "circle_radio_selected"), for: .selected)
            }
        }
    }
    
    var radioArray: [UIButton]! {
        didSet {
            for b in radioArray {
                b.setImage(UIImage(named: "circle_radio_unselected"), for: .normal)
                b.setImage(UIImage(named: "circle_radio_selected"), for: .selected)
            }
        }
    }
    var usbArray: [UIButton]! {
        didSet {
            for b in usbArray {
                b.setImage(UIImage(named: "circle_radio_unselected"), for: .normal)
                b.setImage(UIImage(named: "circle_radio_selected"), for: .selected)
            }
        }
    }
    var odoArray: [UIButton]! {
        didSet {
            for b in odoArray {
                b.setImage(UIImage(named: "circle_radio_unselected"), for: .normal)
                b.setImage(UIImage(named: "circle_radio_selected"), for: .selected)
            }
        }
    }
    var transArray: [UIButton]! {
        didSet {
            for b in transArray {
                b.setImage(UIImage(named: "circle_radio_unselected"), for: .normal)
                b.setImage(UIImage(named: "circle_radio_selected"), for: .selected)
            }
        }
    }
    var selectedButton: UIButton?
    var selectedButton1: UIButton?
    var defaultButton1: UIButton = UIButton() {
        didSet {
            buttonArrayUpdated1(buttonSelected: self.defaultButton1)
        }
    }
    
    var defaultButton2: UIButton = UIButton() {
        didSet {
            buttonArrayUpdated2(buttonSelected: self.defaultButton2)
        }
    }
    
    var defaultButton3: UIButton = UIButton() {
        didSet {
            buttonArrayUpdated3(buttonSelected: self.defaultButton3)
        }
    }
    
    var defaultButton4: UIButton = UIButton() {
        didSet {
            buttonArrayUpdated4(buttonSelected: self.defaultButton4)
        }
    }
    
    var defaultButton5: UIButton = UIButton() {
        didSet {
            buttonArrayUpdated5(buttonSelected: self.defaultButton5)
        }
    }
    
    
    func buttonArrayUpdated1(buttonSelected: UIButton) {
        for b in bluetoothArray {
            if b == buttonSelected {
                selectedButton = b
                b.isSelected = true
            } else {
                b.isSelected = false
            }
        }
    }
    
    
    func buttonArrayUpdated2(buttonSelected: UIButton) {
        for b in radioArray {
            if b == buttonSelected {
                selectedButton = b
                b.isSelected = true
            } else {
                b.isSelected = false
            }
        }
    }
    
    
    func buttonArrayUpdated3(buttonSelected: UIButton) {
        for b in usbArray {
            if b == buttonSelected {
                selectedButton = b
                b.isSelected = true
            } else {
                b.isSelected = false
            }
        }
    }
    
    
    func buttonArrayUpdated4(buttonSelected: UIButton) {
        for b in odoArray {
            if b == buttonSelected {
                selectedButton = b
                b.isSelected = true
            } else {
                b.isSelected = false
            }
        }
    }
    
    func buttonArrayUpdated5(buttonSelected: UIButton) {
        for b in transArray {
            if b == buttonSelected {
                selectedButton = b
                b.isSelected = true
            } else {
                b.isSelected = false
            }
        }
    }
    
    
    
    
}



