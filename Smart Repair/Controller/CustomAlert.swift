//
//  CustomAlert.swift
//  Coupons
//
//  Created by Infrap on 20/5/20.
//  Copyright © 2020 Infrap. All rights reserved.
//

import Foundation
import UIKit
// customalert

class CustomAlert {
    func showAlert(title:String,message:String)->UIAlertController{
        let alert = UIAlertController(title: title, message:message, preferredStyle: .alert)
                       alert.addAction(UIAlertAction(title:"OK", style: .cancel, handler: nil))
        return alert
                      
    }
    
}
