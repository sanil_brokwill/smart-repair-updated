//
//  ForgotPasswordVC.swift
//  Coupons
//
//  Created by Infrap on 3/8/20.
//  Copyright © 2020 Infrap. All rights reserved.
//

//Forgot password
import UIKit
import TextFieldEffects
class ForgotPasswordVC: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var insetView: UIView!
     var manager = CouponManager()
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    var uifunctions = UIFunctions()
    var customAlert = CustomAlert()
     var isFirstTime = true
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.delegate = self
        setUpUI()
    }
    
    // return key in keyboard
       func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
         textField.resignFirstResponder()
         return true
       }
    
    // textfield moving up
       func textFieldDidBeginEditing(_ textField: UITextField) {
               self.animateViewMoving(up: true, moveValue: 100)
       }
       func textFieldDidEndEditing(_ textField: UITextField) {
               self.animateViewMoving(up: false, moveValue: 100)
       }

       func animateViewMoving (up:Bool, moveValue :CGFloat){
           
           let movementDuration:TimeInterval = 0.3
           let movement:CGFloat = ( up ? -moveValue : moveValue)
           UIView.beginAnimations( "animateView", context: nil)
           UIView.setAnimationBeginsFromCurrentState(true)
           UIView.setAnimationDuration(movementDuration )
           self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
           UIView.commitAnimations()
       }

    
    func setUpUI(){
//        uifunctions.beautify(button: sendButton)
//        uifunctions.beautify(textField: emailTextField)
        uifunctions.beutify(insetView: insetView)
        
        
    }
    
    func textViewDidEndEditing(textView: UITextView) {
       
       
    }
    override func viewWillAppear(_ animated: Bool) {
//        uifunctions.setGradientBackground(view: self.view)
    }
//    Validate textfield and submit api
    @IBAction func sendButtonPressed(_ sender: Any) {
        
        if let email = emailTextField.text{
            if email == "" {
                sendButton.isEnabled = false
                let alert = self.customAlert.showAlert(title: " Sorry!", message: "Please Enter Email")
                self.present(alert, animated: true, completion: nil)
            } else{
                manager.forgotPassword(email: email, success: {response in
                    print(response)
                    DispatchQueue.main.async {
                        if response.meta.code == 200 {
                            let alert = UIAlertController(title: "Successful", message: response.message, preferredStyle:.alert)
                            let alertBtn = UIAlertAction(title: "Ok", style: .cancel){_ in
                                self.dismiss(animated: true)
                            }
                            alert.addAction(alertBtn)
                            self.present(alert, animated: true, completion: nil)
                            
                            self.emailTextField.text = nil
                        }else {
                            let alert = self.customAlert.showAlert(title: "Sorry!", message: response.message)
                            self.present(alert, animated: true, completion: nil)
                        }
                        self.sendButton.isEnabled = true
                    }
                    //self.emailTextField.text?.removeAll()
                }, failiure: {error in
                })
                }
        }
        
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
}
