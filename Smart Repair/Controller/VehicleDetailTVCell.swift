//
//  VehicleDetailTVCell.swift
//  Smart Repair
//
//  Created by MyMac on 16/12/21.
//  Copyright © 2021 Infrap. All rights reserved.
//

import UIKit

class VehicleDetailTVCell: UITableViewCell {

    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var planLabel: UILabel!
    @IBOutlet weak var colourLabel: UILabel!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var RegisterLabel: UILabel!
    @IBOutlet weak var BrandLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        outerView.layer.cornerRadius = 15
        outerView.clipsToBounds = true
        outerView.layer.masksToBounds = false
        outerView.layer.shadowRadius = 5
        outerView.layer.shadowOpacity = 0.3
        outerView.layer.shadowOffset = CGSize(width: 3, height: 3)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
