//
//  ChangePasswordViewController.swift
//  Coupons
//
//  Created by Infrap on 20/5/20.
//  Copyright © 2020 Infrap. All rights reserved.
//
// Change password
import UIKit
import TextFieldEffects
import Reachability

//Change password
class ChangePasswordViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var newPasswordTextField: HoshiTextField!
    @IBOutlet weak var oldPasswordTextField: UITextField!
    
    @IBOutlet weak var confirmPasswordTextField: HoshiTextField!
    
    @IBOutlet weak var submitButton: UIButton!
    var customAlert = CustomAlert()
    var uiFunctions = UIFunctions()
    let manager = CouponManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        newPasswordTextField.delegate = self
        oldPasswordTextField.delegate = self
        confirmPasswordTextField.delegate = self
        tryInternet()
       uiBeauty()
    

    }
    // textfield moving up
    func textFieldDidBeginEditing(_ textField: UITextField) {
            self.animateViewMoving(up: true, moveValue: 100)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
            self.animateViewMoving(up: false, moveValue: 100)
    }

    func animateViewMoving (up:Bool, moveValue :CGFloat){
        
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
        UIView.commitAnimations()
    }


    
    // return key in keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
      textField.resignFirstResponder()
      return true
    }
 /// Concatenate Two Strings Separated by space.
    /// - Author: John Xavier
    ///  - Note: Beutify textfield and button
    ///  - Important: make sure to do something ...
    func uiBeauty(){
        submitButton.layer.cornerRadius = submitButton.frame.height/2
    }
    // checking for the internet connection
    func tryInternet(){
        let reachability = try! Reachability()
        reachability.whenReachable = { reachability in
            if reachability.connection == .wifi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        }
        reachability.whenUnreachable = { _ in
            print("Not reachable")
        }
    }
//    Validate textfield and submit api
    @IBAction func submitButtonPressed(_ sender: Any) {
        let newPassword = newPasswordTextField.text
        let confirmPassword = confirmPasswordTextField.text
        let oldPassword = oldPasswordTextField.text
        if oldPassword == ""{
            let alert = customAlert.showAlert(title: "Password Required", message: "Old Password is required")
                           present(alert, animated: true, completion: nil)
        }else{
            if let password = UserDefaults.standard.value(forKey: "password") as? String{
                if password != oldPassword{
                    let alert = customAlert.showAlert(title: "Wrong Password", message: "Old password is wrong")
                    present(alert, animated: true, completion: nil)
                }
            }
        }
        if newPassword != "" {
            if newPassword!.count < 6{
                //print password is short
                let alert = customAlert.showAlert(title: "Password is short", message: "Use atleast 6 charecters for password")
                present(alert, animated: true, completion: nil)
            }else{
                if newPassword != confirmPassword{
                    //password doesnt match
                    let alert = customAlert.showAlert(title: "Passwords does not match", message: "New password and confirm passwords do not match")
                    present(alert, animated: true, completion: nil)

                }else{
                    //passwords match. call api
                    manager.changePassword(newPassword: newPassword!, success: {message in
                        print("password changed successfully")
                        DispatchQueue.main.async {
                            
                            UserDefaults.standard.set(newPassword, forKey: "password")
                        let alert = self.customAlert.showAlert(title: "Successful", message: "Password changed successfully")
                        self.present(alert, animated: true, completion: nil)
                        }
                    }, failiure: {message in
                        DispatchQueue.main.async {
                            
                            
                        print("failed to change password")
                        print("password changed successfully")
                                             let alert = self.customAlert.showAlert(title: "Failed!", message: "Something went wrong please try again later")
                                             self.present(alert, animated: true, completion: nil)
                        }
                    })
                }
            }
        }else{
            //enter a new password to continue
            let alert = customAlert.showAlert(title: "Field empty", message: "Please enter a password to submit")
            present(alert, animated: true, completion: nil)

        }
    }

}
