//
//  FullScreenImageVC.swift
//  Smart Repair
//
//  Created by Infrap on 6/6/21.
//  Copyright © 2021 Infrap. All rights reserved.
//

import UIKit
// 
class FullScreenImageVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
   var imageObjectArray:[ImageObject]?
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageController: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offSet = scrollView.contentOffset.x
        let width = scrollView.frame.width
        let horizontalCenter = width / 2

        pageController.currentPage = Int(offSet + horizontalCenter) / Int(width)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          if let count = imageObjectArray?.count{
              return count
          }
          return 0
         // imageArray.count
      }
      
      func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "itemCell", for: indexPath) as! imageCollectionCell
          let imageObject = imageObjectArray![indexPath.item]
          let url = URL(string: imageObject.image )
          cell.itemImage.kf.setImage(with: url)
          return cell
          
      }
      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
          let size = collectionView.layer.frame.size

          return size
      }
      func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
          //imageTapped(index: indexPath.item)
      }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
