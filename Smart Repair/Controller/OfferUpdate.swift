//
//  OfferUpdate.swift
//  Smart Repair
//
//  Created by MyMac on 26/10/21.
//  Copyright © 2021 Infrap. All rights reserved.
//

import Foundation
struct OfferUpdaateResponse:Decodable{
    var status : Int?
    var meta:Meta2?
    var message :String?
    //var data :[Any?]
    var errors: String?

}

struct Meta2 : Decodable{

    var code : Int?
    var message: String
}

//{
//    "status": 0,
//    "meta": {
//        "code": 200,
//        "message": "success"
//    },
//    "message": "success",
//    "data": [],
//    "errors": "error"
//}
//
