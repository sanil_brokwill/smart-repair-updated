//
//  TimeFormatter.swift
//  Coupons
//
//  Created by Infrap on 5/8/20.
//  Copyright © 2020 Infrap. All rights reserved.
//

import Foundation
class TimeToReadable{
    func getTimeFormatted(from timeString:String)->String{
       
        let df: DateFormatter = DateFormatter()

        df.dateFormat = "yyyy-MM-dd HH:mm:ss"

        guard let beginDate = df.date(from: timeString)
            else{
                
                print("Date format is invalid")
            return ""
                
        }
             
        let dateFormatter = DateFormatter()
        //            dateFormatter.dateStyle = .medium
        dateFormatter.dateFormat = "d MMM, yyyy"
        print(dateFormatter.string(from: beginDate))
        
        var dateString = String()
        
        dateFormatter.doesRelativeDateFormatting = true

        dateFormatter.dateStyle = .medium

        print(dateFormatter.string(from: beginDate))
        dateFormatter.locale = Locale(identifier: "en-GB")
        dateString = dateFormatter.string(from: beginDate)
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "h:mm a"

        let time = "\(dateString)"
        print(time)
       // println(time)      // prints "Today, 5:10 PM"
        let timeFormatted = String(time)
        return timeFormatted
        
    }
}
