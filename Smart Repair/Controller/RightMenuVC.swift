
import UIKit
import NVActivityIndicatorView
import Reachability
class RightMenuVC: UITableViewController {
var activityData = ActivityData()
    override func viewDidLoad() {
        super.viewDidLoad()
        tryInternet()
        // Do any additional setup after loading the view.
    }
    func tryInternet(){
        let reachability = try! Reachability()
        reachability.whenReachable = { reachability in
            if reachability.connection == .wifi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        }
        reachability.whenUnreachable = { _ in
            print("Not reachable")
        }
    }
    
    //logout pressed
    @IBAction func logOutPressed(_ sender: Any) {
        let manager = CouponManager()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(self.activityData)
        manager.logOut(success: {message in
            print(message)
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            DispatchQueue.main.async {
                
                self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)

            }
        }, failiure: {message in
             print(message)
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
        })
        
    }


}
