//
//  PrivacyPolicyVC.swift
//  Smart Repair
//
//  Created by MyMac on 18/11/21.
//  Copyright © 2021 Infrap. All rights reserved.
//

import UIKit
//privacy policy
class PrivacyPolicyVC: UIViewController {

    @IBOutlet weak var privacyTextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        privacyTextView.textAlignment = .justified
//       privacyTextView = "PRIVACY POLICY\n" +
//        "This policy outlines the way we at Exless (ABN 63 649 402 175) collect, hold, use and disclose personal information. \n" +
//        "WHAT PERSONAL INFORMATION WE COLLECT & HOW AND WHY WE COLLECT IT?\n" +
//        "What personal information do we collect?\n" +
//        "The personal information we collect is generally limited to:\n" +
//        "•\tyour name and contact details;\n" +
//        "•\tbilling information;\n" +
//        "•\tbusiness information;\n" +
//        "•\topinions; and\n" +
//        "•\tany communications we have.\n" +
//        "However, we may also collect nformation about how you use our website, via third parties.\n" +
//        "How do we collect your personal information?\n" +
//        "The main way we collect information is when you give it to us, for example, via our website sign up or other forms, via phone, email, when you submit comments or feedback or via social media.\n" +
//        "We also use cookies on our website which may identify you and track your actions and pages you visit. This helps make our website work more securely and efficiently, such as enhancing security on our contact form, and storing your information so you don’t have to put it afresh when you visit us again. \n" +
//        "At times personal information may also be gathered from third parties, such as Google Analytics or Facebook Pixel. These third parties may use cookies, web beacons and similar technology to collect or receive information about you from our website and elsewhere on the internet.\n" +
//        "Why do we collect your personal information?\n" +
//        "We need your personal information to:\n" +
//        "•\tcommunicate with you in relation to your enquiry;\n" +
//        "•\tconduct our business, and enable your use of our website, products and services; and \n" +
//        "•\tin some cases to comply with our legal obligations, such as record keeping.\n" +
//        "We also collect personal information to analyse and enhance our business operations and improve your experience with our business. This is used as statistical information to analyse traffic to our website, and to customise content and advertising we provide. \n" +
//        "You can opt out of the collection and use of this information by changing your privacy settings or opting out. To opt out you can go here: https://www.facebook.com/ads/website_custom_audiences/ or here https://tools.google.com/dlpage/gaoptout\n" +
//        "WHEN DO WE DISCLOSE PERSONAL INFORMATION & HOW YOU CAN ACCESS IT? \n" +
//        "When do we disclose your personal information?\n" +
//        "We will take reasonable precautions to protect your personal information, including against loss, unauthorised access, disclosure, misuse or modification. It is kept securely and accessible only to authorised personnel. Information is kept in accordance with our legal record keeping obligations and then destroyed appropriately. We generally will not disclose your personal information unless:\n" +
//        "•\tyou consent;\n" +
//        "•\tit is required or authorised by law; or\n" +
//        "•\tit is reasonably necessary for one of the purposes for which we collect it.\n" +
//        "However, we do disclose your personal information where it is necessary to obtain third party services, such as analytics, data storage, payment service providers or marketing and advertising services. To protect your personal information we endeavour to ensure that our third party service providers also comply with the Australian Privacy Principles, but some third parties we use may collect, hold and process personal information overseas.  You can opt out of the collection and use of this information by changing your privacy settings or opting out.\n" +
//        "How can you access or delete your information? \n" +
//        "If you want access to your information to correct or have it deleted please email us at info@exless.com. Except where we are permitted or required by law to withhold it, we will help you. If you consider that we have breached any privacy laws please also email us at info@exless.com. You can make a complaint with the Office of the Australian Information Commissioner phone on 1300 363 992, online at http://www.oaic.gov.au/privacy/making-a- privacy-complaint or post to: Office of the Australian Information Commissioner, GPO Box 5218, Sydney, NSW 2001.\n"
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
