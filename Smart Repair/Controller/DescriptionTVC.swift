//
//  DescriptionTVC.swift
//  Smart Repair
//
//  Created by MyMac on 16/11/21.
//  Copyright © 2021 Infrap. All rights reserved.
//

import UIKit
//ADD Market Description
class DescriptionTVC: UITableViewController {
    var titlearray = ["Sell Vehicle","Post Vehicle"]
    
    var descriptionarray = ["Traditionally, selling old cars is painful enough to make you stall the whole process. It takes 20-30 attempts to get the correct valuation, meet buyers face-to-face, negotiate each time, and then of course, wait several months for that RC Transfer. Do you really want to put yourself through that?","Traditionally, selling old cars is painful enough to make you stall the whole process. It takes 20-30 attempts to get the correct valuation, meet buyers face-to-face, negotiate each time, and then of course, wait several months for that RC Transfer. Do you really want to put yourself through that?"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let userde = UserDefaults.standard
        userde.set(0, forKey: "selectionflag")
        userde.synchronize()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return descriptionarray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)as! DescrptionTableViewCell
        
        cell.sellButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        cell.sellButton.titleLabel?.font = UIFont(name: "GillSans-Italic", size: 17)
        
        cell.sellLabel.text = titlearray[indexPath.row]
        cell.sellTextview.text = descriptionarray[indexPath.row]
        
        cell.sellButton.tag = indexPath.row
    
        cell.sellButton.addTarget(self, action: #selector(DescriptionTVC.btnAction(_:)), for: .touchUpInside)
        
        
        return cell
    }
    
    
    @objc func btnAction(_ sender: AnyObject) {
      performSegue(withIdentifier: "details", sender: self)
    
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
