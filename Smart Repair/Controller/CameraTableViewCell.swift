//
//  CameraTableViewCell.swift
//  Smart Repair
//
//  Created by MyMac on 13/10/21.
//  Copyright © 2021 Infrap. All rights reserved.
//

import UIKit

class CameraTableViewCell: UITableViewCell {

    
    @IBOutlet weak var ContainerView: UIView!
    @IBOutlet weak var carlabel: UILabel!
    @IBOutlet weak var carimageview: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        ContainerView.layer.cornerRadius = 15
        ContainerView.clipsToBounds = true
       ContainerView.layer.masksToBounds = false
        ContainerView.layer.shadowRadius = 5
        ContainerView.layer.shadowOpacity = 0.3
        ContainerView.layer.shadowOffset = CGSize(width: 3, height: 3)
        carlabel.textAlignment = .center
//        ContainerView.layer.shadowOffset = CGSize(width: 0, height: 3)
//        //ContainerView.layer.shadowColor = color.cgColor
//        let shadowRect: CGRect = ContainerView.bounds.insetBy(dx: 0, dy: 0.5)
//        ContainerView.layer.shadowPath = UIBezierPath(rect: shadowRect).cgPath
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}



