//
//  ProfileVC.swift
//  Smart Repair
//
//  Created by MyMac on 18/11/21.
//  Copyright © 2021 Infrap. All rights reserved.
//

import UIKit
import CoreLocation
//this profile is  now not using
class ProfileVC: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var PlaceLabel: UILabel!
    @IBOutlet weak var profilename: UILabel!
    var locationManager = CLLocationManager()
    
   // let dict = UserDefaults.object(forKey: "userDict") as? [String: String] ?? [String: String]()
    
//let dict = UserDefaults.standard.object([String: String].self, with: "userDict")
    
    let result:[String:String] = UserDefaults.standard.value(forKey: "userDict") as! [String : String]
   

    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // profilename.text = fullname as! String
        profilename.text = result["full_name"]
        
        locationManager.delegate = self
           locationManager.requestWhenInUseAuthorization()
           locationManager.desiredAccuracy = kCLLocationAccuracyBest
           locationManager.startUpdatingLocation()
           locationManager.startMonitoringSignificantLocationChanges()
           // Here you can check whether you have allowed the permission or not.
           if CLLocationManager.locationServicesEnabled()
           {
               switch(CLLocationManager.authorizationStatus())
               {
               case .authorizedAlways, .authorizedWhenInUse:
                   print("Authorize.")
                   let latitude: CLLocationDegrees = (locationManager.location?.coordinate.latitude)!
                               let longitude: CLLocationDegrees = (locationManager.location?.coordinate.longitude)!
                               let location = CLLocation(latitude: latitude, longitude: longitude) //changed!!!
                   CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
                       if error != nil {
                           return
                       }else if let city = placemarks?.first?.locality,
                           let localarea = placemarks?.first?.subLocality{
                           print(localarea)
                           print(city)
                           
                           self.PlaceLabel.text = city + "," + localarea
                           //self.cityNameStr = city
                       }
                       else {
                       }
                   })
                   break

               case .notDetermined:
                   print("Not determined.")
                   self.showAlertMessage(messageTitle: "Bolo Board", withMessage: "Location service is disabled!!")
                   break

               case .restricted:
                   print("Restricted.")
                   self.showAlertMessage(messageTitle: "Bolo Board", withMessage: "Location service is disabled!!")
                   break

               case .denied:
                   print("Denied.")
               }
           }
       }

       func showAlertMessage(messageTitle: NSString, withMessage: NSString) ->Void  {
           let alertController = UIAlertController(title: messageTitle as String, message: withMessage as String, preferredStyle: .alert)
           let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in

           }
           alertController.addAction(cancelAction)

           let OKAction = UIAlertAction(title: "Settings", style: .default) { (action:UIAlertAction!) in
               if let url = URL(string: "App-Prefs:root=Privacy&path=LOCATION/com.company.AppName") {
                   if #available(iOS 10.0, *) {
                       UIApplication.shared.open(url, options: [:], completionHandler: nil)
                   } else {
                       // Fallback on earlier versions
                   }
               }
           }
           alertController.addAction(OKAction)
           self.present(alertController, animated: true, completion:nil)
       }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
