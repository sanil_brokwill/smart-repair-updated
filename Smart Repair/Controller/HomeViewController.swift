

import UIKit
import Reachability
import FanMenu
import Macaw
import Floaty
import NVActivityIndicatorView
import SideMenu
import PopupController
import Kingfisher
import SwiftQRScanner
import SafariServices
//HOME PAGE
class HomeViewController: UIViewController {
    
    var menu : SideMenuNavigationController?
    var towing : SideMenuNavigationController?
     let userDefaults = UserDefaults.standard
    @IBOutlet weak var couponTableView: UITableView!
    @IBOutlet weak var floatingMenu: Floaty!
    let timeToReadable = TimeToReadable()
    private var couponArray = [CouponData]()
    private var startValue = 0
    private var limit  = 20
   // var selected = UserTrends()
//     var latitude = userDefaults.string(forKey: "latitude")
//    private var longitude 
//    private var ipAddress
//    private var endtime
//    private var starttime
   var flagvalue = 0
    private var manager = CouponManager()
    var userData = UserData.sharedInstance
    var activityData = ActivityData()
    var selectedCouponData : CouponData?
    private var pullControl = UIRefreshControl()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       NotificationCenter.default.addObserver(self,selector: #selector(SecondTypeNotification),
                                 name: NSNotification.Name(rawValue: "SecondTypeNotification"),
                                 object: nil)
        
         NotificationCenter.default.addObserver(self,selector:#selector(ThirdTypeNotification),
                                 name: NSNotification.Name(rawValue: "ThirdTypeNotification"),
                                object: nil)
        
        
        NotificationCenter.default.addObserver(self,selector:#selector(FourthTypeNotification),
                                name: NSNotification.Name(rawValue: "FourthTypeNotification"),
                               object: nil)
        
        
        NotificationCenter.default.addObserver(self,selector:#selector(FifthTypeNotification),
                                name: NSNotification.Name(rawValue: "FifthTypeNotification"),
                               object: nil)
        let userde = UserDefaults.standard
        userde.set(flagvalue, forKey: "selectionflag")
        userde.synchronize()
// sidemenu
        if #available(iOS 13.0, *) {
            menu = SideMenuNavigationController(rootViewController: menuListController())
            SideMenuManager.default.leftMenuNavigationController = menu
        } else {
            // Fallback on earlier versions
        }
        checkVersion()
        setUpFloatingMenu()
        tableSetting()
        tryInternet()
        getCoupons()
        getgcm()
        
       // refreshing the page
      pullControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        pullControl.addTarget(self, action: #selector(refreshListData(_:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            couponTableView.refreshControl = pullControl
        } else {
            couponTableView.addSubview(pullControl)
        }
    }
    
    @objc func refreshListData(_ sender: Any) {
        //  your code to reload tableView
        
        getCoupons()
        
        self.pullControl.endRefreshing()
    }
    
    @objc func SecondTypeNotification(notification: NSNotification){
        DispatchQueue.main.async
          {
            //Land on SecondViewController
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc: NewsandEventsTVC = storyboard.instantiateViewController(withIdentifier: "newsandevents") as! NewsandEventsTVC
            self.navigationController?.pushViewController(vc, animated: true)
          }
      }
    
    
    @objc func ThirdTypeNotification(notification: NSNotification){
        DispatchQueue.main.async
        {
//            //Land on SecondViewController
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let vc: NewsandEventsTVC = storyboard.instantiateViewController(withIdentifier: "newsandevents") as! NewsandEventsTVC
//            self.navigationController?.pushViewController(vc, animated: true)
       }
      }
    
    
    @objc func FourthTypeNotification(notification: NSNotification){
        DispatchQueue.main.async
          {
            //Land on SecondViewController
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc: MarketListVC = storyboard.instantiateViewController(withIdentifier: "MarketList") as! MarketListVC
            self.navigationController?.pushViewController(vc, animated: true)
          }
      }
    
    @objc func FifthTypeNotification(notification: NSNotification){
        DispatchQueue.main.async
          {
            //Land on SecondViewController
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc: MarketListVC = storyboard.instantiateViewController(withIdentifier: "MarketList") as! MarketListVC
            self.navigationController?.pushViewController(vc, animated: true)
          }
      }
    
    @IBAction func didTapMenu(){
        present(menu!, animated: true)
    }
    
    // setting floating menu
    func setUpFloatingMenu(){
        
        let item1 = setItem(title: "Log Out", icon: "logout")
              item1.handler = .some({ (FloatyItem) in
                               self.logOut()
                           })
        
     
        let item2 = setItem(title: "Market", icon: "ecommerce")
        item2.handler = .some({ (FloatyItem) in
            
            self.performSegue(withIdentifier: "toMarketPage", sender: self)
            
        })
        
        let item3 = setItem(title: "Change PassWord", icon: "password")
        item3.handler = .some({ (FloatyItem) in
            self.performSegue(withIdentifier: "toChangePassword", sender: self)
        })
        
        //toNewsandEvents
        
        let item4 = setItem(title: "Repair Quotes", icon: "events")
        item4.handler = .some({(FloatyItem) in
            self.performSegue(withIdentifier: "toCameraTable", sender: self)
        })
        
        
        let item5 = setItem(title: "Claim Coupon", icon: "offer_coupon")
         item5.handler = .some({(FloatyItem) in
             self.performSegue(withIdentifier: "toclaimpage", sender: self)
         })
        
        let item6 = setItem(title: "News&Events", icon: "news_events")
              item6.handler = .some({ (FloatyItem) in
                  self.performSegue(withIdentifier: "toNewsandEvents", sender: self)
//
                               
                           })
//
//        let item7 = setItem(title: "Emergency", icon: "news_events")
//              item7.handler = .some({ (FloatyItem) in
//                  self.performSegue(withIdentifier: "tosms", sender: self)
////
//
//                           })
        
        
        
              floatingMenu.addItem(item: item1)
              floatingMenu.addItem(item: item3)
              floatingMenu.addItem(item: item2)
              floatingMenu.addItem(item: item4)
              floatingMenu.addItem(item: item5)
              floatingMenu.addItem(item: item6)
              //floatingMenu.addItem(item: item7)
        
    }
    
    // qrcode scanning
//    @IBAction func leftBarButtontapped(_ sender: Any) {
//
//        let scanner = QRCodeScannerController()
//
//        //QRCode with Camera switch and Torch
//        //let scanner = QRCodeScannerController(cameraImage: UIImage(named: "camera"), cancelImage: UIImage(named: "cancel"), flashOnImage: UIImage(named: "flash-on"), flashOffImage: UIImage(named: "flash-off"))
//        scanner.delegate = self
//        self.present(scanner, animated: true, completion: nil)
//
//
//
//    }
    
    func setItem(title:String,icon:String) -> FloatyItem{
        let item = FloatyItem()
        item.buttonColor = #colorLiteral(red: 0, green: 0.5976013541, blue: 1, alpha: 1)
        let iconImageView = UIImageView(image: UIImage(named: icon))
//        iconImageView.image = iconImageView.image?.withRenderingMode(.alwaysTemplate)
        iconImageView.tintColor = .white
        item.icon = iconImageView.image
        item.title = title
        return item
    }
//logout
    func logOut(){
        
        let alert = UIAlertController(title: "Sign out?", message: "Are you sure to sign out from this account?", preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:nil))
        alert.addAction(UIAlertAction(title: "Sign out", style:.default, handler: {(_: UIAlertAction!) in
             //Sign out action
            let manager = CouponManager()
                   NVActivityIndicatorPresenter.sharedInstance.startAnimating(self.activityData)
                   manager.logOut(success: {message in
                       print(message)
                       NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                       DispatchQueue.main.async {
                           
                           self.performSegue(withIdentifier: "logoutSegue", sender: nil)

                       }
                   }, failiure: {message in
                        print(message)
                       NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                   })
             
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
//    load coupons from API
    func getCoupons(){
        
        manager.couponFetch(limit: limit, start: startValue, success: {couponResponse in
            
            let couponData = couponResponse.data
            
            self.couponArray.append(contentsOf: couponData)
            
            DispatchQueue.main.async {
                self.couponTableView.reloadData()
            }
            
        }, failiure: {message in
            
            print(message)
            
        })
    }
    
    
    func getgcm()
    {
        manager.addGcmUser(success: { (response) in
        
                    print("Success")
        
                        print(response)
            
            
                }, failiure: {
                message in
            
                        print(message)
            
                    })
}
    
    func tryInternet(){
        let reachability = try! Reachability()
        reachability.whenReachable = { reachability in
            if reachability.connection == .wifi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        }
        reachability.whenUnreachable = { _ in
            print("Not reachable")
        }
    }
    
    func checkVersion(){
        AppUpdater.shared.showUpdate(withConfirmation: true)
               
    }
//    Set up tableView
    func tableSetting(){
        couponTableView.tableFooterView = UIView()
        couponTableView.delegate = self
        couponTableView.dataSource = self
        couponTableView.register(UINib(nibName: "CouponCardCell", bundle: nil), forCellReuseIdentifier: "CouponCardCell")
         couponTableView.register(UINib(nibName: "DetailsCardCell", bundle: nil), forCellReuseIdentifier: "cell2")
    }
    
}



//MARK: - tableview delegate and data source
extension HomeViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        couponArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == couponArray.count - 1{
            startValue = startValue + limit
            getCoupons()
        }
      
        let couponData = couponArray[indexPath.row]
        print(couponData.coupon_image)
        
        if couponData.coupon_image == ""{

            let cell = couponTableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! DetailsCardCell

                cell.couponData = couponData

            return cell

        }
        else{
            
            let cell = couponTableView.dequeueReusableCell(withIdentifier: "CouponCardCell", for: indexPath) as! CouponCardCell
           // let url = couponData.coupon_image
           // cell.itemImage.kf.setImage(with: url)
            
            cell.itemImage.kf.setImage(with: URL(string: couponData.coupon_image), placeholder: nil, options: nil, progressBlock: nil, completionHandler: { result in
            switch result {
                case .success(let value):
                            print("Image: \(value.image). Got from: \(value.cacheType)")
              //  cell.itemImage.downloadImageFrom(link: value.image, contentMode: <#T##UIView.ContentMode#>)
                
                
                case .failure(let error):
                            print("Error: \(error)")
                }
            })
           // cell.itemImage.downloadImageFrom(link:couponData.coupon_image, contentMode: UIView.ContentMode.scaleAspectFill)
            //cell.itemImage.kf.cancelDownloadTask()
            
            cell.couponData = couponData            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let couponData = couponArray[indexPath.row]
        if couponData.coupon_image == ""{
            return 160
        }else{
            return 250
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedCouponData = couponArray[indexPath.row]
        
        performSegue(withIdentifier: "couponDetailSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "couponDetailSegue"{
            let destination = segue.destination as! CouponDetailVC
            destination.coupon = selectedCouponData
        }
    }
}

// Code for Folder in Homepage
@available(iOS 13.0, *)
class menuListController:UITableViewController  {
    
//    private var popupSize: CGSize
//    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
//        return popupSize
//    }
//
//
    var profileArray = [ProfileData]()
    var manager = CouponManager()
    var items = ["Profile","Privacy Policy", "Terms & Conditions","App Info"]
    var icons = [UIImage(systemName: "person.circle"),UIImage(systemName: "i.square.fill"),UIImage(systemName: "doc.text.fill"),UIImage(systemName: "i.circle.fill")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadValues()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
       
    }
    
    
    func loadValues()
    {
        
        manager.getProfiledata(success: {
            (ProfileResponse) in
            
            print("Success!!")
             
            let Responsedata = ProfileResponse.data
            self.profileArray.append(contentsOf: Responsedata)
            print(self.profileArray)
            
            let profile_data = self.profileArray[0]
            let n = profile_data.memberVehicles
            
            DispatchQueue.main.async {
                //self.profileTableview.reloadData()
               
                let used = UserDefaults.standard
                
                used.set(profile_data.memberName, forKey: "profile_name")
                //used.set(profile_data.memberImage, forKey: "profile_image")
                //used.set(profile_data.memberVehicles, forKey: "profile_vehicle")as! Array<Any>
               // used.set(profile_data.memberImage, forKey: "profile_image")
                used.set(profile_data.memberImage, forKey: "profile_image")
                used.set(profile_data.memberEmail, forKey: "profile_email")
                used.set(profile_data.memberPhone, forKey: "profile_number")
                used.set(profile_data.memberID, forKey: "profile_id")
                used.synchronize()
            }
            
            
        }, failiure: {message in
            
            print(message)
            
        })
        
        
    }
    
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for:indexPath)
        cell.textLabel?.text = items[indexPath.row]
        cell.imageView?.image = icons[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        print("You selected cell number: \(indexPath.row)!")
        
        if(indexPath.row == 0)
        {
            if(profileArray.count != 0)


            {

            let profile_data = self.profileArray[0]
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "profileDetail") as! ProfileDetailVC
            nextViewController.profilevehicle = profile_data.memberVehicles
            self.present(nextViewController, animated:true, completion:nil)
            }
            else
                
            {
                let alert = UIAlertController(title: "Failed to load!", message: "Check your Internet Connection", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                
            }
        }
        
        if(indexPath.row == 1)
        {
           
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "Privacy") as! PrivacyPolicyVC
            self.present(nextViewController, animated:true, completion:nil)

        }
        
        if(indexPath.row == 2)
        {
            //let vc = self.storyboard?.instantiateViewController(withIdentifier: "Privacy") as? PrivacyPolicyVC
            
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "termsandconditons") as! Terms_ConditionsVC
            self.present(nextViewController, animated:true, completion:nil)

        }
        
        
        if(indexPath.row == 3)
        {
            //let vc = self.storyboard?.instantiateViewController(withIdentifier: "Privacy") as? PrivacyPolicyVC
            
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "appinformation") as! AppinfoVC
            self.present(nextViewController, animated:true, completion:nil)

        }
        
    }
    
}
// Code for QRCode
//extension HomeViewController:QRScannerCodeDelegate {
//    func qrScanner(_ controller: UIViewController, scanDidComplete result: String) {
//        print("result:\(result)")
//
//        loadSafari(url: result)
//
//
//    }
//
//    func qrScannerDidFail(_ controller: UIViewController, error: String) {
//        print("error:\(error)")
//    }
//
//    func qrScannerDidCancel(_ controller: UIViewController) {
//        print("SwiftQRScanner did cancel")
//    }
//
//
//    func loadSafari(url : String){
//
//        if let url = URL(string: url){ //check whether the string is URL
//            if #available(iOS 10.0, *) {
//                UIApplication.shared.open(url, options: [:], completionHandler: nil)
//            } else {
//                UIApplication.shared.openURL(url)
//            }
//        }else {
//
//
//            //do more if scanned text is not url string
//        }
//
////          guard let url = URL(string: url) else { return }
////          let safariController = SFSafariViewController(url: url)
////          present(safariController, animated: true, completion: nil)
//       }
//
//}



