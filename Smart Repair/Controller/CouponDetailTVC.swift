// Coupon Details
import UIKit
class CouponDetailVC: UIViewController {
    
    @IBOutlet weak var couponImage: UIImageView!
    @IBOutlet weak var offerAmountLbl: UILabel!
//    @IBOutlet weak var offerProviderLbl: UILabel!
//    @IBOutlet weak var placeLbl: UILabel!
//    @IBOutlet weak var couponCodeLbl: UILabel!
    @IBOutlet weak var expiryDateLbl: UILabel!
//    @IBOutlet weak var descreptionTxtView: UITextView!
    @IBOutlet weak var startingDateLabel: UILabel!
    
//    @IBOutlet weak var imageBottomConstraint: NSLayoutConstraint!
//    @IBOutlet weak var cardViewBottomConstraint: NSLayoutConstraint!
//    @IBOutlet weak var cardViewTopDistance: NSLayoutConstraint!
    @IBOutlet weak var CardView: UIView!
    var coupon: CouponData?
//    show coupon details in a card
    override func viewDidLoad() {
        super.viewDidLoad()
        couponImage.layer.cornerRadius = 10
        CardView.layer.cornerRadius = 20
       
        
        couponImage.layer.shadowPath = UIBezierPath(rect: CardView.bounds).cgPath
        CardView.layer.shadowRadius = 5
        CardView.layer.shadowOffset = .zero
        CardView.layer.shadowOpacity = 1
        couponImage.layer.shadowOpacity = 1
        couponImage.layer.shadowRadius = 1
        if let couponData1 = coupon{
        //                offerAmountLbl.text = "dsfsd"
                    offerAmountLbl.attributedText = coupon?.coupon_discount.htmlToAttributedString
                    offerAmountLbl.font = .systemFont(ofSize: 40)
            offerAmountLbl.textAlignment = .center
                    print(couponData1)
                    let timeInstance = TimeToReadable()
                    let date = timeInstance.getTimeFormatted(from: coupon!.coupon_expires_on)
                    expiryDateLbl.text = "\(date)"
                    }
        
        loadCouponDetail()
    }
    func loadCouponDetail(){
        
        if  coupon?.coupon_image == ""{
//            cardViewTopDistance.constant = 50
//            cardViewBottomConstraint.constant += 150
            couponImage.isHidden = true
        }else{
            //imageBottomConstraint.constant -= 150
//            cardViewTopDistance.constant = 10
//
//            cardViewBottomConstraint.constant += 100
            couponImage.downloadImageFrom(link: coupon!.coupon_image, contentMode: .scaleAspectFill)
           
            
        }
//        let offerAmount = String(coupon!.coupon_discount)
//        offerAmountLbl.text = "\(offerAmount) OFF"
//        offerProviderLbl.text = coupon?.coupon_item
//        placeLbl.text = coupon?.coupon_place
//        couponCodeLbl.text = "Coupon Code:\n \(coupon!.coupon_serial)"
        
        
        let timeInstance = TimeToReadable()
            let date = timeInstance.getTimeFormatted(from: coupon!.coupon_expires_on)
        let startDate = timeInstance.getTimeFormatted(from: coupon!.coupon_added_on)
        expiryDateLbl.text = "\(date)"
        startingDateLabel.text = "\(startDate)"
        //descreptionTxtView.text = coupon?.coupon_description
    }
    
    
    @IBAction func closePressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
