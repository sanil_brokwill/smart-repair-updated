

import UIKit
// market list

class MarketListVC: UIViewController {
    
    lazy var faButton: UIButton = {
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .blue
       // button.addTarget(self, action: #selector(fabTapped(_:)), for: .touchUpInside)
        return button
    }()

    let image = UIImage(named: "plus")

    @IBOutlet weak var marketListTableView: UITableView!
    @IBOutlet weak var productCollectionView: UICollectionView!
    private var pullControl = UIRefreshControl()
    var itemArray = [MarketItem]()
    let manager = CouponManager()
    var selectedItem = MarketItem()
    private var startValue = 0
    private var limit  = 20
    override func viewDidLoad() {
        super.viewDidLoad()
        marketListTableView.delegate = self
        marketListTableView.dataSource = self
        marketListTableView.tableFooterView = UIView()
        self.addBackButton()
        loaditems()
        
        pullControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
                pullControl.addTarget(self, action: #selector(refreshListData(_:)), for: .valueChanged)
                if #available(iOS 10.0, *) {
                    marketListTableView.refreshControl = pullControl
                } else {
                    marketListTableView.addSubview(pullControl)
                }
        
    }
    
    @objc private func refreshListData(_ sender: Any) {
        
            loaditems()
            self.pullControl.endRefreshing() // You can stop after API Call
            // Call API
        }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        if let view = UIApplication.shared.keyWindow {
//            view.addSubview(faButton)
           // setupButton()     // this button is to enter market detail.now it is hidden.On this button click,go to detail page
       // }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       // if let view = UIApplication.shared.keyWindow, faButton.isDescendant(of: view) {
            //faButton.removeFromSuperview()
       // }
    }
    
    func setupButton() {
        NSLayoutConstraint.activate([
            faButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -26),
            faButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -26),
            //faButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: -36),
            
            
            faButton.heightAnchor.constraint(equalToConstant: 60),
            faButton.widthAnchor.constraint(equalToConstant: 60)
            ])
        
        
        //faButton.setTitle("Upload", for: UIControl.State.normal)
       // faButton.setImage(image, for: UIControl.State.normal)
        faButton.setBackgroundImage(image, for: UIControl.State.normal)
        faButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        faButton.layer.cornerRadius = 30
        faButton.layer.masksToBounds = true
        //faButton.layer.borderColor = UIColor.lightGray.cgColor
        //faButton.layer.borderWidth = 4
        faButton.backgroundColor = #colorLiteral(red: 0, green: 0.5976013541, blue: 1, alpha: 1)
    }
//    @objc func fabTapped(_ button: UIButton) {
//        print("button tapped")
//
//        let userde = UserDefaults.standard
//        userde.set(1, forKey: "selectionflag")
//        userde.synchronize()
//
//        performSegue(withIdentifier: "descriptionsegue", sender: self)
//    }
    
    
    
    func loaditems(){
        manager.getMarketList(limit: limit, start: startValue, success: { (response) in
            print("Success")
            self.itemArray = response
            
            
            DispatchQueue.main.async {
                
                self.marketListTableView.reloadData()
            }
            
        }) { (error) in
            print("Error")
        }
    }
    @objc func showDetail(sender:UIButton){
        //flag = 0
        selectedItem = itemArray[sender.tag]
        performSegue(withIdentifier: "listToDetail", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var flag = UserDefaults.standard.value(forKey: "selectionflag")
        if(flag as! Int == 0){
        let des = segue.destination as! MarketItemDetailsTVC
        des.marketItem = selectedItem
        }
    }
}

extension MarketListVC : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == itemArray.count - 1{
            startValue = startValue + limit
            loaditems()
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "marketItemCell", for: indexPath) as! MarketItemCell
        
        cell.item = itemArray[indexPath.row]
        cell.ownerPage = self
       // cell.viewDetailsButton.tag = indexPath.row
       // cell.viewDetailsButton.addTarget(self, action: #selector(showDetail), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //flag = 0
        selectedItem = itemArray[indexPath.row]
               performSegue(withIdentifier: "listToDetail", sender: self)
    }
    
}
