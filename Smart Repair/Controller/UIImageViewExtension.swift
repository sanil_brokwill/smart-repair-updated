//
//  UIImageViewExtension.swift
//  Coupons
//
//  Created by Infrap on 3/6/20.
//  Copyright © 2020 Infrap. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    func downloadImageFrom(link:String, contentMode: UIView.ContentMode) {
        if let url = NSURL(string: link) as URL? {
        URLSession.shared.dataTask( with: url, completionHandler: {
            (data, response, error) -> Void in
            DispatchQueue.main.async() {
                self.contentMode =  contentMode
                if let data = data { self.image = UIImage(data: data) }
            }
        }).resume()
        }
    }
    
    func downloadUserImageFrom(link:String, contentMode: UIView.ContentMode) {
        if link.contains(".jpg") || link.contains(".png"){
        if let url = NSURL(string: link) as URL? {
            URLSession.shared.dataTask( with: url, completionHandler: {
                (data, response, error) -> Void in
                DispatchQueue.main.async() {
                    self.contentMode =  contentMode
                    if let data = data { self.image = UIImage(data: data) }
                    else {
                        //load default image
                        self.image = UIImage(named: "usericonDark")
                    }
                }
            }).resume()
        } else {
            //load default image
            self.image = UIImage(named: "usericonDark")
        }
        } else {
            //load default image
            self.image = UIImage(named: "usericonDark")
        }
    }
    
    func downloadUserImageAndSaveLocally(link:String,userId:String, contentMode: UIView.ContentMode){
        self.image = UIImage(named: "usericonDark")
        let key = "userImage"+userId
        let userdefaults = UserDefaults.standard
        //if image is available local show that

        if let profilePicData = userdefaults.data(forKey: key){
            if let profilePic = UIImage(data: profilePicData){
                 self.image = profilePic
            }
           
        }
        //check online for new image in other thread
        if link.contains(".jpg") || link.contains(".png"){
            if let url = NSURL(string: link) as URL? {
                URLSession.shared.dataTask( with: url, completionHandler: {
                    (data, response, error) -> Void in
                    DispatchQueue.main.async() {
                        self.contentMode =  contentMode
                        if let data = data {
                            let imageDownloadedData = data
                             self.image = UIImage(data: data)
                            userdefaults.set(imageDownloadedData, forKey: key)
                        }
                    }
                }).resume()
            }
        }
        
        
    }
   
}
