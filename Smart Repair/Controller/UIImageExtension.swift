//
//  UIImageExtension.swift
//  Coupons
//
//  Created by Infrap on 3/6/20.
//  Copyright © 2020 Infrap. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
    func base64String() -> String {
        let imageData = self.pngData()
        let imageString = (imageData?.base64EncodedString(options: .lineLength64Characters))!
        let tempImageString = imageString.replacingOccurrences(of: "+", with: "%2B")
        return tempImageString.replacingOccurrences(of: "\r\n", with: "")
//        return (imageData?.base64EncodedString(options: .lineLength64Characters))!
    }
    
    func imageData() -> Data {
        return self.pngData()!
    }
 
}
