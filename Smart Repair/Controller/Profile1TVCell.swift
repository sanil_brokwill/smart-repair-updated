//
//  Profile1TVCell.swift
//  Smart Repair
//
//  Created by MyMac on 16/12/21.
//  Copyright © 2021 Infrap. All rights reserved.
//

import UIKit

class Profile1TVCell: UITableViewCell {

    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var OuterView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        OuterView.layer.cornerRadius = 15
        OuterView.clipsToBounds = true
        OuterView.layer.masksToBounds = false
        OuterView.layer.shadowRadius = 5
        OuterView.layer.shadowOpacity = 0.3
        OuterView.layer.shadowOffset = CGSize(width: 3, height: 3)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
