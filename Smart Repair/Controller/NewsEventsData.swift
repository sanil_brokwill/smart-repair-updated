//
//  File.swift
//  Smart Repair
//
//  Created by MyMac on 02/12/21.
//  Copyright © 2021 Infrap. All rights reserved.
//


// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

// MARK: - Welcome
struct NewsEventsResponse: Decodable{
    let status: Int
    let meta: Meta3
    let message: String
    let data: [NewsEventsData]
    let errors: String
    let totalItems: Int

    enum CodingKeys: String, CodingKey {
        case status, meta, message, data, errors
        case totalItems = "total_items"
    }
}

// MARK: - Datum
struct NewsEventsData: Decodable {
    let newsID: Int
    let newsTitle, newsDescription: String
    let newsType : Int
    let addedOn, addedBy : String
    let statusID: Int
    let newsLink: String

    enum CodingKeys: String, CodingKey {
        case newsID = "news_id"
        case newsTitle = "news_title"
        case newsDescription = "news_description"
        case newsType = "news_type"
        case addedBy = "added_by"
        case addedOn = "added_on"
        case statusID = "status_id"
        case newsLink = "news_link"
        
        
    }
}

// MARK: - Meta
struct Meta3: Decodable {
    let code: Int
    let message: String
}

//{
//    "status": 0,
//    "meta": {
//        "code": 200,
//        "message": "success"
//    },
//    "message": "success",
//    "data": [
//        {
//            "news_id": 5,
//            "news_title": "",
//            "news_description": "",
//            "news_type": 1,
//            "added_by": "Luby Lukose",
//            "added_on": "2021-12-09 09:38:38",
//            "status_id": 1,
//            "news_link": "http://apiexless.rosterwand.com/backend/web/upload/news_events/news_events-1639042718.jpeg"
//        },
//        {
//            "news_id": 6,
//            "news_title": "Notification Test",
//            "news_description": "<p>es simplemente el texto de relleno de las<b> imprentas y</b> archivos de <i>texto</i>.<i> <font color=\"#ce0000\">Lorem Ipsum ha sido</font></i> el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de</p>",
//            "news_type": 1,
//            "added_by": "Luby Lukose",
//            "added_on": "2021-12-10 10:10:32",
//            "status_id": 1,
//            "news_link": "http://apiexless.rosterwand.com/backend/web/upload/news_events/news_events-1639131032.jpg"
//        },
//        {
//            "news_id": 7,
//            "news_title": "Notification Test",
//            "news_description": "<p>es simplemente el texto de relleno de las<b> imprentas y</b> archivos de <i>texto</i>.<i> <font color=\"#ce0000\">Lorem Ipsum ha sido</font></i> el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de</p>",
//            "news_type": 1,
//            "added_by": "Luby Lukose",
//            "added_on": "2021-12-10 10:11:13",
//            "status_id": 1,
//            "news_link": "http://apiexless.rosterwand.com/backend/web/upload/news_events/news_events-1639131073.jpg"
//        },
//        {
//            "news_id": 8,
//            "news_title": "Notification Test",
//            "news_description": "<p>es simplemente el texto de relleno de las<b> imprentas y</b> archivos de <i>texto</i>.<i> <font color=\"#ce0000\">Lorem Ipsum ha sido</font></i> el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de</p>",
//            "news_type": 1,
//            "added_by": "Luby Lukose",
//            "added_on": "2021-12-10 10:11:48",
//            "status_id": 1,
//            "news_link": "http://apiexless.rosterwand.com/backend/web/upload/news_events/news_events-1639131108.jpg"
//        },
//        {
//            "news_id": 10,
//            "news_title": "",
//            "news_description": "",
//            "news_type": 1,
//            "added_by": "Luby Lukose",
//            "added_on": "2021-12-10 10:28:50",
//            "status_id": 1,
//            "news_link": "http://apiexless.rosterwand.com/backend/web/upload/news_events/news_events-1639132130.jpg"
//        }
//    ],
//    "errors": "error",
//    "total_items": 5
//}
