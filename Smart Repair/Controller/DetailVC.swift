//
//  DetailVC.swift
//  Smart Repair
//
//  Created by MyMac on 16/11/21.
//  Copyright © 2021 Infrap. All rights reserved.
//

import UIKit
import iOSDropDown

//Add market first page

class DetailVC: UIViewController,UITextFieldDelegate {
    
  
    //@IBOutlet weak var bluebutton1: PVRadioButton!
    
    //@IBOutlet weak var bluetooth2: PVRadioButton!
    
    @IBOutlet weak var bluebtn2: UIButton!
    @IBOutlet weak var bluebtn1: UIButton!
    
    @IBOutlet weak var radio1: UIButton!
    
    @IBOutlet weak var radio2: UIButton!
    
    @IBOutlet weak var portbtn1: UIButton!
    
    @IBOutlet weak var portbtn2: UIButton!
    
    @IBOutlet weak var Odobtn1: UIButton!
    
    @IBOutlet weak var odobtn2: UIButton!
    
    @IBOutlet weak var Transbtn1: UIButton!
    
    @IBOutlet weak var transbtn2: UIButton!
    
    @IBOutlet weak var BrandropDown: DropDown!
    
    @IBOutlet weak var scrollview: UIScrollView!
    
    
    @IBOutlet weak var RegisterExpiry: UITextField!
    
    @IBOutlet weak var VehName: UITextField!
    
    @IBOutlet weak var Fuel: UITextField!
    
    @IBOutlet weak var Kilometers: UITextField!{
        didSet {
            Kilometers?.addDoneCancelToolbar(onDone: (target: self, action: #selector(doneButtonTappedForMyKilometerTextField)))
        }
    }

    
    @IBOutlet weak var Colour: UITextField!
    
    @IBOutlet weak var Engine: UITextField!
    
    @IBOutlet weak var Cylinder: UITextField!
    
   
    @IBOutlet weak var Seat: UITextField!{
        didSet {
            Seat?.addDoneCancelToolbar(onDone: (target: self, action: #selector(doneButtonTappedForMyseatTextField)))
        }
    }
    
    
    @IBOutlet weak var Door: UITextField!{
        didSet {
            Door?.addDoneCancelToolbar(onDone: (target: self, action: #selector(doneButtonTappedForMyDoorTextField)))
        }
    }
    
    @IBOutlet weak var Varient: UITextField!
    @IBOutlet weak var datepicker: UIDatePicker!
    @IBOutlet weak var ManufactureYear: UITextField!
    
    @IBOutlet weak var NextButton: UIButton!
    let radioController: RadioButtonController = RadioButtonController()
   
    let datePicker = UIDatePicker()
    var selectedDate = ""
    var bluetoothvalue = ""
    var radiovalue = ""
    var usbvalue = ""
    var odovalue = ""
    var transvalue = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        VehName.delegate = self
        Fuel.delegate = self
        Engine.delegate = self
        Kilometers.delegate = self
        Colour.delegate = self
        Cylinder.delegate = self
        //SetPrice.delegate = self
        Seat.delegate = self
        Door.delegate = self
        Varient.delegate = self
        
        
        // enable multiple selection for water, beer and wine buttons.
       
        radioController.bluetoothArray = [bluebtn1,bluebtn2]
                //radioController.defaultButton1 = bluebtn1
        
        radioController.radioArray = [radio1,radio2]
                //radioController.defaultButton2 = radio1
        
        radioController.usbArray = [portbtn1,portbtn2]
                //radioController.defaultButton3 = portbtn1
        
        radioController.odoArray = [Odobtn1,odobtn2]
                //radioController.defaultButton4 = Odobtn1
        
        
        radioController.transArray = [Transbtn1,transbtn2]
                //radioController.defaultButton5 = Transbtn1
        
        let contentWidth = scrollview.bounds.width
               let contentHeight = scrollview.bounds.height * 3
        scrollview.contentSize = CGSize(width: contentWidth, height: contentHeight)
        BrandropDown.optionArray = ["Maruti Suzki",
                                    "Hundai",
                                    "Tata",
                                    "Mahindra",
                                    "Honda",
                                    "Kia",
                                    "Toyota"]
        BrandropDown.didSelect{(selectedText , index ,id) in
        self.BrandropDown.text = selectedText
        }
        BrandropDown.arrowSize = 10
        
        NextButton.layer.cornerRadius = 15
        showDatePicker()
    }
 
        // NOTE:- here you can recognize with tag weather it is `Male` or `Female`
    
     @objc func doneButtonTappedForMyKilometerTextField() {
        print("Done");
        Kilometers.resignFirstResponder()
    }
    @objc func doneButtonTappedForMyseatTextField() {
       print("Done");
       Seat.resignFirstResponder()
   }
    @objc func doneButtonTappedForMyDoorTextField() {
       print("Done");
       Door.resignFirstResponder()
   }
       
    
    @IBAction func Bluebtn1Action(_ sender: UIButton) {
           radioController.buttonArrayUpdated1(buttonSelected: sender)
        
        bluetoothvalue = "Yes"
       }

       @IBAction func  Bluebtn2Action(_ sender: UIButton) {
           radioController.buttonArrayUpdated1(buttonSelected: sender)
           
           bluetoothvalue = "No"
       }

       @IBAction func RadiobtnAction1(_ sender: UIButton) {
           radioController.buttonArrayUpdated2(buttonSelected: sender)
           
           radiovalue = "Yes"
       }
    
    
    @IBAction func  RadiobtnAction2(_ sender: UIButton) {
        radioController.buttonArrayUpdated2(buttonSelected: sender)
        
        radiovalue = "No"
    }
    
    @IBAction func usbbtnAction1(_ sender: UIButton) {
        radioController.buttonArrayUpdated3(buttonSelected: sender)
        
        usbvalue = "Yes"
    }
 
 
 @IBAction func  usbbtnAction2(_ sender: UIButton) {
     radioController.buttonArrayUpdated3(buttonSelected: sender)
     
     usbvalue = "No"
 }
    
    @IBAction func OdobtnAction1(_ sender: UIButton) {
        radioController.buttonArrayUpdated4(buttonSelected: sender)
        
        odovalue = "Yes"
    }
 
 
 @IBAction func  OdobtnAction2(_ sender: UIButton) {
     radioController.buttonArrayUpdated4(buttonSelected: sender)
     odovalue = "No"
 }
    
    
    @IBAction func TransbtnAction1(_ sender: UIButton) {
        radioController.buttonArrayUpdated5(buttonSelected: sender)
        
        transvalue = "Manuel"
    }
 
 
 @IBAction func  TransbtnAction2(_ sender: UIButton) {
     radioController.buttonArrayUpdated5(buttonSelected: sender)
     
     transvalue = "Automatic"
 }

   
    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        //datePicker.maximumDate = Date()
        //ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 0/255, green: 153/255, blue: 51/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(doneButtonTapped(sender:)))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([ cancelButton,spaceButton , doneButton], animated: false)
        
        ManufactureYear.inputAccessoryView = toolBar
        ManufactureYear.inputView = datePicker
        RegisterExpiry.inputAccessoryView = toolBar
        RegisterExpiry.inputView = datePicker
        
    }
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
            if textField == ManufactureYear {
                datePicker.datePickerMode = .date
            }
            if textField == RegisterExpiry{
                datePicker.datePickerMode = .date
            }
        
        //bluetooth.showList()
            
        }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    @objc func doneButtonTapped(sender:UIBarButtonItem) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        if ManufactureYear.isFirstResponder {
            formatter.dateStyle = .medium
            formatter.timeStyle = .none
            ManufactureYear.text = formatter.string(from: datePicker.date)
        }
        if RegisterExpiry.isFirstResponder {
            formatter.dateStyle = .medium
            formatter.timeStyle = .none
            RegisterExpiry.text = formatter.string(from: datePicker.date)
        }
       
        self.view.endEditing(true)
    }
    
    @IBAction func Nextbtntapped(_ sender: Any) {
        
        
        
        
        
        
//        if(BrandropDown.text == "")
//        {
//            let alert = UIAlertController(title: "Missing Field!", message: "Please Select Brand Name", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title:"OK", style: .cancel, handler: nil))
//            present(alert, animated: true, completion: nil)
//        }
//
//
//        if(VehName.text == "")
//        {
//            let alert = UIAlertController(title: "Vehicle name is Mandatory!", message: "Please complete the required field", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title:"OK", style: .cancel, handler: nil))
//            present(alert, animated: true, completion: nil)
//        }
//
//        else if(Varient.text == "")
//        {
//            let alert = UIAlertController(title: "Varient is Mandatory!", message: "Please complete the required field", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//            present(alert, animated: true, completion:  nil)
//        }
//
//        else if(Kilometers.text == "")
//        {
//            let alert = UIAlertController(title: "Kilometer is Mandatory!!", message: "Please complete the required field", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//            present(alert, animated: true, completion: nil)
//        }
//        else if(ManufactureYear.text == "")
//        {
//            let alert = UIAlertController(title: "MAnufacture Year is Mandatory!", message: "Please complete the required field", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//            present(alert, animated: true,completion: nil)
//        }
//        else if(RegisterExpiry.text == "")
//        {
//            let alert = UIAlertController(title: "Register Expiry is Mandatory!", message: "Please complete the required field", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//            present(alert, animated: true, completion: nil)
//
//        }
//
//        else if(Fuel.text == "")
//        {
//            let alert = UIAlertController(title: "Fuel is Mandatory!", message: "Please complete the required field", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//            present(alert, animated: true, completion: nil)
//        }
//
//        else if(Colour.text == "")
//        {
//            let alert = UIAlertController(title: "Colour is Mandatory!", message: "Please complete the required fielde", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//            present(alert, animated: true, completion: nil)
//        }
//
//        else if(Engine.text == "")
//        {
//            let alert = UIAlertController(title: "Engine is Mandatory!!", message: "Please complete the required field", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//            present(alert,animated: true, completion: nil)
//        }
//        else if(Cylinder.text == "")
//        {
//            let alert = UIAlertController(title: "Number of Cylinder is Mandatory! ", message: "Please complete the required field", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//            present(alert, animated: true, completion: nil)
//        }
//
//        else if(Seat.text == "")
//        {
//            let alert = UIAlertController(title: "Number of seats is Mandatory!", message: "Please complete the required field", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//            present(alert, animated: true, completion: nil)
//        }
//        else if (Door.text == "")
//        {
//            let alert = UIAlertController(title: "Number of doors is Mandatory!", message: "Please complete the required field", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//            present(alert, animated: true, completion: nil)
//
//        }
//
//       else if(bluebtn1.isSelected == false && bluebtn2.isSelected == false)
//            {
//            let alert = UIAlertController(title: "Bluetooth is Mandatory!", message: "Please complete the required field", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//            present(alert, animated: true, completion: nil)
//            }
//
//        else if(radio1.isSelected == false && radio2.isSelected == false)
//             {
//             let alert = UIAlertController(title: "Radio is Mandatory!", message: "Please complete the required field", preferredStyle: .alert)
//             alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//             present(alert, animated: true, completion: nil)
//             }
//
//        else if(portbtn1.isSelected == false && portbtn2.isSelected == false)
//             {
//             let alert = UIAlertController(title: " USB port is Mandatory!", message: "Please complete the required field", preferredStyle: .alert)
//             alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//             present(alert, animated: true, completion: nil)
//             }
//
//        else if(Odobtn1.isSelected == false && odobtn2.isSelected == false)
//             {
//             let alert = UIAlertController(title: " odometer is Mandatory!", message: "Please complete the required field", preferredStyle: .alert)
//             alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//             present(alert, animated: true, completion: nil)
//             }
//        else if(Transbtn1.isSelected == false && transbtn2.isSelected == false)
//             {
//             let alert = UIAlertController(title: "Transmission is Mandatory!", message: "Please complete the required field", preferredStyle: .alert)
//             alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//             present(alert, animated: true, completion: nil)
//             }
//
//        else
//        {
        
        let userde = UserDefaults.standard
        userde.set(VehName.text, forKey: "VehicleName")
        userde.set(BrandropDown.text, forKey: "BrandName")
        userde.set(Varient.text, forKey: "Varient")
        userde.set(Kilometers.text, forKey: "Kilometers")
        userde.set(ManufactureYear.text, forKey: "ManufactureYear")
        userde.set(RegisterExpiry.text, forKey: "RegisterExpiry")
        userde.set(Fuel.text, forKey: "fuel")
        userde.set(Colour.text, forKey:"colour")
        userde.set(Engine.text, forKey: "Engine")
        userde.set(Cylinder.text, forKey: "Cylinder")
        userde.set(Seat.text, forKey: "Seat")
        userde.set(Door.text, forKey: "Door")
        userde.set(bluetoothvalue, forKey: "Bluetooth")
        userde.set(radiovalue, forKey: "Radio")
        userde.set(usbvalue, forKey: "Usbport")
        userde.set(odovalue, forKey: "Odometer")
        userde.set(transvalue, forKey: "Transmission")
        
        performSegue(withIdentifier: "secondPage", sender: self)
            
        //}
    }
    
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIButton {
    //MARK:- Animate check mark
    func checkboxAnimation(closure: @escaping () -> Void){
        guard let image = self.imageView else {return}
        self.adjustsImageWhenHighlighted = false
        self.isHighlighted = false
        
        UIView.animate(withDuration: 0.1, delay: 0.1, options: .curveLinear, animations: {
            image.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            
        }) { (success) in
            UIView.animate(withDuration: 0.1, delay: 0, options: .curveLinear, animations: {
                self.isSelected = !self.isSelected
                //to-do
                closure()
                image.transform = .identity
            }, completion: nil)
        }
        
    }
}


extension UITextField {
    func addDoneCancelToolbar(onDone: (target: Any, action: Selector)? = nil, onCancel: (target: Any, action: Selector)? = nil) {
        let onCancel = onCancel ?? (target: self, action: #selector(cancelButtonTapped))
        let onDone = onDone ?? (target: self, action: #selector(doneButtonTapped))

        let toolbar: UIToolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.items = [
            UIBarButtonItem(title: "Cancel", style: .plain, target: onCancel.target, action: onCancel.action),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: .done, target: onDone.target, action: onDone.action)
        ]
        toolbar.sizeToFit()

        self.inputAccessoryView = toolbar
    }
    
    
    @objc func doneButtonTapped() { self.resignFirstResponder() }
    @objc func cancelButtonTapped() { self.resignFirstResponder() }
    
}
