

//
//  CameraTableViewC.swift
//  Smart Repair
//
//  Created by MyMac on 13/10/21.
//  Copyright © 2021 Infrap. All rights reserved.
//

import UIKit
import Alamofire
//Repair Quote
var vSpinner : UIView?
class CameraTableViewC: UITableViewController {
    
    
    lazy var faButton: UIButton = {
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .blue
        button.addTarget(self, action: #selector(fabTapped(_:)), for: .touchUpInside)
        return button
    }()

    @IBOutlet weak var uploadButton: UIButton!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    var labelarray = ["Capture the FrontView","Capture the View","Capture the Left-sideView" ,"Capture the View","Capture the RearView","Capture the View","Capture the Right-sideView","Capture the View"]
    
//    var labelarray = ["Capture the FrontView","Capture the View", "Capture RearView","Capture RightView","Capture LeftView","Capture View","Capture View","Capture View","Capture View"]
    var scan: UIImage!
    let row = UserDefaults.standard.value(forKey: "selectedrow")
    let manager = CouponManager()
    var uploadimage = [UIImage]()
    var imagedata1 :Data?
    var imagedata2 :Data?
    var imagedata3 :Data?
    var imagedata4 :Data?
    let mem = UserDefaults.standard.value(forKey: "memberId")
    let sess = UserDefaults.standard.value(forKey: "sessionId")
    let device = UserDefaults.standard.value(forKey: "deviceId")
    
    override func viewWillAppear(_ animated: Bool) {
        //super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let view = UIApplication.shared.keyWindow {
            view.addSubview(faButton)
            setupButton()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let view = UIApplication.shared.keyWindow, faButton.isDescendant(of: view) {
            faButton.removeFromSuperview()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        
       // uploadButton.layer.cornerRadius = 15
        self.addBackButton()
        var userd = UserDefaults.standard
        
        print(imagearray)
        
        print(scan)
        
        if (scan != nil)
        {
            
            let new =  loadImageFromDiskWith(fileName: "example.png")
            
            let resizedImage = new?.resizedTo1MB()

        
            if(row as! Int  == 0 ){
                
                imagearray[0] = resizedImage
                imagearray1 = imagearray
            }
        
            if(row as! Int  == 1 ){
            
                imagearray[1] = resizedImage
                imagearray1 = imagearray
                
            }
            
            if(row as! Int  == 2 ){
            
                imagearray[2] = resizedImage
                imagearray1 = imagearray
                
            }
            if(row as! Int  == 3 ){
                
                imagearray[3] = resizedImage
                imagearray1 = imagearray
            }
            
            if(row as! Int  == 4 ){
                
                imagearray[4] = resizedImage
                imagearray1 = imagearray
            }
        
            if(row as! Int  == 5 ){
            
                imagearray[5] = resizedImage
                imagearray1 = imagearray
                
            }
            
            if(row as! Int  == 6 ){
            
                imagearray[6] = resizedImage
                imagearray1 = imagearray
                
            }
            if(row as! Int  == 7 ){
                
                imagearray[7] = resizedImage
                imagearray1 = imagearray
                
            }
    tableView.reloadData()
        
            
        }
       
    }
    
    func setupTableView() {
        tableView.backgroundColor = .white
    }

    func setupButton() {
        NSLayoutConstraint.activate([
            //faButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -36),
            faButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20),
            faButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 120),
            
            
            faButton.heightAnchor.constraint(equalToConstant: 60),
            faButton.widthAnchor.constraint(equalToConstant: 150)
            ])
        faButton.setTitle("Upload", for: UIControl.State.normal)
        faButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        faButton.layer.cornerRadius = 15
        faButton.layer.masksToBounds = true
        //faButton.layer.borderColor = UIColor.lightGray.cgColor
        //faButton.layer.borderWidth = 4
        faButton.backgroundColor = #colorLiteral(red: 0, green: 0.5976013541, blue: 1, alpha: 1)
    }

    @objc func fabTapped(_ button: UIButton) {
        print("button tapped")
        
        if(imagearray1 .isEmpty)
            
        {
            let alert = UIAlertController(title: "Oops!", message: "Capture Atleast one Image to upload", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        else
        {
            
        if(imagearray1[0] == UIImage(named: "frontcar1"))
             {
                  imagearray1[0] = UIImage(named: "nil")
              }
        if(imagearray1[1] == UIImage(named: "backcar1"))
             {
                  imagearray1[1] = UIImage(named: "nil")
              }
        if(imagearray1[2] == UIImage(named: "rightcar1"))
             {
                  imagearray1[2] = UIImage(named: "nil")
              }
        if(imagearray1[3] == UIImage(named: "leftcar1"))
             {
                  imagearray1[3] = UIImage(named: "nil")
              }
            if(imagearray1[4] == UIImage(named: "frontmirrorcar"))
                 {
                      imagearray1[4] = UIImage(named: "nil")
                  }
            if(imagearray1[5] == UIImage(named: "frontsidecar1"))
                 {
                      imagearray1[5] = UIImage(named: "nil")
                  }
            if(imagearray1[6] == UIImage(named: "frontsidecar2"))
                 {
                      imagearray1[6] = UIImage(named: "nil")
                  }
            if(imagearray1[7] == UIImage(named: "backsidecar1"))
                 {
                      imagearray1[7] = UIImage(named: "nil")
                  }
        
             imagedata1 = imagearray1[0]?.pngData()
            
             imagedata2 = imagearray1[1]?.pngData()
            
              imagedata3 = imagearray1[2]?.pngData()
            
              imagedata4 = imagearray1[3]?.pngData()
            
   //  trythis(imagedata1: imagedata1, imagedata2: imagedata2, imagedata3: imagedata3, imagedata4: imagedata4)
            //
            
            let alert = UIAlertController(title: "Confirm!", message: "Do you want to continue with uploading?", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                    
                    self.Uploadmultiformpartdata()
                    
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
        }
    }
    
    func loadImageFromDiskWith(fileName: String) -> UIImage? {

      let documentDirectory = FileManager.SearchPathDirectory.documentDirectory

        let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)

        if let dirPath = paths.first {
            let imageUrl = URL(fileURLWithPath: dirPath).appendingPathComponent(fileName)
            let image = UIImage(contentsOfFile: imageUrl.path)
            return image

        }

        return nil
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return labelarray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "imagecell", for: indexPath)as! CameraTableViewCell
        
        cell.carimageview.image = nil
        cell.carimageview?.clipsToBounds = true
        cell.carimageview.contentMode = .scaleAspectFit
        
        cell.carimageview.autoresizingMask = [.flexibleTopMargin, .flexibleHeight, .flexibleRightMargin, .flexibleLeftMargin, .flexibleTopMargin, .flexibleWidth]
       
        cell.carlabel?.text = labelarray[indexPath.row]
        if (scan != nil)
        {
            cell.carimageview?.image = imagearray[indexPath.row]
        }
        else
        {
        cell.carimageview?.image = imagearray[indexPath.row]
        }
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var row = Int()
        row = indexPath.row
        
        let userd = UserDefaults.standard
        userd.set(row, forKey: "selectedrow")
        userd.synchronize()
        
        performSegue(withIdentifier: "camera", sender: imagearray[indexPath.row])

            }
    
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 32
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "camera"){
            if let indexPath = tableView.indexPathForSelectedRow {
                let nextVC = segue.destination as?CameraPicViewC
                //nextVC?.selectedImage = imagearray[indexPath.row]!
            }
        }
        
    }
//    @IBAction func uploadbtn(_ sender: Any) {
//
//        if(imagearray1 .isEmpty)
//
//        {
//            let alert = UIAlertController(title: "Oops!", message: "Capture Atleast one Image to upload", preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
//        }
//
//        else
//        {
//
//
//        if(imagearray1[0] == UIImage(named: "frontcar"))
//             {
//                  imagearray1[0] = UIImage(named: "nil")
//              }
//        if(imagearray1[1] == UIImage(named: "backcar"))
//             {
//                  imagearray1[1] = UIImage(named: "nil")
//              }
//        if(imagearray1[2] == UIImage(named: "sidecar"))
//             {
//                  imagearray1[2] = UIImage(named: "nil")
//              }
//        if(imagearray1[3] == UIImage(named: "topcar"))
//             {
//                  imagearray1[3] = UIImage(named: "nil")
//              }
//
//             imagedata1 = imagearray1[0]?.pngData()
//
//             imagedata2 = imagearray1[1]?.pngData()
//
//              imagedata3 = imagearray1[2]?.pngData()
//
//              imagedata4 = imagearray1[3]?.pngData()
//
//
//
//
//   //  trythis(imagedata1: imagedata1, imagedata2: imagedata2, imagedata3: imagedata3, imagedata4: imagedata4)
//
//            let alert = UIAlertController(title: "Sure?", message: "Do you want to continue with uploading?", preferredStyle: UIAlertController.Style.alert)
//                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
//
//                    self.trythis1()
//
//                }))
//                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
//                self.present(alert, animated: true, completion: nil)
//
//
//
//
//
//
//        }
//    }
    
    func Uploadmultiformpartdata(){
        
        faButton.isEnabled = false
        
        let  photoImageData = imagearray1.compactMap {  $0?.pngData() }
        
        var parameters: [String : Any] = [:]
        
        let urlString = "https://exless.com/api/add_quote"
    
        self.showSpinner(onView: self.view)
    
        parameters = ["member_id": mem, "device_id" :device, "session_id" : sess ]
        
        let headers: HTTPHeaders =
            ["Content-type": "multipart/form-data",
            "Accept": "application/json"]
        
        AF.upload(multipartFormData: { (multipartFormData) in
            
            let count = photoImageData.count
        
            for i in 0..<count{
                    multipartFormData.append(photoImageData[i], withName: "quote_image[\(i)]", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
                }
            
            for (key, value) in parameters {
               multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        
        }, to: CouponManager().QuoteURL, //URL Here
                  method: .post,
                  headers: headers)
                  .responseJSON { (resp) in
                      //defer{SVProgressHUD.dismiss()}
                      self.removeSpinner()
                  print(resp)
                      if let err = resp.error{
                                  print(err)
                          print("Something went Wrong!Try again Later")
                          let alert = UIAlertController(title: "Oops!", message: "Something went Wrong!Try again Later", preferredStyle: UIAlertController.Style.alert)
                          alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                          self.present(alert, animated: true, completion: nil)
                                  //onError?(err)
                                  return
                              }
                      
                              print("Successfully uploaded")
                      let alert = UIAlertController(title: "Success", message: "Successfully uploaded", preferredStyle: UIAlertController.Style.alert)
                      alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                      self.present(alert, animated: true, completion: nil)
                      
                      self.faButton.isEnabled = true
                      }

                  }

       
    func trythis(imagedata1 :Data?,imagedata2 :Data?,imagedata3 :Data?,imagedata4 :Data?)
    {
        var parameters: [String : Any] = [:]
        
        let urlString = "http://apiexless.rosterwand.com/backend/web/index.php?r=api_2_0_/add_quote"
    
        self.showSpinner(onView: self.view)
    
        parameters = ["member_id": mem, "session_id" : sess ,"device_id" :device ]
        
        let headers: HTTPHeaders =
            ["Content-type": "multipart/form-data",
            "Accept": "application/json"]
    AF.upload(multipartFormData: { (multipartFormData) in
        
        if let data = imagedata1
        {
        multipartFormData.append(data, withName: "face_view", fileName: "swift_file1.jpeg", mimeType: "image/png")
        }
        if let data = imagedata2
        {
        multipartFormData.append(data, withName: "rear_view", fileName: "swift_file2.jpeg", mimeType: "image/png")
        }

        if let data = imagedata3
        {
        multipartFormData.append(data, withName: "right_view", fileName: "swift_file3.jpeg", mimeType: "image/png")
        }

        if let data = imagedata4
        {
        multipartFormData.append(data, withName: "left_view", fileName: "swift_file4.jpeg", mimeType: "image/png")
        }

        for (key, value) in parameters {
           multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
        }
    }, to: urlString, //URL Here
              method: .post,
              headers: headers)
              .responseJSON { (resp) in
                  //defer{SVProgressHUD.dismiss()}
                  self.removeSpinner()
              print(resp)
                  if let err = resp.error{
                              print(err)
                      print("Something went Wrong!Try again Later")
                      let alert = UIAlertController(title: "Oops!", message: "Something went Wrong!Try again Later", preferredStyle: UIAlertController.Style.alert)
                      alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                      self.present(alert, animated: true, completion: nil)
                              //onError?(err)
                              return
                          }
                  
                 print("Successfully uploaded")
                  let alert = UIAlertController(title: "Success", message: "Successfully uploaded", preferredStyle: UIAlertController.Style.alert)
                  alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                  self.present(alert, animated: true, completion: nil)
                  }

              }

     }
extension UIImage {

func resized(withPercentage percentage: CGFloat) -> UIImage? {
    let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
    UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
    defer { UIGraphicsEndImageContext() }
    draw(in: CGRect(origin: .zero, size: canvasSize))
    return UIGraphicsGetImageFromCurrentImageContext()
}

func resizedTo1MB() -> UIImage? {
    guard let imageData = self.pngData() else { return nil }

    var resizingImage = self
    var imageSizeKB = Double(imageData.count) / 1000.0 // ! Or devide for 1024 if you need KB but not kB

    while imageSizeKB > 1000 { // ! Or use 1024 if you need KB but not kB
        guard let resizedImage = resizingImage.resized(withPercentage: 0.9),
              let imageData = resizedImage.pngData()
            else { return nil }

        resizingImage = resizedImage
        imageSizeKB = Double(imageData.count) / 1000.0 // ! Or devide for 1024 if you need KB but not kB
    }

    return resizingImage
}
}
extension UIViewController {
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            vSpinner?.removeFromSuperview()
            vSpinner = nil
        }
    }
}
extension UIViewController {

    func addBackButton() {
        let btnLeftMenu: UIButton = UIButton()
        //let image = UIImage(named: "backButtonImage");
        //btnLeftMenu.setImage(image, for: .normal)
        btnLeftMenu.setTitle("Back", for: .normal);
        btnLeftMenu.sizeToFit()
        btnLeftMenu.addTarget(self, action: #selector (backButtonClick(sender:)), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }

    @objc func backButtonClick(sender : UIButton) {
        self.navigationController?.popToRootViewController(animated: true);
    }
}


////
////  CameraTableViewC.swift
////  Smart Repair
////
////  Created by MyMac on 13/10/21.
////  Copyright © 2021 Infrap. All rights reserved.
////
//
//import UIKit
//import Alamofire
////Repair Quote
//var vSpinner : UIView?
//class CameraTableViewC: UITableViewController {
//
//
//    lazy var faButton: UIButton = {
//        let button = UIButton(frame: .zero)
//        button.translatesAutoresizingMaskIntoConstraints = false
//        button.backgroundColor = .blue
//        button.addTarget(self, action: #selector(fabTapped(_:)), for: .touchUpInside)
//        return button
//    }()
//
//    @IBOutlet weak var uploadButton: UIButton!
//    @IBOutlet weak var activity: UIActivityIndicatorView!
//    var labelarray = ["Capture the FrontView","Capture the View","Capture the Left-side View","Capture the View","Capture the RearView","Capture the View","Capture Right-side View","Capture the View"]
////    var labelarray = ["Capture FrontView", "Capture RearView","Capture RightView","Capture LeftView","Capture View","Capture View","Capture View","Capture View"]
//    var scan: UIImage!
//    let row = UserDefaults.standard.value(forKey: "selectedrow")
//    let manager = CouponManager()
//    var uploadimage = [UIImage]()
//    var imagedata1 :Data?
//    var imagedata2 :Data?
//    var imagedata3 :Data?
//    var imagedata4 :Data?
//    let mem = UserDefaults.standard.value(forKey: "memberId")
//    let sess = UserDefaults.standard.value(forKey: "sessionId")
//    let device = UserDefaults.standard.value(forKey: "deviceId")
//
//    override func viewWillAppear(_ animated: Bool) {
//        //super.viewWillDisappear(animated)
//        navigationController?.setNavigationBarHidden(false, animated: animated)
//
//    }
//
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        if let view = UIApplication.shared.keyWindow {
//            view.addSubview(faButton)
//            setupButton()
//        }
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        if let view = UIApplication.shared.keyWindow, faButton.isDescendant(of: view) {
//            faButton.removeFromSuperview()
//        }
//    }
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        setupTableView()
//
//       // uploadButton.layer.cornerRadius = 15
//        self.addBackButton()
//        var userd = UserDefaults.standard
//
//        print(imagearray)
//
//        print(scan)
//
//        if (scan != nil)
//        {
//
//            let new =  loadImageFromDiskWith(fileName: "example.png")
//
//            let resizedImage = new?.resizedTo1MB()
//
//
//            if(row as! Int  == 0 ){
//
//                imagearray[0] = resizedImage
//                imagearray1 = imagearray
//            }
//
//            if(row as! Int  == 1 ){
//
//                imagearray[1] = resizedImage
//                imagearray1 = imagearray
//
//            }
//
//            if(row as! Int  == 2 ){
//
//                imagearray[2] = resizedImage
//                imagearray1 = imagearray
//
//            }
//            if(row as! Int  == 3 ){
//
//                imagearray[3] = resizedImage
//                imagearray1 = imagearray
//            }
//
//            if(row as! Int  == 4 ){
//
//                imagearray[4] = resizedImage
//                imagearray1 = imagearray
//            }
//
//            if(row as! Int  == 5 ){
//
//                imagearray[5] = resizedImage
//                imagearray1 = imagearray
//
//            }
//
//            if(row as! Int  == 6 ){
//
//                imagearray[6] = resizedImage
//                imagearray1 = imagearray
//
//            }
//            if(row as! Int  == 7 ){
//
//                imagearray[7] = resizedImage
//                imagearray1 = imagearray
//
//            }
//    tableView.reloadData()
//
//
//        }
//
//    }
//
//    func setupTableView() {
//        tableView.backgroundColor = .white
//    }
//
//    func setupButton() {
//        NSLayoutConstraint.activate([
//            //faButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -36),
//            faButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20),
//            faButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 120),
//
//
//            faButton.heightAnchor.constraint(equalToConstant: 60),
//            faButton.widthAnchor.constraint(equalToConstant: 150)
//            ])
//        faButton.setTitle("Upload", for: UIControl.State.normal)
//        faButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
//        faButton.layer.cornerRadius = 15
//        faButton.layer.masksToBounds = true
//        //faButton.layer.borderColor = UIColor.lightGray.cgColor
//        //faButton.layer.borderWidth = 4
//        faButton.backgroundColor = #colorLiteral(red: 0, green: 0.5976013541, blue: 1, alpha: 1)
//    }
//
//    @objc func fabTapped(_ button: UIButton) {
//        print("button tapped")
//
//        if(imagearray1 .isEmpty)
//
//        {
//            let alert = UIAlertController(title: "Oops!", message: "Capture Atleast one Image to upload", preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
//        }
//
//        else
//        {
//
//
//        if(imagearray1[0] == UIImage(named: "frontmirrorcar"))
//             {
//                  imagearray1[0] = UIImage(named: "nil")
//              }
//        if(imagearray1[1] == UIImage(named: "frontsidecar2"))
//             {
//                  imagearray1[1] = UIImage(named: "nil")
//              }
//        if(imagearray1[2] == UIImage(named: "rightcar1"))
//             {
//                  imagearray1[2] = UIImage(named: "nil")
//              }
//        if(imagearray1[3] == UIImage(named: "backsidecar1"))
//             {
//                  imagearray1[3] = UIImage(named: "nil")
//              }
//            if(imagearray1[4] == UIImage(named: "frontcar1"))
//                 {
//                      imagearray1[4] = UIImage(named: "nil")
//                  }
//            if(imagearray1[5] == UIImage(named: "backcar1"))
//                 {
//                      imagearray1[5] = UIImage(named: "nil")
//                  }
//            if(imagearray1[6] == UIImage(named: "leftcar1"))
//                 {
//                      imagearray1[6] = UIImage(named: "nil")
//                  }
//            if(imagearray1[7] == UIImage(named: "frontsidecar1"))
//                 {
//                      imagearray1[7] = UIImage(named: "nil")
//                  }
//
//
////            if(imagearray1[0] == UIImage(named: "frontcar1"))
////                 {
////                      imagearray1[0] = UIImage(named: "nil")
////                  }
////            if(imagearray1[1] == UIImage(named: "backcar1"))
////                 {
////                      imagearray1[1] = UIImage(named: "nil")
////                  }
////            if(imagearray1[2] == UIImage(named: "rightcar1"))
////                 {
////                      imagearray1[2] = UIImage(named: "nil")
////                  }
////            if(imagearray1[3] == UIImage(named: "leftcar1"))
////                 {
////                      imagearray1[3] = UIImage(named: "nil")
////                  }
////                if(imagearray1[4] == UIImage(named: "frontmirrorcar"))
////                     {
////                          imagearray1[4] = UIImage(named: "nil")
////                      }
////                if(imagearray1[5] == UIImage(named: "frontsidecar1"))
////                     {
////                          imagearray1[5] = UIImage(named: "nil")
////                      }
////                if(imagearray1[6] == UIImage(named: "frontsidecar2"))
////                     {
////                          imagearray1[6] = UIImage(named: "nil")
////                      }
////                if(imagearray1[7] == UIImage(named: "backsidecar1"))
////                     {
////                          imagearray1[7] = UIImage(named: "nil")
////                      }
////
////
//             imagedata1 = imagearray1[0]?.pngData()
//
//             imagedata2 = imagearray1[1]?.pngData()
//
//              imagedata3 = imagearray1[2]?.pngData()
//
//              imagedata4 = imagearray1[3]?.pngData()
//
//   //  trythis(imagedata1: imagedata1, imagedata2: imagedata2, imagedata3: imagedata3, imagedata4: imagedata4)
//            //
//
//            let alert = UIAlertController(title: "Confirm!", message: "Do you want to continue with uploading?", preferredStyle: UIAlertController.Style.alert)
//                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
//
//                    self.Uploadmultiformpartdata()
//
//                }))
//                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
//                self.present(alert, animated: true, completion: nil)
//        }
//    }
//
//    func loadImageFromDiskWith(fileName: String) -> UIImage? {
//
//      let documentDirectory = FileManager.SearchPathDirectory.documentDirectory
//
//        let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
//        let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)
//
//        if let dirPath = paths.first {
//            let imageUrl = URL(fileURLWithPath: dirPath).appendingPathComponent(fileName)
//            let image = UIImage(contentsOfFile: imageUrl.path)
//            return image
//
//        }
//
//        return nil
//    }
//
//    // MARK: - Table view data source
//
//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 1
//    }
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete implementation, return the number of rows
//        return labelarray.count
//    }
//
//
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        let cell = tableView.dequeueReusableCell(withIdentifier: "imagecell", for: indexPath)as! CameraTableViewCell
//
//        cell.carimageview.image = nil
//        cell.carimageview?.clipsToBounds = true
//        cell.carimageview.contentMode = .scaleAspectFit
//
//        cell.carimageview.autoresizingMask = [.flexibleTopMargin, .flexibleHeight, .flexibleRightMargin, .flexibleLeftMargin, .flexibleTopMargin, .flexibleWidth]
//
//        cell.carlabel?.text = labelarray[indexPath.row]
//        if (scan != nil)
//        {
//            cell.carimageview?.image = imagearray[indexPath.row]
//        }
//        else
//        {
//        cell.carimageview?.image = imagearray[indexPath.row]
//        }
//        return cell
//    }
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        var row = Int()
//        row = indexPath.row
//
//        let userd = UserDefaults.standard
//        userd.set(row, forKey: "selectedrow")
//        userd.synchronize()
//
//        performSegue(withIdentifier: "camera", sender: imagearray[indexPath.row])
//
//            }
//
//
//    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        return UIView()
//    }
//
//    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 32
//    }
//
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if (segue.identifier == "camera"){
//            if let indexPath = tableView.indexPathForSelectedRow {
//                let nextVC = segue.destination as?CameraPicViewC
//                //nextVC?.selectedImage = imagearray[indexPath.row]!
//            }
//        }
//
//    }
////    @IBAction func uploadbtn(_ sender: Any) {
////
////        if(imagearray1 .isEmpty)
////
////        {
////            let alert = UIAlertController(title: "Oops!", message: "Capture Atleast one Image to upload", preferredStyle: UIAlertController.Style.alert)
////            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
////            self.present(alert, animated: true, completion: nil)
////        }
////
////        else
////        {
////
////
////        if(imagearray1[0] == UIImage(named: "frontcar"))
////             {
////                  imagearray1[0] = UIImage(named: "nil")
////              }
////        if(imagearray1[1] == UIImage(named: "backcar"))
////             {
////                  imagearray1[1] = UIImage(named: "nil")
////              }
////        if(imagearray1[2] == UIImage(named: "sidecar"))
////             {
////                  imagearray1[2] = UIImage(named: "nil")
////              }
////        if(imagearray1[3] == UIImage(named: "topcar"))
////             {
////                  imagearray1[3] = UIImage(named: "nil")
////              }
////
////             imagedata1 = imagearray1[0]?.pngData()
////
////             imagedata2 = imagearray1[1]?.pngData()
////
////              imagedata3 = imagearray1[2]?.pngData()
////
////              imagedata4 = imagearray1[3]?.pngData()
////
////
////
////
////   //  trythis(imagedata1: imagedata1, imagedata2: imagedata2, imagedata3: imagedata3, imagedata4: imagedata4)
////
////            let alert = UIAlertController(title: "Sure?", message: "Do you want to continue with uploading?", preferredStyle: UIAlertController.Style.alert)
////                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
////
////                    self.trythis1()
////
////                }))
////                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
////                self.present(alert, animated: true, completion: nil)
////
////
////
////
////
////
////        }
////    }
//
//    func Uploadmultiformpartdata(){
//
//        faButton.isEnabled = false
//
//        let  photoImageData = imagearray1.compactMap {  $0?.pngData() }
//
//        var parameters: [String : Any] = [:]
//
//        let urlString = "http://exless.com/api/add_quote"
//
//        self.showSpinner(onView: self.view)
//
//        parameters = ["member_id": mem, "session_id" : sess ,"device_id" :device ]
//
//        let headers: HTTPHeaders =
//            ["Content-type": "multipart/form-data",
//            "Accept": "application/json"]
//
//        AF.upload(multipartFormData: { (multipartFormData) in
//
//            let count = photoImageData.count
//
//            for i in 0..<count{
//                    multipartFormData.append(photoImageData[i], withName: "quote_image[\(i)]", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
//                }
//
//            for (key, value) in parameters {
//               multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
//            }
//
//        }, to: urlString, //URL Here
//                  method: .post,
//                  headers: headers)
//                  .responseJSON { (resp) in
//                      //defer{SVProgressHUD.dismiss()}
//                      self.removeSpinner()
//                  print(resp)
//                      if let err = resp.error{
//                                  print(err)
//                          print("Something went Wrong!Try again Later")
//                          let alert = UIAlertController(title: "Oops!", message: "Something went Wrong!Try again Later", preferredStyle: UIAlertController.Style.alert)
//                          alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//                          self.present(alert, animated: true, completion: nil)
//                                  //onError?(err)
//                                  return
//                              }
//
//                              print("Successfully uploaded")
//                      let alert = UIAlertController(title: "Success", message: "Successfully uploaded", preferredStyle: UIAlertController.Style.alert)
//                      alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//                      self.present(alert, animated: true, completion: nil)
//
//                      self.faButton.isEnabled = true
//                      }
//
//                  }
//
//
//    func trythis(imagedata1 :Data?,imagedata2 :Data?,imagedata3 :Data?,imagedata4 :Data?)
//    {
//        var parameters: [String : Any] = [:]
//
//        let urlString = "http://apiexless.rosterwand.com/backend/web/index.php?r=api_2_0_/add_quote"
//
//        self.showSpinner(onView: self.view)
//
//        parameters = ["member_id": mem, "session_id" : sess ,"device_id" :device ]
//
//        let headers: HTTPHeaders =
//            ["Content-type": "multipart/form-data",
//            "Accept": "application/json"]
//    AF.upload(multipartFormData: { (multipartFormData) in
//
//        if let data = imagedata1
//        {
//        multipartFormData.append(data, withName: "face_view", fileName: "swift_file1.jpeg", mimeType: "image/png")
//        }
//        if let data = imagedata2
//        {
//        multipartFormData.append(data, withName: "rear_view", fileName: "swift_file2.jpeg", mimeType: "image/png")
//        }
//
//        if let data = imagedata3
//        {
//        multipartFormData.append(data, withName: "right_view", fileName: "swift_file3.jpeg", mimeType: "image/png")
//        }
//
//        if let data = imagedata4
//        {
//        multipartFormData.append(data, withName: "left_view", fileName: "swift_file4.jpeg", mimeType: "image/png")
//        }
//
//        for (key, value) in parameters {
//           multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
//        }
//    }, to: urlString, //URL Here
//              method: .post,
//              headers: headers)
//              .responseJSON { (resp) in
//                  //defer{SVProgressHUD.dismiss()}
//                  self.removeSpinner()
//              print(resp)
//                  if let err = resp.error{
//                              print(err)
//                      print("Something went Wrong!Try again Later")
//                      let alert = UIAlertController(title: "Oops!", message: "Something went Wrong!Try again Later", preferredStyle: UIAlertController.Style.alert)
//                      alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//                      self.present(alert, animated: true, completion: nil)
//                              //onError?(err)
//                              return
//                          }
//
//                 print("Successfully uploaded")
//                  let alert = UIAlertController(title: "Success", message: "Successfully uploaded", preferredStyle: UIAlertController.Style.alert)
//                  alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//                  self.present(alert, animated: true, completion: nil)
//                  }
//
//              }
//
//     }
//extension UIImage {
//
//func resized(withPercentage percentage: CGFloat) -> UIImage? {
//    let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
//    UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
//    defer { UIGraphicsEndImageContext() }
//    draw(in: CGRect(origin: .zero, size: canvasSize))
//    return UIGraphicsGetImageFromCurrentImageContext()
//}
//
//func resizedTo1MB() -> UIImage? {
//    guard let imageData = self.pngData() else { return nil }
//
//    var resizingImage = self
//    var imageSizeKB = Double(imageData.count) / 1000.0 // ! Or devide for 1024 if you need KB but not kB
//
//    while imageSizeKB > 1000 { // ! Or use 1024 if you need KB but not kB
//        guard let resizedImage = resizingImage.resized(withPercentage: 0.9),
//              let imageData = resizedImage.pngData()
//            else { return nil }
//
//        resizingImage = resizedImage
//        imageSizeKB = Double(imageData.count) / 1000.0 // ! Or devide for 1024 if you need KB but not kB
//    }
//
//    return resizingImage
//}
//}
//extension UIViewController {
//    func showSpinner(onView : UIView) {
//        let spinnerView = UIView.init(frame: onView.bounds)
//        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
//        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
//        ai.startAnimating()
//        ai.center = spinnerView.center
//
//        DispatchQueue.main.async {
//            spinnerView.addSubview(ai)
//            onView.addSubview(spinnerView)
//        }
//
//        vSpinner = spinnerView
//    }
//
//    func removeSpinner() {
//        DispatchQueue.main.async {
//            vSpinner?.removeFromSuperview()
//            vSpinner = nil
//        }
//    }
//}
//extension UIViewController {
//
//    func addBackButton() {
//        let btnLeftMenu: UIButton = UIButton()
//        //let image = UIImage(named: "backButtonImage");
//        //btnLeftMenu.setImage(image, for: .normal)
//        btnLeftMenu.setTitle("Back", for: .normal);
//        btnLeftMenu.sizeToFit()
//        btnLeftMenu.addTarget(self, action: #selector (backButtonClick(sender:)), for: .touchUpInside)
//        let barButton = UIBarButtonItem(customView: btnLeftMenu)
//        self.navigationItem.leftBarButtonItem = barButton
//    }
//
//    @objc func backButtonClick(sender : UIButton) {
//        self.navigationController?.popToRootViewController(animated: true);
//    }
//}
