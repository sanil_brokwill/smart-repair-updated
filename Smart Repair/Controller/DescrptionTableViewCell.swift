//
//  DescrptionTableViewCell.swift
//  Smart Repair
//
//  Created by MyMac on 16/11/21.
//  Copyright © 2021 Infrap. All rights reserved.
//

import UIKit

class DescrptionTableViewCell: UITableViewCell {

    @IBOutlet weak var sellLabel: UILabel!
    @IBOutlet weak var sellTextview: UITextView!
    @IBOutlet weak var sellButton: UIButton!
    @IBOutlet weak var outerView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let color: UIColor = UIColor( red: CGFloat(197/255.0), green: CGFloat(197/255.0), blue: CGFloat(197/255.0), alpha: CGFloat(1.0) )
        
        
        outerView.layer.cornerRadius = 15
        outerView.clipsToBounds = true
        outerView.layer.masksToBounds = false
        outerView.layer.shadowRadius = 8
        outerView.layer.shadowOpacity = 0.6
        outerView.layer.shadowOffset = CGSize(width: 3, height: 3)
        outerView.layer.shadowColor = color.cgColor
        sellButton.layer.cornerRadius = 15
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
