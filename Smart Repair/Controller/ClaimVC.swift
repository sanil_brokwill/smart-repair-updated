//
//  ClaimVC.swift
//  Smart Repair
//
//  Created by Infrap on 14/9/21.
//  Copyright © 2021 Infrap. All rights reserved.
//

import UIKit

class ClaimVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var Claimtableview: UITableView!
    
    var cars = ["Claim1","Claim2","Claim3"]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cars.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
      let  cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ClaimTableViewcell
        
        return cell
       
    }
//    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//       return UITableView.automaticDimension
//        
//
//
//    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Claimtableview.register(ClaimTableViewcell.self, forCellReuseIdentifier: "cell")
        Claimtableview.estimatedRowHeight = 454
        Claimtableview.rowHeight = UITableView.automaticDimension
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
