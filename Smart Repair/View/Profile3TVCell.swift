//
//  Profile3TVCell.swift
//  Smart Repair
//
//  Created by MyMac on 16/12/21.
//  Copyright © 2021 Infrap. All rights reserved.
//

import UIKit

class Profile3TVCell: UITableViewCell {

    @IBOutlet weak var PlanLabel: UILabel!
    @IBOutlet weak var ColourLabel: UILabel!
    @IBOutlet weak var ModelLabel: UILabel!
    @IBOutlet weak var BrandLabel: UILabel!
    @IBOutlet weak var Registerlabel: UILabel!
    @IBOutlet weak var OuterView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        OuterView.layer.cornerRadius = 15
        OuterView.clipsToBounds = true
        OuterView.layer.masksToBounds = false
        OuterView.layer.shadowRadius = 5
        OuterView.layer.shadowOpacity = 0.3
        OuterView.layer.shadowOffset = CGSize(width: 3, height: 3)
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
