//
//  YoutubeVCell.swift
//  Smart Repair
//
//  Created by Infrap on 7/9/21.
//  Copyright © 2021 Infrap. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

class YoutubeVCell: UITableViewCell {

   // @IBOutlet weak var youtubeview: UIView!
    @IBOutlet weak var newsandeventscontrol: UIControl!
    
    @IBOutlet weak var readmorelabel: UILabel!
    @IBOutlet weak var youtubeview: YTPlayerView!
    @IBOutlet weak var TimeLabel1: UILabel!
    @IBOutlet weak var NameLabel1: UILabel!
    
    @IBOutlet weak var Deletebutton: UIButton!
    
    //@IBOutlet weak var Youtubeplayer: YTPlayerView!
    
    @IBOutlet weak var YoutubeHeadingLbl: UILabel!
    
    @IBOutlet weak var YoutubeDescrpview: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //youtubeview.delegate = self
        
        //youtubeview.load(withVideoId: "YE7VzlLtp-4", playerVars: ["playsinline": "1"])
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//extension YoutubeVCell: YTPlayerViewDelegate {
//    func playerViewPreferredWebViewBackgroundColor(_ playerView: YTPlayerView) -> UIColor {
//        return UIColor.black
//    }
//
//    func playerViewPreferredInitialLoading(_ playerView: YTPlayerView) -> UIView? {
////        let customLoadingView = UIView()
////        Create a custom loading view
////        return customLoadingView
//    }
//}

