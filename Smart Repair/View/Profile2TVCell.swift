//
//  Profile2TVCell.swift
//  Smart Repair
//
//  Created by MyMac on 16/12/21.
//  Copyright © 2021 Infrap. All rights reserved.
//

import UIKit

class Profile2TVCell: UITableViewCell {

    @IBOutlet weak var cardExpiry: UILabel!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var cardNumber: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
