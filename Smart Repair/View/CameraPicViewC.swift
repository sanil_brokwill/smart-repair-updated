//
//  CameraPicViewC.swift
//  Smart Repair
//
//  Created by MyMac on 13/10/21.
//  Copyright © 2021 Infrap. All rights reserved.
//

import UIKit
import AVFoundation
import Foundation

// repair quote image capturing page
class CameraPicViewC: UIViewController {

    @IBOutlet weak var ImageOverlay: UIImageView!
    @IBOutlet weak var shapeLayer: UIView!
    @IBOutlet weak var btnCapture: UIButton!
    
    var selectedImage = UIImage()
    //var imagearray1 = [TestStruct]()
    var scan = UIImage()
    
    var row = Int()
    let manager = CouponManager()
    var userData  = UserData.sharedInstance
    var userId = String()
    override func viewWillAppear(_ animated: Bool) {
       // super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
        //navigationController?.removeViewController(CameraPicViewC.self)
    }
    
    var  imagearray2 = [UIImage(named: "frontmirrorcar"), UIImage(named: "frontsidecar2"), UIImage(named: "rightcar1"), UIImage(named: "backsidecar1"),UIImage(named: "frontcar1"), UIImage(named: "backcar1"), UIImage(named: "leftcar1"), UIImage(named: "frontsidecar1")]
    
//    var  imagearray2 = [UIImage(named: "frontcar1"), UIImage(named: "backcar1"), UIImage(named: "rightcar1"), UIImage(named: "leftcar1"),UIImage(named: "frontmirrorcar"), UIImage(named: "frontsidecar1"), UIImage(named: "frontsidecar2"), UIImage(named: "backsidecar1")]
    let captureSession = AVCaptureSession()
    let stillImageOutput = AVCaptureStillImageOutput()
    var previewLayer : AVCaptureVideoPreviewLayer?
    
    // If we find a device we'll store it here for later use
    var captureDevice : AVCaptureDevice?

    override func viewDidLoad() {
        super.viewDidLoad()
        row = UserDefaults.standard.value(forKey: "selectedrow") as! Int
        let midX = self.view.bounds.midX
        let midY = self.view.bounds.midY

        let circlePath = UIBezierPath(arcCenter: CGPoint(x: midX,y: midY), radius: CGFloat(20), startAngle: CGFloat(0), endAngle:CGFloat(M_PI * 2), clockwise: true)

        let shapeLayer = CAShapeLayer()

        shapeLayer.path = circlePath.cgPath
        //change the fill color
        shapeLayer.fillColor = UIColor.clear.cgColor
        //you can change the stroke color
        shapeLayer.strokeColor = UIColor.blue.cgColor
        //you can change the line width
        shapeLayer.lineWidth = 2.5
       // shapeLayer.backgroundColor = UIColor(patternImage: UIImage(named: "camera")!).cgColor

        view.layer.addSublayer(shapeLayer)
        print("Shape layer drawn")
        //=====================
        captureSession.sessionPreset = AVCaptureSession.Preset.high
        //imgOverlay.backgroundColor = UIColor(patternImage: UIImage(named: "frontcar" )!)
        
        ImageOverlay.image = imagearray2[row]
       // ImageOverlay.contentMode = .scaleAspectFill
        if let devices = AVCaptureDevice.devices() as? [AVCaptureDevice] {
            // Loop through all the capture devices on this phone
            for device in devices {
                // Make sure this particular device supports video
                if (device.hasMediaType(AVMediaType.video)) {
                    // Finally check the position and confirm we've got the back camera
                    if(device.position == AVCaptureDevice.Position.back) {
                        captureDevice = device
                        if captureDevice != nil {
                            print("Capture device found")
                            beginSession()
                        }
                    }
                }
            }
        }
        
        let fileManager = FileManager.default
        let yourProjectImagesPath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("example\([self.row])")
        if fileManager.fileExists(atPath: yourProjectImagesPath) {
        try! fileManager.removeItem(atPath: yourProjectImagesPath)
        }
        
        var swipeflag = UserDefaults.standard.value(forKey: "swipeflag")
        
        
        if(swipeflag as! String  == "0")
        {
        showToast(controller: self, message : "Swipe left to go back", seconds: 3.0)
            
        }
        
    }
    
    
    
    func showToast(controller: UIViewController, message : String, seconds: Double) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.view.backgroundColor = UIColor.black
        alert.view.alpha = 0.6
        alert.view.layer.cornerRadius = 15
        controller.present(alert, animated: true)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
            alert.dismiss(animated: true)
            
            let userd = UserDefaults.standard
            userd.set("1", forKey: "swipeflag")
            userd.synchronize()
            
            
        }
    }
    
    @IBAction func actionCameraCapture(_ sender: AnyObject) {

        print("Camera button pressed")
        saveToCamera()
    }

    func beginSession() {

        do {
            try captureSession.addInput(AVCaptureDeviceInput(device: captureDevice!))
            stillImageOutput.outputSettings = [AVVideoCodecKey:AVVideoCodecJPEG]

            if captureSession.canAddOutput(stillImageOutput) {
                captureSession.addOutput(stillImageOutput)
            }

        }
        catch {
            print("error: \(error.localizedDescription)")
        }

         let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        self.view.layer.addSublayer(previewLayer)
        previewLayer.frame = self.view.layer.frame
       captureSession.startRunning()
        print("Capture session running")
       // self.view.addSubview(navigationBar)
        self.view.addSubview(ImageOverlay)
        self.view.addSubview(btnCapture)
            }

  func saveToCamera() {
        
        if let videoConnection = stillImageOutput.connection(with: AVMediaType.video) {
              stillImageOutput.captureStillImageAsynchronously(from: videoConnection, completionHandler: { (CMSampleBuffer, Error) in

                if let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(CMSampleBuffer!) {
                    if let cameraImage = UIImage(data: imageData) {

                    
                       UIImageWriteToSavedPhotosAlbum(cameraImage, nil, nil, nil)
                    
                        
                        self.scan = cameraImage
                        
                        self.saveImage(imageName: "example.png", image:self.scan )
                    }

                    self.performSegue(withIdentifier: "backtotable", sender: nil)
                    
                }
            })
        }
    }
    //save image to local storage
    func saveImage(imageName: String, image: UIImage) {


     guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }

        let fileName = imageName
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        guard let data = image.jpegData(compressionQuality: 1) else { return }

        //Checks if file exists, removes it if so.
        if FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try FileManager.default.removeItem(atPath: fileURL.path)
                print("Removed old image")
            } catch let removeError {
                print("couldn't remove file at path", removeError)
            }

        }

        do {
            try data.write(to: fileURL)
        } catch let error {
            print("error saving file with error", error)
        }

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "backtotable"){
            let yourNextViewController = (segue.destination as! CameraTableViewC)
            yourNextViewController.scan = scan
            print(scan)
            }
        }
}
extension UINavigationController {
    
    func removeViewController(_ controller: UIViewController.Type) {
        if let viewController = viewControllers.first(where: { $0.isKind(of: controller.self) }) {
            viewController.removeFromParent()
        }
    }
}


extension CameraPicViewC:UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}




