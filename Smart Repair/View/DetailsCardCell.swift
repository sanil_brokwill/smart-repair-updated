
import UIKit

class DetailsCardCell: UITableViewCell {
    
    @IBOutlet weak var myView: UIView!
    @IBOutlet weak var htmlTextView: UITextView!
    @IBOutlet weak var expiryDateLabel: UILabel!
    @IBOutlet weak var startingDateLabel: UILabel!
    
    var couponData :CouponData?{
        didSet{
      htmlTextView.attributedText = couponData?.coupon_discount.htmlToAttributedString
            htmlTextView.font = .systemFont(ofSize: 40)
            let couponData1 = couponData
            print(couponData1)
            let timeInstance = TimeToReadable()
            let date = timeInstance.getTimeFormatted(from: couponData!.coupon_expires_on)
            expiryDateLabel.text = "Expires on \(date)"
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        setUpView()
    }
    
      func loadData(){
        expiryDateLabel.text = "Expires on \(couponData?.coupon_expires_on ?? "No Date Available")"
        }
    
        func setUpView(){
            if let safeView = myView{
            safeView.layer.cornerRadius = 5
            safeView.layer.shadowRadius = 3
            safeView.layer.shadowOffset = .zero
            safeView.layer.shadowOpacity = 0.3
            }
        }
            
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
