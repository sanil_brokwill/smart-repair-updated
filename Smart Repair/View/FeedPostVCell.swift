//
//  FeedPostVCell.swift
//  Smart Repair
//
//  Created by Infrap on 7/9/21.
//  Copyright © 2021 Infrap. All rights reserved.
//

import UIKit
import ExpandableLabel

protocol FeedPostVCellDelegate {
    func moreTapped(cell: FeedPostVCell)
}


class FeedPostVCell: UITableViewCell {
    
    @IBOutlet weak var Outerview: UIView!
    
    @IBOutlet weak var descrptionlabel: UILabel!
    @IBOutlet weak var FeedpostControl: UIControl!
    
    @IBOutlet weak var FeedDescrpView: UITextView!
    @IBOutlet weak var FeedImg: UILabel!
    @IBOutlet weak var FeedTimelbl: UILabel!
    @IBOutlet weak var FeedNamelbl: UILabel!
    
    @IBOutlet weak var FeedDeleteBtn: UIButton!
    
    @IBOutlet weak var FeedImgView: UIImageView!
    
    @IBOutlet weak var showmorebtn: UIButton!
    
    @IBOutlet weak var descriptionlbl: ExpandableLabel!
    
    var isExpanded: Bool = false
    var delegate: FeedPostVCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
    //showmorebtn.isHidden = fa
        showmorebtn.isEnabled = false
        sizeToFit()
        layoutIfNeeded()
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.labelAction(gesture:)))
        
        descrptionlabel.addGestureRecognizer(tap)
        descrptionlabel.isUserInteractionEnabled = true
                tap.delegate = self
       
        //FeedDescrpView.isHidden = true
        //FeedDescrpView.is
        // Initialization code
    }
    
    @objc func labelAction(gesture: UITapGestureRecognizer)
        {
           
            descrptionlabel.numberOfLines = descrptionlabel.numberOfLines == 0 ? 2 : 0
            UIView.animate(withDuration: 0.5) {
                self.descrptionlabel.superview?.layoutIfNeeded()
                  }
            //self.showmorebtn.setTitle("show less", for: UIControl.State.normal)
        }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func showmoretapped(_ sender: Any) {
        
    }
    

}

extension UILabel {

    func retrieveTextHeight () -> CGFloat {
        let attributedText = NSAttributedString(string: self.text!, attributes: [NSAttributedString.Key.font:self.font])

        let rect = attributedText.boundingRect(with: CGSize(width: self.frame.size.width, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)

        return ceil(rect.size.height)
    }

}
