//
//  ProfileData.swift
//  Smart Repair
//
//  Created by MyMac on 17/12/21.
//  Copyright © 2021 Infrap. All rights reserved.
//

import Foundation

// MARK: - ProfileResponse
struct ProfileResponse: Decodable {
    let status: Int
    let meta: Meta4
    let message: String
    let data: [ProfileData]
    let errors: String
    let totalItems: Int

    enum CodingKeys: String, CodingKey {
        case status, meta, message, data, errors
        case totalItems = "total_items"
    }
}

// MARK: - Datum
struct ProfileData: Decodable {
    let memberID: Int
    let memberName, memberPhone: String
    let memberImage: String
    let memberEmail : String
    let memberVehicles: [MemberVehicle]
    

    enum CodingKeys: String, CodingKey {
        case memberID = "member_id"
        case memberName = "member_name"
        case memberPhone = "member_phone"
        case memberImage = "member_image"
        case memberEmail = "member_email"
        case memberVehicles = "member_vehicles"
    }
}

// MARK: - MemberVehicle
struct MemberVehicle: Decodable {
    let id: Int
    let vehicleNumber, vehicleMake, vehicleModel, vehicleColour: String
    let vehiclePolicy: String

    enum CodingKeys: String, CodingKey {
        case id
        case vehicleNumber = "vehicle_number"
        case vehicleMake = "vehicle_make"
        case vehicleModel = "vehicle_model"
        case vehicleColour = "vehicle_colour"
        case vehiclePolicy = "vehicle_policy"
    }
}

// MARK: - Meta
struct Meta4: Decodable {
    let code: Int
    let message: String
}



//{
//    "status": 0,
//    "meta": {
//        "code": 200,
//        "message": "success"
//    },
//    "message": "success",
//    "data": {
//        "member_id": 2,
//        "member_name": "Brokwill Test 1",
//        "member_phone": "9495949501",
//        "member_image": "9495949501",
//        "member_vehicles": [
//            {
//                "id": 2,
//                "vehicle_number": "KL-49-2211",
//                "vehicle_make": "Audi",
//                "vehicle_model": "A4",
//                "vehicle_colour": "Red",
//                "vehicle_policy": "1382022"
//            }
//        ]
//    },
//    "errors": "error",
//    "total_items": 5
//}





























//{
//    "status": 0,
//    "meta": {
//        "code": 200,
//        "message": "success"
//    },
//    "message": "success",
//    "data": [
//        {
//
//            "member_id": 1,
//            "member_name": "Prasad",
//            "member_phone": "8848123909",
//            "member_image": "http://localhost/Exless/backend/web/upload/profile/1639117837.jpg",
//            "member_vehicles": [
//                {
//                    "id": 108,
//                    "vehicle_number": "KL-49-2255",
//                    "vehicle_make": "Toyota",
//                    "vehicle_model": "Etios",
//                    "vehicle_colour": "White",
//                    "vehicle_policy": "123456"
//                },
//                {
//                    "id": 109,
//                    "vehicle_number": "KL-49-6660",
//                    "vehicle_make": "Maruthi",
//                    "vehicle_model": "Celerio",
//                    "vehicle_colour": "Blue",
//                    "vehicle_policy": "456123"
//                }
//            ],
//            "member_cards": [
//                {
//                    "id": 11,
//                    "card_number": "121212121212xxxx",
//                    "card_expiry": "07-23"
//                },
//                {
//                    "id": 12,
//                    "card_number": "424242424242xxxx",
//                    "card_expiry": "12-22"
//                }
//            ]
//        }
//    ],
//    "errors": "error",
//    "total_items": 1
//}
