

import UIKit
import Kingfisher
class MarketItemCell: UITableViewCell {

    @IBOutlet weak var productImage: UIImageView!
//    @IBOutlet weak var outerImageView: UIView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    //@IBOutlet weak var enquireButton: UIButton!
   // @IBOutlet weak var viewDetailsButton: UIButton!
    @IBOutlet weak var pageController: UIPageControl!
    
    @IBOutlet weak var soldOutBanner: UIImageView!
    @IBOutlet weak var productCollectionView: UICollectionView!
   var thisWidth:CGFloat = 0
   var ownerPage:MarketListVC?
   
    
   
     
    
    var item: MarketItem?{
        didSet{
            if let item1 = item{
                productName.text = item1.market_brand
                productPrice.text = "$\(item1.market_price!)"//String(item1.market_price!)
                imageObjectArray = item?.market_images
                if item1.market_sold_status != 1{
                    soldOutBanner.isHidden = true
                }else{
                    soldOutBanner.isHidden = false
                }
            }
            
        }
    }
     var imageObjectArray:[ImageObject]?{
       
        didSet{
            pageController.numberOfPages = imageObjectArray!.count
            productCollectionView.reloadData()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
       
        // Initialization code
//        outerImageView.layer.cornerRadius = 10
//        outerImageView.layer.shadowOffset = .zero
//        outerImageView.layer.shadowRadius = 2
//        outerImageView.layer.shadowOpacity = 0.2
        
//        enquireButton.layer.cornerRadius = 10
//        enquireButton.layer.borderColor = #colorLiteral(red: 0, green: 0.3018001914, blue: 0.8385717273, alpha: 1)
//        enquireButton.layer.borderWidth = 2
      //  viewDetailsButton.layer.cornerRadius = 10

        thisWidth = CGFloat(self.frame.width)
        productCollectionView.delegate = self
        productCollectionView.dataSource = self
        
       
    }
    override func layoutSubviews() {
     
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    
//    func imageTapped(index:Int) {
//
//           let imageObject = imageObjectArray[index]
//                  let url = URL(string: imageObject.image )
//        // let imageView = sender.view as! UIImageView
//         let newImageView = UIImageView()
//           newImageView.kf.setImage(with: url)
//         newImageView.frame = UIScreen.main.bounds
//         newImageView.backgroundColor = .black
//         newImageView.contentMode = .scaleAspectFit
//         newImageView.isUserInteractionEnabled = true
//         let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
//         newImageView.addGestureRecognizer(tap)
//        self.view.addSubview(newImageView)
//         self.navigationController?.isNavigationBarHidden = true
//         self.tabBarController?.tabBar.isHidden = true
//     }
//
//     @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
//         self.navigationController?.isNavigationBarHidden = false
//         self.tabBarController?.tabBar.isHidden = false
//         sender.view?.removeFromSuperview()
//     }
    
    
}
extension MarketItemCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = imageObjectArray?.count{
            return count
        }
        return 0
       // imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "itemCell", for: indexPath) as! imageCollectionCell
        let imageObject = imageObjectArray![indexPath.item]
//        let url = URL(string: imageObject.image )
//        cell.itemImage.kf.setImage(with: url)
//        
        
        cell.itemImage.kf.setImage(with: URL(string: imageObject.image), placeholder: nil, options: nil, progressBlock: nil, completionHandler: { result in
        switch result {
            case .success(let value):
                        print("Image: \(value.image). Got from: \(value.cacheType)")
          //  cell.itemImage.downloadImageFrom(link: value.image, contentMode: <#T##UIView.ContentMode#>)
            
            
            case .failure(let error):
                        print("Error: \(error)")
            }
        })
        
        
        
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.layer.frame.size

        return size
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //imageTapped(index: indexPath.item)
        ownerPage?.selectedItem = self.item!
        ownerPage?.performSegue(withIdentifier: "listToDetail", sender: ownerPage)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offSet = scrollView.contentOffset.x
        let width = scrollView.frame.width
        let horizontalCenter = width / 2

        pageController.currentPage = Int(offSet + horizontalCenter) / Int(width)
    }
}
class imageCollectionCell:UICollectionViewCell{
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var soldOutImage: UIImageView!
    
}
