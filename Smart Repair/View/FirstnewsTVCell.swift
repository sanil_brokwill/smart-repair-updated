//
//  FirstnewsTVCell.swift
//  Smart Repair
//
//  Created by Infrap on 7/9/21.
//  Copyright © 2021 Infrap. All rights reserved.
//

import UIKit
import ReadMoreTextView
import ExpandableLabel

protocol FirstnewsTVCellDelegate {
    func moreTapped(cell: FirstnewsTVCell)
}

class FirstnewsTVCell: UITableViewCell {

    @IBOutlet weak var readmoretextview: ReadMoreTextView!
    @IBOutlet weak var FirstMain: UIView!
    
    @IBOutlet weak var Firstview: UIView!
    
    @IBOutlet weak var NameLabel: UILabel!
    
    @IBOutlet weak var TimeLabel: UILabel!
    
    @IBOutlet weak var Middleview: UIView!
    
    @IBOutlet weak var Imgdisplayview: UIImageView!
    
    @IBOutlet weak var HeadingLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var DescriptionView: UITextView!
    
    @IBOutlet weak var showmorebutton: UIButton!
    
    @IBOutlet weak var descriptionlbl: ExpandableLabel!
   
    var delegate: FirstnewsTVCellDelegate?

    var isExpanded: Bool = false
    
    
    var showmore : String?
    
    
    var textFieldText = ""
    var isTextExpanded = false
    
    //var news_events = NewsEventsData()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        showmorebutton.isEnabled = false
       // showmorebutton.isHidden = true
        descriptionLabel.numberOfLines = 2
        
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.labelAction(gesture:)))
        descriptionLabel.addGestureRecognizer(tap)
        descriptionLabel.isUserInteractionEnabled = true
                tap.delegate = self
       
        
        //self.showmorebutton.addTarget(self, action: #selector(showmoreButtonTapped(_:)), for: .touchUpInside)
    }
    
    @objc func labelAction(gesture: UITapGestureRecognizer)
        {
            descriptionLabel.numberOfLines = descriptionLabel.numberOfLines == 0 ? 2 : 0
            UIView.animate(withDuration: 0.5) {
                self.descriptionLabel.superview?.layoutIfNeeded()
                  }
        }
    

    @IBAction func showmorebtnpressed(_ sender: Any) {
        
    
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
//    @IBAction func showmoreButtonTapped(_ sender: UIButton){
//
//        if let youtuber = showmore,
//               let delegate = delegate {
//            self.delegate?.youtuberTableViewCell(self, subscribeButtonTappedFor: showmore!)
//            }
//
//
//    }
    
    

}





