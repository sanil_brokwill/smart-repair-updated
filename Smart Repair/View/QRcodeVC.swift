//
//  QRcodeVC.swift
//  Smart Repair
//
//  Created by MyMac on 21/12/21.
//  Copyright © 2021 Infrap. All rights reserved.
//
import AVFoundation
import UIKit
import SafariServices
import SwiftQRScanner
// This class  and code is not using 
class QRcodeVC: UIViewController, AVCaptureMetadataOutputObjectsDelegate  {
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
   

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        
    }
    
    
    
//    @IBAction func scanQRCode(_ sender: Any) {
//
//        //QRCode scanner without Camera switch and Torch
//        let scanner = QRCodeScannerController()
//
//        //QRCode with Camera switch and Torch
//        //let scanner = QRCodeScannerController(cameraImage: UIImage(named: "camera"), cancelImage: UIImage(named: "cancel"), flashOnImage: UIImage(named: "flash-on"), flashOffImage: UIImage(named: "flash-off"))
//        scanner.delegate = self
//        self.present(scanner, animated: true, completion: nil)
//    }
////        createScanningFrame()
//        createScanningIndicator()
//        view.backgroundColor = UIColor.black
//         captureSession = AVCaptureSession()
//          //guard let videoCaptureVideo
//
//        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video)
//        else{ return }
//        let videoInput: AVCaptureDeviceInput
//        do{
//            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
//        } catch{
//            return
//        }
//
//        if(captureSession.canAddInput(videoInput)){
//            captureSession.addInput(videoInput)
//        }else{
//            failed()
//            return
//        }
//
//        let metadataOutput = AVCaptureMetadataOutput()
//       // metadataOutput.rectOfInterest = convertRectOfInterest(rect:)
//
//        if (captureSession.canAddOutput(metadataOutput)){
//            captureSession.addOutput(metadataOutput)
//
//            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
//            metadataOutput.metadataObjectTypes = [.qr]
//
//        }
//        else{
//            failed()
//            return
//        }
//
//        // Do any additional setup after loading the view.
//                previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
//
//
//                previewLayer.frame = view.layer.bounds
//                previewLayer.videoGravity = .resizeAspectFill
//                view.layer.addSublayer(previewLayer)
//
//
//
//                captureSession.startRunning()
//
//        let visibleRect = previewLayer.metadataOutputRectConverted(fromLayerRect: previewLayer.bounds)
//        metadataOutput.rectOfInterest = visibleRect
//
//
//
//            }
//
//            func failed() {
//                let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
//                ac.addAction(UIAlertAction(title: "OK", style: .default))
//                present(ac, animated: true)
//                captureSession = nil
//            }
//
//
////    if let rect = videoPreviewLayer?.rectOfInterestConverted(parentRect: self.view.frame, fromLayerRect: scanAreaView.frame) {
////            captureMetadataOutput.rectOfInterest = rect
////        }
//    func createScanningIndicator() {
//
//        let height: CGFloat = 15
//        let opacity: Float = 0.4
//        let topColor = UIColor.green.withAlphaComponent(0)
//        let bottomColor = UIColor.green
//
//        let layer = CAGradientLayer()
//        layer.colors = [topColor.cgColor, bottomColor.cgColor]
//        layer.opacity = opacity
//
//        let squareWidth = view.frame.width * 0.6
//        let xOffset = view.frame.width * 0.2
//        let yOffset = view.frame.midY - (squareWidth / 2)
//        layer.frame = CGRect(x: xOffset, y: yOffset, width: squareWidth, height: height)
//
//        self.view.layer.insertSublayer(layer, at: 0)
//
//        let initialYPosition = layer.position.y
//        let finalYPosition = initialYPosition + squareWidth - height
//        let duration: CFTimeInterval = 2
//
//        let animation = CABasicAnimation(keyPath: "position.y")
//        animation.fromValue = initialYPosition as NSNumber
//        animation.toValue = finalYPosition as NSNumber
//        animation.duration = duration
//        animation.repeatCount = .infinity
//        animation.isRemovedOnCompletion = false
//
//        layer.add(animation, forKey: nil)
//    }
//
//    func convertRectOfInterest(rect: CGRect) -> CGRect {
//        let screenRect = self.view.frame
//        let screenWidth = screenRect.width
//        let screenHeight = screenRect.height
//        let newX = 1 / (screenWidth / rect.minX)
//        let newY = 1 / (screenHeight / rect.minY)
//        let newWidth = 1 / (screenWidth / rect.width)
//        let newHeight = 1 / (screenHeight / rect.height)
//        return CGRect(x: newX, y: newY, width: newWidth, height: newHeight)
//    }
//
//
//    func loadSafari(url : String){
//      guard let url = URL(string: url) else { return }
//      let safariController = SFSafariViewController(url: url)
//      present(safariController, animated: true, completion: nil)
//    }
//
//
//
//            override func viewWillAppear(_ animated: Bool) {
//                super.viewWillAppear(animated)
//
//                if (captureSession?.isRunning == false) {
//                    captureSession.startRunning()
//                }
//            }
//
//            override func viewWillDisappear(_ animated: Bool) {
//                super.viewWillDisappear(animated)
//
//                if (captureSession?.isRunning == true) {
//                    captureSession.stopRunning()
//                }
//            }
//
//            func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
//                captureSession.stopRunning()
//
//                if let metadataObject = metadataObjects.first {
//                    guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
//                    guard let stringValue = readableObject.stringValue else { return }
//                    AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
//                    found(code: stringValue)
//                }
//
//                dismiss(animated: true)
//            }
//
//            func found(code: String) {
//                print(code)
//
//                if code.isValidURL {
//
//                    loadSafari(url: code)
//                    UIPasteboard.general.string = code
//
//
//                    showToast(controller: self, message : "Copied to clipboard", seconds: 1.0)
//
//                }
//
//                else
//
//                {
//                    let ac = UIAlertController(title: "Sorry", message: "URL is invalid.", preferredStyle: .alert)
//                    ac.addAction(UIAlertAction(title: "OK", style: .default))
//                    present(ac, animated: true)
//
//
//
//                }
//
//
//
//            }
//
//            override var prefersStatusBarHidden: Bool {
//                return true
//            }
//
//            override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
//                return .portrait
//            }
//
//
//
//    func createScanningFrame() {
//
//        let lineLength: CGFloat = 15
//        let squareWidth = view.frame.width * 0.6
//        let topLeftPosX = view.frame.width * 0.2
//        let topLeftPosY = view.frame.midY - (squareWidth / 2)
//        let btmLeftPosY = view.frame.midY + (squareWidth / 2)
//        let btmRightPosX = view.frame.midX + (squareWidth / 2)
//        let topRightPosX = view.frame.width * 0.8
//
//        let path = UIBezierPath()
//
//        //top left
//        path.move(to: CGPoint(x: topLeftPosX, y: topLeftPosY + lineLength))
//        path.addLine(to: CGPoint(x: topLeftPosX, y: topLeftPosY))
//        path.addLine(to: CGPoint(x: topLeftPosX + lineLength, y: topLeftPosY))
//
//        //bottom left
//        path.move(to: CGPoint(x: topLeftPosX, y: btmLeftPosY - lineLength))
//        path.addLine(to: CGPoint(x: topLeftPosX, y: btmLeftPosY))
//        path.addLine(to: CGPoint(x: topLeftPosX + lineLength, y: btmLeftPosY))
//
//        //bottom right
//        path.move(to: CGPoint(x: btmRightPosX - lineLength, y: btmLeftPosY))
//        path.addLine(to: CGPoint(x: btmRightPosX, y: btmLeftPosY))
//        path.addLine(to: CGPoint(x: btmRightPosX, y: btmLeftPosY - lineLength))
//
//        //top right
//        path.move(to: CGPoint(x: topRightPosX, y: topLeftPosY + lineLength))
//        path.addLine(to: CGPoint(x: topRightPosX, y: topLeftPosY))
//        path.addLine(to: CGPoint(x: topRightPosX - lineLength, y: topLeftPosY))
//
//        let shape = CAShapeLayer()
//        shape.path = path.cgPath
//        shape.strokeColor = UIColor.white.cgColor
//        shape.lineWidth = 3
//        shape.fillColor = UIColor.clear.cgColor
//
//        self.view.layer.insertSublayer(shape, at: 0)
//    }
//
//
//
//
//    func showToast(controller: UIViewController, message : String, seconds: Double) {
//        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
//        alert.view.backgroundColor = UIColor.black
//        alert.view.alpha = 0.6
//        alert.view.layer.cornerRadius = 15
//        controller.present(alert, animated: true)
//        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
//            alert.dismiss(animated: true)
//
//
//
//        }
//    }
//
//
//
//        }
//extension String {
//    var isValidURL: Bool {
//        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
//        if let match = detector.firstMatch(in: self, options: [], range: NSRange(location: 0, length: self.utf16.count)) {
//            // it is a link, if the match covers the whole string
//            return match.range.length == self.utf16.count
//        } else {
//            return false
//        }
//    }
}


//extension QRcodeVC: QRScannerCodeDelegate {
//    func qrScanner(_ controller: UIViewController, scanDidComplete result: String) {
//        print("result:\(result)")
//    }
//
//    func qrScannerDidFail(_ controller: UIViewController, error: String) {
//        print("error:\(error)")
//    }
//
//    func qrScannerDidCancel(_ controller: UIViewController) {
//        print("SwiftQRScanner did cancel")
//    }
//
//}

