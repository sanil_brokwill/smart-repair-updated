//
//  ClaimTableViCell.swift
//  Smart Repair
//
//  Created by Infrap on 15/9/21.
//  Copyright © 2021 Infrap. All rights reserved.
//

import UIKit

class ClaimTableViCell: UITableViewCell {

    @IBOutlet weak var iconimage: UIImageView!
    @IBOutlet weak var labeldata: UILabel!
    @IBOutlet weak var Textdata: UITextView!
    @IBOutlet weak var Tileshapedview: UIView!
    @IBOutlet weak var Mainview: UIView!
    
    @IBOutlet weak var RedeemButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let color: UIColor = UIColor( red: CGFloat(197/255.0), green: CGFloat(197/255.0), blue: CGFloat(197/255.0), alpha: CGFloat(1.0) )
        
//        Tileshapedview.layer.cornerRadius = 24
//        Tileshapedview.layer.shadowColor = color.cgColor
//        Tileshapedview.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
//        Tileshapedview.layer.shadowRadius = 9.0
//        Tileshapedview.layer.shadowOpacity = 0.6
        
        Tileshapedview.layer.cornerRadius = 15
        Tileshapedview.clipsToBounds = true
       Tileshapedview.layer.masksToBounds = false
        Tileshapedview.layer.shadowRadius = 8
        Tileshapedview.layer.shadowOpacity = 0.6
        Tileshapedview.layer.shadowOffset = CGSize(width: 3, height: 3)
        Tileshapedview.layer.shadowColor = color.cgColor
        //Textdata.isUserInteractionEnabled = false
        
        RedeemButton.layer.cornerRadius = 15
        
       // RedeemButton.titleLabel?.font = UIFont(name: "Montserrat-Regular", size: 8.0)
        
        
        
        
    }
    
    // Inside UITableViewCell subclass

    override func layoutSubviews() {
        super.layoutSubviews()

       // contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
}
