

import UIKit
import Kingfisher

class CouponCardCell: UITableViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var expiryDateLabel: UILabel!
    @IBOutlet weak var staringDateLabel: UILabel!
    
    var couponData :CouponData?{
            didSet{
        
                let timeInstance = TimeToReadable()
                let date = timeInstance.getTimeFormatted(from: couponData!.coupon_expires_on)
                let startDate = timeInstance.getTimeFormatted(from: couponData!.coupon_added_on)
                expiryDateLabel.text = "Expires on \(date)"
                staringDateLabel.text = "Starts on \(startDate)"
            }
        }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
         setUpView()
//        loadData()
    }
    override func prepareForReuse() {
     itemImage.image = UIImage(named: "default")
    }
    func loadData(){
      expiryDateLabel.text = "Expires on \(couponData?.coupon_expires_on ?? "No Date Available")"

    }
    
    func setUpView(){
        
        mainView.layer.cornerRadius = 10
        itemImage.layer.cornerRadius = 10
        mainView.layer.shadowRadius = 3
        mainView.layer.shadowOffset = .zero
        mainView.layer.shadowOpacity = 0.3
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
