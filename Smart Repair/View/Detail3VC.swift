//
//  Detail3VC.swift
//  Smart Repair
//
//  Created by MyMac on 16/11/21.
//  Copyright © 2021 Infrap. All rights reserved.
//

import UIKit
//import BSImagePicker
import Photos
//import YPImagePicker
import TLPhotoPicker
import Alamofire

//Add market Detail page 3(adding images and uploading)
class Detail3VC: UIViewController,TLPhotosPickerViewControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource {
    
    @IBOutlet weak var selectimagebtn: UIButton!
    @IBOutlet weak var pagecontrol: UIPageControl!
    @IBOutlet weak var imageCollectionview: UICollectionView!
    @IBOutlet weak var NextButton: UIButton!
    let mem = UserDefaults.standard.value(forKey: "memberId")
    let sess = UserDefaults.standard.value(forKey: "sessionId")
    let device = UserDefaults.standard.value(forKey: "deviceId")
    
    var brandName = UserDefaults.standard.value(forKey: "BrandName")
    var vehicleName = UserDefaults.standard.value(forKey: "VehicleName")
    var Varient = UserDefaults.standard.value(forKey: "Varient")
    var kiloMeters = UserDefaults.standard.value(forKey: "Kilometers")
    var manufactureYear = UserDefaults.standard.value(forKey: "ManufactureYear")
    var registerExpiry = UserDefaults.standard.value(forKey: "RegisterExpiry")
    var Fuel = UserDefaults.standard.value(forKey: "fuel")
    var Colour = UserDefaults.standard.value(forKey: "colour")
    var Engine = UserDefaults.standard.value(forKey: "Engine")
    var Cylinder = UserDefaults.standard.value(forKey: "Cylinder")
    var Seat = UserDefaults.standard.value(forKey: "Seat")
    var Door = UserDefaults.standard.value(forKey: "Door")
    var Bluetooth = UserDefaults.standard.value(forKey: "Bluetooth")
    var Radio = UserDefaults.standard.value(forKey: "Radio")
    var USBport = UserDefaults.standard.value(forKey: "Usbport")
    var Odometer = UserDefaults.standard.value(forKey: "Odometer")
    var Transmission = UserDefaults.standard.value(forKey: "Transmission")
    
    var Price = UserDefaults.standard.value(forKey: "Price")
    var Descriptions = UserDefaults.standard.value(forKey: "Description")
    var currentLocation = UserDefaults.standard.value(forKey: "CurrentLocation")
    var countryCode = UserDefaults.standard.value(forKey: "CountryCode")
    var phoneNumber = UserDefaults.standard.value(forKey: "Phonenumber")
    var whatsappNumber = UserDefaults.standard.value(forKey: "Whatsappnumber")
    var IsAvailable = UserDefaults.standard.value(forKey: "IsAvailable")
    var selectedAssets = [TLPHAsset]()
    var dataArray: [UIImage] = []
    var flag = 0
    
    @IBOutlet weak var Backbtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NextButton.layer.cornerRadius = 15
        view.backgroundColor = UIColor.white
        imageCollectionview.isPagingEnabled = true
        imageCollectionview.showsHorizontalScrollIndicator = true
       // selectimagebtn.setImage(UIImage(named: "camera.png"), for: .normal)
        selectimagebtn.setTitle("", for: .normal)
        if #available(iOS 13.0, *) {
            dataArray.append(UIImage(named:"default" )!)
        } else {
            // Fallback on earlier versions
        }
        
        // Do any additional setup after loading the view.
    }
    
    
    override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            
        }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offSet = scrollView.contentOffset.x
        let width = scrollView.frame.width
        let horizontalCenter = width / 2

        pagecontrol.currentPage = Int(offSet + horizontalCenter) / Int(width)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        pagecontrol.numberOfPages = dataArray.count
        pagecontrol.isHidden = !(dataArray.count > 1)

        return dataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "galleryCell", for: indexPath) as! GalleyCollectionVCell
           
            
         if(flag == 0)
        {
       
           
                cell.SelectedImage.image = dataArray[indexPath.row]
                
         }
           
        
        
        if(flag == 1)
        {
        
            cell.SelectedImage.contentMode = .scaleToFill
            cell.SelectedImage.image = dataArray[indexPath.row]
            
           
        
        }
        
        
        return cell
    }
    
    @IBAction func BacktoMarketclick(_ sender: Any) {
        
        performSegue(withIdentifier: "backtomarket", sender: self)
    }
    
    @IBAction func PickerClick(_ sender: Any) {
        
        flag = 1
        dataArray.removeAll()
        
        let viewController = TLPhotosPickerViewController()
        viewController.delegate = self
        viewController.didExceedMaximumNumberOfSelection = { [weak self] (picker) in
            self?.showExceededMaximumAlert(vc: picker)
        }
        var configure = TLPhotosPickerConfigure()
        configure.numberOfColumn = 3
        viewController.configure = configure
        viewController.selectedAssets = self.selectedAssets
      
        
        viewController.logDelegate = self

        self.present(viewController, animated: true, completion: nil)
    }
    
    func dismissPhotoPicker(withTLPHAssets: [TLPHAsset]) {
        // use selected order, fullresolution image
        self.selectedAssets = withTLPHAssets
        getFirstSelectedImage()
        
        if(dataArray.count == 0)
        {
            dataArray.append(UIImage(named:"default" )!)
        }
        
       
        self.imageCollectionview.reloadData()
        //iCloud or video
//        getAsyncCopyTemporaryFile()
    }
    func getAsyncCopyTemporaryFile() {
        if let asset = self.selectedAssets.first {
            asset.tempCopyMediaFile(convertLivePhotosToJPG: false, progressBlock: { (progress) in
                print(progress)
            }, completionBlock: { (url, mimeType) in
                print("completion\(url)")
                print(mimeType)
            })
        }
    }
    func getFirstSelectedImage() {
        
        for value in selectedAssets

        {

            let array = value.fullResolutionImage
            
            dataArray.append(array!)

           print(dataArray)
           // cell.imagedisplay.image = array[indexPath.row]


        }
        let asset1 = self.selectedAssets.count
        if let asset = self.selectedAssets.last {
            if asset.type == .video {
                asset.videoSize(completion: { [weak self] (size) in
                    //self?.label.text = "video file size\(size)"
                })
                return
            }
            if let image = asset.fullResolutionImage {
                print(image)
                //dataArray.append(selectedAssets.)
                //self.label.text = "local storage image"
               // self.imageView.image = image
            }else {
                print("Can't get image at local storage, try download image")
                asset.cloudImageDownload(progressBlock: { [weak self] (progress) in
                    DispatchQueue.main.async {
                       // self?.label.text = "download \(100*progress)%"
                        print(progress)
                    }
                }, completionBlock: { [weak self] (image) in
                    if let image = image {
                        //use image
                        DispatchQueue.main.async {
                            //self?.label.text = "complete download"
                            //self?.imageView.image = image
                        }
                    }
                })
            }
        }
    }
    func dismissPhotoPicker(withPHAssets: [PHAsset]) {
        
        print("cancel")
        // if you want to used phasset.
    }

    func photoPickerDidCancel() {
        // cancel
        print("cancel1")
        
        
        if(dataArray.count == 0)
        {
            dataArray.append(UIImage(named:"default" )!)
        }
        
    }

    func dismissComplete() {
        // picker dismiss completion
    }
    
    func didExceedMaximumNumberOfSelection(picker: TLPhotosPickerViewController) {
        self.showExceededMaximumAlert(vc: picker)
    }
    
    func handleNoAlbumPermissions(picker: TLPhotosPickerViewController) {
        picker.dismiss(animated: true) {
            let alert = UIAlertController(title: "", message: "Denied albums permissions granted", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func handleNoCameraPermissions(picker: TLPhotosPickerViewController) {
        let alert = UIAlertController(title: "", message: "Denied camera permissions granted", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        picker.present(alert, animated: true, completion: nil)
    }

    func showExceededMaximumAlert(vc: UIViewController) {
        let alert = UIAlertController(title: "", message: "Exceed Maximum Number Of Selection", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
    
    func showUnsatisifiedSizeAlert(vc: UIViewController) {
        let alert = UIAlertController(title: "Oups!", message: "The required size is: 300 x 300", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func NextBtnClick(_ sender: Any) {
        if(flag == 0 || dataArray[0] == UIImage(named:"default" ))
        {
            let alert = UIAlertController(title: "", message: "Select atleast one image from gallery or camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else{
            
            let alert = UIAlertController(title: "Confirm!", message: "Do you want to continue with Submission?", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                   // self.showSpinner(onView: self.view)
                    print(self.dataArray)
                    for i in 0..<self.dataArray.count {

                    //let newsize = self.dataArray[i].photoresizedTo1MB()
                        
                        let newsize = self.dataArray[i].resize(500)
                        
                        self.dataArray[i] = newsize
                    }
                    //self.removeSpinner()

                    print(self.dataArray)
                    self.Uploadmultiformpartdata()
                    //self.removeSpinner()
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)

            
        }
        
    }
    
    func Uploadmultiformpartdata(){
        
        //faButton.isEnabled = false
        
        let  photoImageData = self.dataArray.compactMap {  $0.pngData() }
        
        var parameters: [String : Any] = [:]
        
        let urlString = "http://apiexless.rosterwand.com/backend/web/index.php?r=api_3_1_/add_market"
    
        self.showSpinner(onView: self.view)
    
        parameters = ["member_id": mem, "session_id" : sess ,"device_id" :device,"brand":brandName,"model_name" :vehicleName,"variant":Varient,"total_km":kiloMeters,"make_year":manufactureYear,"registration_expiry":registerExpiry,"fuel":Fuel,"colour":Colour,"price":Price,"engine":Engine,"cylinders":Cylinder,"seates":Seat,"door":Door,"radio":Radio,"bluetooth":Bluetooth,"usb_port":USBport,"odometer":Odometer,"transmission":Transmission,"description":Descriptions,"address":currentLocation,"contact_number":phoneNumber,"whatsapp_number":whatsappNumber]
        
        let headers: HTTPHeaders =
            ["Content-type": "multipart/form-data",
            "Accept": "application/json"]
        
        AF.upload(multipartFormData: { (multipartFormData) in
            
            let count = photoImageData.count
        
            for i in 0..<count{
                    multipartFormData.append(photoImageData[i], withName: "market_image[\(i)]", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
                }
            
            for (key, value) in parameters {
               multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        
        }, to: urlString, //URL Here
                  method: .post,
                  headers: headers)
                  .responseJSON { (resp) in
                      //defer{SVProgressHUD.dismiss()}
                      self.removeSpinner()
                  print(resp)
                      if let err = resp.error{
                                  print(err)
                          print("Something went Wrong!Try again Later")
                          let alert = UIAlertController(title: "Oops!", message: "Something went Wrong!Try again Later", preferredStyle: UIAlertController.Style.alert)
                          alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                          self.present(alert, animated: true, completion: nil)
                                  //onError?(err)
                                  return
                              }
                      
                              print("Successfully Added")
                      let alert = UIAlertController(title: "Success", message: "Successfully Added", preferredStyle: UIAlertController.Style.alert)
                      alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                      self.present(alert, animated: true, completion: nil)
                      
                      //self.faButton.isEnabled = true
                      }

                  }

    
    
    
    
}


extension Detail3VC: TLPhotosPickerLogDelegate {
    //For Log User Interaction
    func selectedCameraCell(picker: TLPhotosPickerViewController) {
        print("selectedCameraCell")
    }
    
    func selectedPhoto(picker: TLPhotosPickerViewController, at: Int) {
        print("selectedPhoto")
    }
    
    func deselectedPhoto(picker: TLPhotosPickerViewController, at: Int) {
        print("deselectedPhoto")
    }
    
    func selectedAlbum(picker: TLPhotosPickerViewController, title: String, at: Int) {
        print("selectedAlbum")
    }
    
    
}


//extension UIImage {
//
//func resized1(withPercentage percentage: CGFloat) -> UIImage? {
//    let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
//    UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
//    defer { UIGraphicsEndImageContext() }
//    draw(in: CGRect(origin: .zero, size: canvasSize))
//    return UIGraphicsGetImageFromCurrentImageContext()
//}
//
//func photoresizedTo1MB() -> UIImage? {
//    guard let imageData = self.pngData() else { return nil }
//
//    var resizingImage = self
//    var imageSizeKB = Double(imageData.count) / 1000.0 // ! Or devide for 1024 if you need KB but not kB
//
//    while imageSizeKB > 1000 { // ! Or use 1024 if you need KB but not kB
//        guard let resizedImage = resizingImage.resized(withPercentage: 0.9),
//              let imageData = resizedImage.pngData()
//            else { return nil }
//
//        resizingImage = resizedImage
//        imageSizeKB = Double(imageData.count) / 1000.0 // ! Or devide for 1024 if you need KB but not kB
//    }
//
//    return resizingImage
//}
//}


extension UIImage {
    func resize(_ max_size: CGFloat) -> UIImage {
        // adjust for device pixel density
        let max_size_pixels = max_size / UIScreen.main.scale
        // work out aspect ratio
        let aspectRatio =  size.width/size.height
        // variables for storing calculated data
        var width: CGFloat
        var height: CGFloat
        var newImage: UIImage
        if aspectRatio > 1 {
            // landscape
            width = max_size_pixels
            height = max_size_pixels / aspectRatio
        } else {
            // portrait
            height = max_size_pixels
            width = max_size_pixels * aspectRatio
        }
        // create an image renderer of the correct size
        let renderer = UIGraphicsImageRenderer(size: CGSize(width: width, height: height), format: UIGraphicsImageRendererFormat.default())
        // render the image
        newImage = renderer.image {
            (context) in
            self.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
        }
        // return the image
        return newImage
    }
}

